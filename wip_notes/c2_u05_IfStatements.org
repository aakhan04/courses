# -*- mode: org -*-

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

#+TITLE: Q&A: If statements
#+AUTHOR: Rachel Wil Sha Singh


*When is the contents of an if statement executed?*

#+BEGIN_SRC cpp :class cpp
  if ( CONDITION )
  {
    // This is executed when CONDITION evaluates to true
    DO_THING_A();
  }
#+END_SRC

*When is the contents of an else statement executed?*

#+BEGIN_SRC cpp :class cpp
  if ( CONDITION )
  {
    // This is executed when CONDITION evaluates to true
    DO_THING_A();
  }
  else
  {
    // This is executed when CONDITION evaluates to false
    DO_THING_B();
  }
#+END_SRC

*When is the contents of an else if statement executed?*

#+BEGIN_SRC cpp :class cpp
  if ( CONDITION1 )
  {
    // This is executed when CONDITION1 evaluates to true
    DO_THING_A();
  }
  else if ( CONDITION2 )
  {
    // This is executed when CONDITION1 evaluates to false
    // and CONDITION2 evalutes to true
    DO_THING_B();
  }
  else if ( CONDITION3 )
  {
    // This is executed when CONDITION1 evaluates to false
    // and CONDITION2 evaluates to false
    // and CONDITION3 evalutes to true
    DO_THING_C();
  }
#+END_SRC

*When is the contents of an else, in an if/else if/else statement, executed?*

#+BEGIN_SRC cpp :class cpp
  if ( CONDITION1 )
  {
    // This is executed when CONDITION1 evaluates to true
    DO_THING_A();
  }
  else if ( CONDITION2 )
  {
    // This is executed when CONDITION1 evaluates to false
    // and CONDITION2 evalutes to true
    DO_THING_B();
  }
  else if ( CONDITION3 )
  {
    // This is executed when CONDITION1 evaluates to false
    // and CONDITION2 evaluates to false
    // and CONDITION3 evalutes to true
    DO_THING_C();
  }
  else
  {
    // This is executed when CONDITION1 evaluates to false
    // and CONDITION2 evaluates to false
    // and CONDITION3 evalutes to false
    DO_THING_D();
  }
#+END_SRC


*Relational operators:*

- equal to: ===
- not equal to: =!==
- less than: =<=
- less than or equal to: =<==
- greater than: =>=
- greater than or equal to: =>==

*Logic operators:*

- and: =&&=
- or: =||=
- not: =!=

-----
