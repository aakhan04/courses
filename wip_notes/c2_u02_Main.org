# -*- mode: org -*-

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

#+TITLE: Q&A: Main()
#+AUTHOR: Rachel Wil Sha Singh

*What are Syntax Errors?*
   Syntax errors are errors in the code language. These can be typos, misspellings, or other linguistic rules broken. These result in errors at compile-time.

*What are Logic Errors?*
   Logic errors are errors in the /logic/ of how the program works. This can include incorrect formulas or boolean expressions.

*What command is used to change directories in the shell?*
   =cd FOLDERNAME=

*What command is used to list files/subdirectories in a directory in the shell?*
   =ls= to list files/folders

*What command is used to build C++ source files in the shell?*
   1. =g++ SOURCENAME.cpp=
   2. =g++ *.cpp=
   3. =g++ *.h *.cpp=
   4. =g++ *.h *.cpp -o PROGRAMNAME.out=

*What is the minimum code required for a C++ program?*
#+BEGIN_SRC cpp :class cpp
  int main() // Program without arguments
  {
    return 0;
  }
#+END_SRC
Or
#+BEGIN_SRC cpp :class cpp
  int main( int argCount, char* args[] ) // Program with arguments
  {
    return 0;
  }
#+END_SRC

*What is a Library?*
   A set of pre-written code meant for use in multiple programs.

*What library do you need to include in your program in order to write text to the screen?*
   =#include <iostream>= to gain access to =cout= (console output) and =cin= (console input).

*What is a program argument?*
   When a program is run, you can add additional information, such as: =./a.out 2 3 4=,
   the values 2, 3, and 4 would be arguments - data passed into the program, which can be
   saved into variables and used while the program is running.
#+BEGIN_SRC cpp :class cpp
  int main( int argCount, char* args[] ) // Program with arguments
  {
    string string_argument = string( args[1] ); // Store argument 1 as a string
    int int_argument = stoi( args[2] );         // Store argument 2 as an integer
    float float_argument = stof( args[3] );     // Store argument 3 as a float


    return 0;
  }
#+END_SRC

-----
