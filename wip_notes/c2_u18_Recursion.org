# -*- mode: org -*-

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

#+TITLE: Q&A: Recursion
#+AUTHOR: Rachel Wil Sha Singh

*What is iterative problem solving?*

Solving problems using loops.

*What is recursive problem solving?*

Solving problems by breaking them into smaller, but idential, steps and calling the function
over and over with different parameters.

*What is a recursive case?*

The state(s) where the function will call itself.

*What is a terminating case?*

The state(s) where the recursive function returns a value.

*Give the code for your iterative and recursive implementations of the following functions:*

*1. Count up*
#+BEGIN_SRC cpp :class cpp
  void CountUp_Iterative( int start, int end )
  {
    for ( int i = start; i <= end; i++ )
    {
      cout << i << endl;
    }
  }

  void CountUp_Recursive( int start, int end )
  {
    // Terminating case
    if ( start > end ) { return; }

    // Recursive case
    cout << start << endl;
    CountUp_Recursive( start+1, end );
  }
#+END_SRC

*2. Sum up*
#+BEGIN_SRC cpp :class cpp
  int SumUp_Iterative( int start, int end )
  {
    int sum = 0;
    for ( int i = start; i <= end; i++ )
    {
      sum += i;
    }
    return sum;
  }

  int SumUp_Recursive( int start, int end )
  {
    // Terminating case
    if ( start > end ) { return 0; }

    // Recursive case
    return start + SumUp_Recursive( start+1, end );
  }
#+END_SRC


-----
