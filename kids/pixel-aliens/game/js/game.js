console.log( "game.js" );

gameState = {
    canvas: null,
    options: {},
    images: {},
    isDone: false,
    screenWidth: 1280,
    screenHeight: 720,

    stars: [],
    trash: [],
    ships: [],

    keys: {
        S1_UP:         { code: "w", isDown: false },
        S1_DOWN:       { code: "s", isDown: false },
        S1_RIGHT:      { code: "d", isDown: false },
        S1_LEFT:       { code: "a", isDown: false },

        S2_UP:         { code: "i", isDown: false },
        S2_DOWN:       { code: "k", isDown: false },
        S2_RIGHT:      { code: "l", isDown: false },
        S2_LEFT:       { code: "j", isDown: false },

        S3_UP:         { code: "ArrowUp", isDown: false },
        S3_DOWN:       { code: "ArrowDown", isDown: false },
        S3_RIGHT:      { code: "ArrowRight", isDown: false },
        S3_LEFT:       { code: "ArrowLeft", isDown: false },

        S4_UP:         { code: "8", isDown: false },
        S4_DOWN:       { code: "2", isDown: false },
        S4_RIGHT:      { code: "6", isDown: false },
        S4_LEFT:       { code: "4", isDown: false },
    },

    Init: function( canvas, options ) {
        gameState.canvas = canvas;
        gameState.options = options;
        gameState.isDone = false;

        gameState.images.ship1 = new Image();
        gameState.images.ship1.src = "assets/images/spaceship1.png";
        gameState.images.ship2 = new Image();
        gameState.images.ship2.src = "assets/images/spaceship2.png";
        gameState.images.ship3 = new Image();
        gameState.images.ship3.src = "assets/images/spaceship3.png";
        gameState.images.ship4 = new Image();
        gameState.images.ship4.src = "assets/images/spaceship4.png";

        gameState.images.trash = [];
        gameState.images.trash.push( new Image() );
        gameState.images.trash.push( new Image() );
        gameState.images.trash.push( new Image() );
        gameState.images.trash.push( new Image() );

        gameState.images.trash[0].src = "assets/images/trash1.png";
        gameState.images.trash[1].src = "assets/images/trash2.png";
        gameState.images.trash[2].src = "assets/images/trash3.png";
        gameState.images.trash[3].src = "assets/images/trash4.png";

        gameState.objShip1.image = gameState.images.ship1;
        gameState.objShip1.x = ( gameState.screenWidth / 5 ) * 1;
        gameState.objShip1.y = gameState.screenHeight - 128;

        gameState.objShip2.image = gameState.images.ship2;
        gameState.objShip2.x = ( gameState.screenWidth / 5 ) * 2;
        gameState.objShip2.y = gameState.screenHeight - 128;

        gameState.objShip3.image = gameState.images.ship3;
        gameState.objShip3.x = ( gameState.screenWidth / 5 ) * 3;
        gameState.objShip3.y = gameState.screenHeight - 128;

        gameState.objShip4.image = gameState.images.ship4;
        gameState.objShip4.x = ( gameState.screenWidth / 5 ) * 4;
        gameState.objShip4.y = gameState.screenHeight - 128;


        UI_TOOLS.CreateButton( { title: "up", words: "W", color: "#000000", font: "bold 20px Sans-serif",
                             src: "assets/ui/key.png", x: 60, y: 10, textX: 10, textY: 30, width: 100, height: 100, fullWidth: 50, fullHeight: 50,
                             Click: function() { gameState.objBear.Move( "UP" ); } } );
        UI_TOOLS.CreateButton( { title: "left", words: "A", color: "#000000", font: "bold 20px Sans-serif",
                             src: "assets/ui/key.png", x: 10, y: 60, textX: 15, textY: 30, width: 100, height: 100, fullWidth: 50, fullHeight: 50,
                             Click: function() { gameState.objBear.Move( "LEFT" ); } } );
        UI_TOOLS.CreateButton( { title: "down", words: "S", color: "#000000", font: "bold 20px Sans-serif",
                             src: "assets/ui/key.png", x: 60, y: 60, textX: 15, textY: 30, width: 100, height: 100, fullWidth: 50, fullHeight: 50,
                             Click: function() { gameState.objBear.Move( "DOWN" ); } } );
        UI_TOOLS.CreateButton( { title: "right", words: "D", color: "#000000", font: "bold 20px Sans-serif",
                             src: "assets/ui/key.png", x: 110, y: 60, textX: 15, textY: 30, width: 100, height: 100, fullWidth: 50, fullHeight: 50,
                             Click: function() { gameState.objBear.Move( "RIGHT" ); } } );

        // Create stars
        for ( var i = 0; i < 25; i++ )
        {
            gameState.stars.push( gameState.InitStar( false ) );
        }

        // Create trash
        for ( var i = 0; i < 10; i++ )
        {
            gameState.trash.push( gameState.InitTrash() );
        }
    },

    Clear: function() {
        UI_TOOLS.ClearUI();
    },

    Click: function( ev ) {
        UI_TOOLS.Click( ev );
    },

    InitStar: function( offscreen ) {
        star = {};
        star.x = Math.floor( Math.random() * gameState.screenWidth );
        star.y = Math.floor( Math.random() * gameState.screenHeight );
        star.radius = Math.floor( Math.random() * 40 ) + 5;
        if ( offscreen ) { star.y - gameState.screenHeight; }
        return star;
    },

    InitTrash: function() {
        var trash = {};
        trash.x = Math.floor( Math.random() * (gameState.screenWidth - 64));
        trash.y = Math.floor( Math.random() * gameState.screenHeight ) - gameState.screenHeight;
        trash.width = 64;
        trash.height = 64;
        trash.type = Math.floor( Math.random() * 4 );
        trash.image = gameState.images.trash[ trash.type ];
        return trash;
    },

    KeyPress: function( ev ) {
        if ( ev.key == "Escape" ) {
            main.changeState( "titleState" );
        }

        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = true;
            }
        } );
    },

    KeyRelease: function( ev ) {
        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = false;
            }
        } );
    },

    Update: function() {
        // Movement input
        if      ( gameState.keys.S1_UP.isDown    ) { gameState.objShip1.Move( "UP" ); }
        else if ( gameState.keys.S1_DOWN.isDown  ) { gameState.objShip1.Move( "DOWN" ); }
        if      ( gameState.keys.S1_LEFT.isDown  ) { gameState.objShip1.Move( "LEFT" ); }
        else if ( gameState.keys.S1_RIGHT.isDown ) { gameState.objShip1.Move( "RIGHT" ); }

        if      ( gameState.keys.S2_UP.isDown    ) { gameState.objShip2.Move( "UP" ); }
        else if ( gameState.keys.S2_DOWN.isDown  ) { gameState.objShip2.Move( "DOWN" ); }
        if      ( gameState.keys.S2_LEFT.isDown  ) { gameState.objShip2.Move( "LEFT" ); }
        else if ( gameState.keys.S2_RIGHT.isDown ) { gameState.objShip2.Move( "RIGHT" ); }

        if      ( gameState.keys.S3_UP.isDown    ) { gameState.objShip3.Move( "UP" ); }
        else if ( gameState.keys.S3_DOWN.isDown  ) { gameState.objShip3.Move( "DOWN" ); }
        if      ( gameState.keys.S3_LEFT.isDown  ) { gameState.objShip3.Move( "LEFT" ); }
        else if ( gameState.keys.S3_RIGHT.isDown ) { gameState.objShip3.Move( "RIGHT" ); }

        if      ( gameState.keys.S4_UP.isDown    ) { gameState.objShip4.Move( "UP" ); }
        else if ( gameState.keys.S4_DOWN.isDown  ) { gameState.objShip4.Move( "DOWN" ); }
        if      ( gameState.keys.S4_LEFT.isDown  ) { gameState.objShip4.Move( "LEFT" ); }
        else if ( gameState.keys.S4_RIGHT.isDown ) { gameState.objShip4.Move( "RIGHT" ); }

        gameState.objShip1.Update();
        gameState.objShip2.Update();
        gameState.objShip3.Update();
        gameState.objShip4.Update();

        // Move stars
        for ( var i = 0; i < gameState.stars.length; i++ )
        {
            var star = gameState.stars[i];
            main.canvasWindow.fillStyle = "#ffffff";
            main.canvasWindow.font = star.radius + "pt Arial";
            main.canvasWindow.fillText( "*", star.x, star.y );
            star.y += ( star.radius * 0.25 );
            if ( star.y > gameState.screenHeight + 64 )
            {
                star.x = Math.floor( Math.random() * gameState.screenWidth );
                star.y = -30;
            }
        }

        // Move garbage
        for ( var i = 0; i < gameState.trash.length; i++ )
        {
            var trash = gameState.trash[i];
            trash.y += trash.type;
            if ( trash.y > gameState.screenHeight + 64 )
            {
                trash.x = Math.floor( Math.random() * gameState.screenWidth );
                trash.y -= gameState.screenHeight;
                trash.y -= 64;
            }

            // Is trash hitting a player?
            if ( gameState.GetDistance( trash, gameState.objShip1 ) < 64 )
            {
                trash.x = Math.floor( Math.random() * gameState.screenWidth );
                trash.y -= gameState.screenHeight;
                trash.y -= 64;
                gameState.objShip1.score++;
            }

            if ( gameState.GetDistance( trash, gameState.objShip2 ) < 64 )
            {
                trash.x = Math.floor( Math.random() * gameState.screenWidth );
                trash.y -= gameState.screenHeight;
                trash.y -= 64;
                gameState.objShip2.score++;
            }

            if ( gameState.GetDistance( trash, gameState.objShip3 ) < 64 )
            {
                trash.x = Math.floor( Math.random() * gameState.screenWidth );
                trash.y -= gameState.screenHeight;
                trash.y -= 64;
                gameState.objShip3.score++;
            }

            if ( gameState.GetDistance( trash, gameState.objShip4 ) < 64 )
            {
                trash.x = Math.floor( Math.random() * gameState.screenWidth );
                trash.y -= gameState.screenHeight;
                trash.y -= 64;
                gameState.objShip4.score++;
            }
        }
    },

    Draw: function() {
        // Draw grass 9cc978
        main.canvasWindow.fillStyle = "#1a1a1a";
        main.canvasWindow.fillRect( 0, 0, main.settings.width, main.settings.height );

        // Draw stars
        for ( var i = 0; i < gameState.stars.length; i++ )
        {
            var star = gameState.stars[i];
            main.canvasWindow.fillStyle = "#ffffff";
            main.canvasWindow.font = star.radius + "pt Arial";
            main.canvasWindow.fillText( "*", star.x, star.y );
        }

        // Draw trash
        for ( var i = 0; i < gameState.trash.length; i++ )
        {
            var trash = gameState.trash[i];
            main.canvasWindow.drawImage( trash.image, trash.x, trash.y, trash.width, trash.height );
        }

        // Draw ship and score

        main.canvasWindow.drawImage(
            gameState.objShip1.image,
            0, 0, gameState.objShip1.width, gameState.objShip1.height,
            gameState.objShip1.x, gameState.objShip1.y,
            gameState.objShip1.fullWidth, gameState.objShip1.fullHeight );
        main.canvasWindow.fillStyle = "#FFFFFF";
        main.canvasWindow.font = "30px monospace";
        main.canvasWindow.fillText( gameState.objShip1.score, gameState.objShip1.x, gameState.objShip1.y - 10 );

        main.canvasWindow.drawImage(
            gameState.objShip2.image,
            0, 0, gameState.objShip2.width, gameState.objShip2.height,
            gameState.objShip2.x, gameState.objShip2.y,
            gameState.objShip2.fullWidth, gameState.objShip2.fullHeight );
        main.canvasWindow.fillText( gameState.objShip2.score, gameState.objShip2.x, gameState.objShip2.y - 10 );

        main.canvasWindow.drawImage(
            gameState.objShip3.image,
            0, 0, gameState.objShip3.width, gameState.objShip3.height,
            gameState.objShip3.x, gameState.objShip3.y,
            gameState.objShip3.fullWidth, gameState.objShip3.fullHeight );
        main.canvasWindow.fillText( gameState.objShip3.score, gameState.objShip3.x, gameState.objShip3.y - 10 );

        main.canvasWindow.drawImage(
            gameState.objShip4.image,
            0, 0, gameState.objShip4.width, gameState.objShip4.height,
            gameState.objShip4.x, gameState.objShip4.y,
            gameState.objShip4.fullWidth, gameState.objShip4.fullHeight );
        main.canvasWindow.fillText( gameState.objShip4.score, gameState.objShip4.x, gameState.objShip4.y - 10 );

        UI_TOOLS.Draw( gameState.canvas );
    },

    GetDistance: function( obj1, obj2 ) {
        var xDiff = (obj2.x + obj2.width/2) - (obj1.x + obj1.width/2);
        var yDiff = (obj2.y + obj2.height/2) - (obj1.y + obj1.height/2);
        return Math.sqrt( xDiff * xDiff + yDiff * yDiff );
    },

    ClickPlay: function() {
    },



    objShip1: {
        x: 0, y: 0,
        width: 64, height: 64,
        fullWidth: 64, fullHeight: 64,
        speed: 5, image: null,
        velX: 0, velY: 0, maxVel: 10,
        acceleration: 1,
        score: 0,

        Move: function( dir ) {
            var self = gameState.objShip1;
            if      ( dir == "UP" )    { self.velY -= self.acceleration; }
            else if ( dir == "DOWN" )  { self.velY += self.acceleration; }
            else if ( dir == "LEFT" )  { self.velX -= self.acceleration; }
            else if ( dir == "RIGHT" ) { self.velX += self.acceleration; }
        },

        Update: function() {
            var self = gameState.objShip1;
            if ( self.velX < 0.5 || self.velX > 0.5 )
            {
                self.x += self.velX;
            }

            if ( self.velY < 0.5 || self.velY > 0.5 )
            {
                self.y += self.velY;
            }

            // Restrict max velocity
            if ( self.velX > self.maxVel  ) { self.velX = self.maxVel; }
            if ( self.velX < -self.maxVel ) { self.velX = -self.maxVel; }
            if ( self.velY > self.maxVel  ) { self.velY = self.maxVel; }
            if ( self.velY < -self.maxVel ) { self.velY = -self.maxVel; }

            // Bounce if hit side of screen
            if ( self.x < 0 || self.x > gameState.screenWidth - 64 ) { self.velX = -self.velX; }
            if ( self.y < 0 || self.y > gameState.screenHeight - 64 ) { self.velY = -self.velY; }
        }
    },

    objShip2: {
        x: 0, y: 0,
        width: 64, height: 64,
        fullWidth: 64, fullHeight: 64,
        speed: 5, image: null,
        velX: 0, velY: 0, maxVel: 10,
        acceleration: 1,
        score: 0,

        Move: function( dir ) {
            var self = gameState.objShip2;
            if      ( dir == "UP" )    { self.velY -= self.acceleration; }
            else if ( dir == "DOWN" )  { self.velY += self.acceleration; }
            else if ( dir == "LEFT" )  { self.velX -= self.acceleration; }
            else if ( dir == "RIGHT" ) { self.velX += self.acceleration; }
        },

        Update: function() {
            var self = gameState.objShip2;
            if ( self.velX < 0.5 || self.velX > 0.5 )
            {
                self.x += self.velX;
            }

            if ( self.velY < 0.5 || self.velY > 0.5 )
            {
                self.y += self.velY;
            }

            // Restrict max velocity
            if ( self.velX > self.maxVel  ) { self.velX = self.maxVel; }
            if ( self.velX < -self.maxVel ) { self.velX = -self.maxVel; }
            if ( self.velY > self.maxVel  ) { self.velY = self.maxVel; }
            if ( self.velY < -self.maxVel ) { self.velY = -self.maxVel; }

            // Bounce if hit side of screen
            if ( self.x < 0 || self.x > gameState.screenWidth - 64 ) { self.velX = -self.velX; }
            if ( self.y < 0 || self.y > gameState.screenHeight - 64 ) { self.velY = -self.velY; }
        }
    },

    objShip3: {
        x: 0, y: 0,
        width: 64, height: 64,
        fullWidth: 64, fullHeight: 64,
        speed: 5, image: null,
        velX: 0, velY: 0, maxVel: 10,
        acceleration: 1,
        score: 0,

        Move: function( dir ) {
            var self = gameState.objShip3;
            if      ( dir == "UP" )    { self.velY -= self.acceleration; }
            else if ( dir == "DOWN" )  { self.velY += self.acceleration; }
            else if ( dir == "LEFT" )  { self.velX -= self.acceleration; }
            else if ( dir == "RIGHT" ) { self.velX += self.acceleration; }
        },

        Update: function() {
            var self = gameState.objShip3;
            if ( self.velX < 0.5 || self.velX > 0.5 )
            {
                self.x += self.velX;
            }

            if ( self.velY < 0.5 || self.velY > 0.5 )
            {
                self.y += self.velY;
            }

            // Restrict max velocity
            if ( self.velX > self.maxVel  ) { self.velX = self.maxVel; }
            if ( self.velX < -self.maxVel ) { self.velX = -self.maxVel; }
            if ( self.velY > self.maxVel  ) { self.velY = self.maxVel; }
            if ( self.velY < -self.maxVel ) { self.velY = -self.maxVel; }

            // Bounce if hit side of screen
            if ( self.x < 0 || self.x > gameState.screenWidth - 64 ) { self.velX = -self.velX; }
            if ( self.y < 0 || self.y > gameState.screenHeight - 64 ) { self.velY = -self.velY; }
        }
    },

    objShip4: {
        x: 0, y: 0,
        width: 64, height: 64,
        fullWidth: 64, fullHeight: 64,
        speed: 5, image: null,
        velX: 0, velY: 0, maxVel: 10,
        acceleration: 1,
        score: 0,

        Move: function( dir ) {
            var self = gameState.objShip4;
            if      ( dir == "UP" )    { self.velY -= self.acceleration; }
            else if ( dir == "DOWN" )  { self.velY += self.acceleration; }
            else if ( dir == "LEFT" )  { self.velX -= self.acceleration; }
            else if ( dir == "RIGHT" ) { self.velX += self.acceleration; }
        },

        Update: function() {
            var self = gameState.objShip4;
            if ( self.velX < 0.5 || self.velX > 0.5 )
            {
                self.x += self.velX;
            }

            if ( self.velY < 0.5 || self.velY > 0.5 )
            {
                self.y += self.velY;
            }

            // Restrict max velocity
            if ( self.velX > self.maxVel  ) { self.velX = self.maxVel; }
            if ( self.velX < -self.maxVel ) { self.velX = -self.maxVel; }
            if ( self.velY > self.maxVel  ) { self.velY = self.maxVel; }
            if ( self.velY < -self.maxVel ) { self.velY = -self.maxVel; }

            // Bounce if hit side of screen
            if ( self.x < 0 || self.x > gameState.screenWidth - 64 ) { self.velX = -self.velX; }
            if ( self.y < 0 || self.y > gameState.screenHeight - 64 ) { self.velY = -self.velY; }
        }
    },

};
