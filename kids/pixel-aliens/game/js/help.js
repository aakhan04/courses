console.log( "title.js" );

helpState = {
    canvas: null,
    options: {},
    images: {},
    isDone: false,

    Init: function( canvas, options ) {
        helpState.canvas = canvas;
        helpState.options = options;
        helpState.isDone = false;
                            
        LANGUAGE_TOOLS.AddText( "English",      "how to play", "How to play" );
        LANGUAGE_TOOLS.AddText( "Esperanto",    "how to play", "Kiel ludi" );
        
        UI_TOOLS.CreateImage( { title: "background", src: "assets/ui/title.png",
                            x: 0, y: 0, width: 720, height: 720, fullWidth: 1280, fullHeight: 720 } );
                            
        UI_TOOLS.CreateText( { title: "howto", words: LANGUAGE_TOOLS.GetText( "English", "help" ),
                            color: "#000000", font: "bold 30px Sans-serif", x: 10, y: 30 } );
                            
        UI_TOOLS.CreateText( { title: "howto", words: LANGUAGE_TOOLS.GetText( "English", "how to play" ),
                            color: "#000000", font: "bold 25px Sans-serif", x: 20, y: 100 } );

        UI_TOOLS.CreateButton( { title: "backButton", words: LANGUAGE_TOOLS.GetText( "English", "back" ),
                             color: "#ffffff", font: "bold 30px Sans-serif",
                             src: "assets/ui/button.png",
                             x: 10, y: options.height - 80, textX: 60, textY: 35, width: 250, height: 100, fullWidth: 250, fullHeight: 50,
                             Click: function() {
                                 main.changeState( "titleState" );
                                 } } );
    },

    Clear: function() {
        UI_TOOLS.ClearUI();
    },

    Click: function( ev ) {
        UI_TOOLS.Click( ev );
    },

    KeyPress: function( ev ) {
    },

    KeyRelease: function( ev ) {
    },

    Update: function() {
    },

    Draw: function() {
        UI_TOOLS.Draw( helpState.canvas );
    },
};
