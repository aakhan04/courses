console.log( "assets_tools.js" );

ASSET_TOOLS = {
    sounds: {},
    nowPlaying: null,
    soundVolume : 100,
    musicVolume : 100,

    Init: function() {
        ASSET_TOOLS.AddSound( {
            file : "shida/confirm.wav", title : "confirm",
            volume : 1.0, loop : false, isMusic : false
            } );
        ASSET_TOOLS.AddSound( {
            file : "shida/cancel.wav", title : "cancel",
            volume : 1.0, loop : false, isMusic : false
            } );
    },

    UpdateSoundVolumes: function()
    {
        for ( var i = 0; i < ASSET_TOOLS.sounds.length; i++ )
        {
            if ( ASSET_TOOLS.sounds[i].isMusic )
                ASSET_TOOLS.sounds[i].volume = ASSET_TOOLS.musicVolume;
            else
                ASSET_TOOLS.sounds[i].volume = ASSET_TOOLS.soundVolume;
        }
    },

    SetSoundVolume: function( value )
    {
        ASSET_TOOLS.soundVolume = value;
        if ( ASSET_TOOLS.soundVolume > 100 )
            ASSET_TOOLS.soundVolume = 100;
        else if ( ASSET_TOOLS.soundVolume < 0 )
            ASSET_TOOLS.soundVolume = 0;

        ASSET_TOOLS.UpdateSoundVolumes();
    },

    SetMusicVolume: function( value )
    {
        ASSET_TOOLS.musicVolume = value;
        if ( ASSET_TOOLS.musicVolume > 100 )
            ASSET_TOOLS.musicVolume = 100;
        else if ( ASSET_TOOLS.musicVolume < 0 )
            ASSET_TOOLS.musicVolume = 0;

        ASSET_TOOLS.UpdateSoundVolumes();
    },

    GetSoundVolume: function()
    {
        return ASSET_TOOLS.soundVolume;
    },

    GetMusicVolume: function()
    {
        return ASSET_TOOLS.musicVolume;
    },

    ChangeSoundVolume: function( amount )
    {
        return ASSET_TOOLS.SetSoundVolume( ASSET_TOOLS.soundVolume + amount );
    },

    ChangeMusicVolume: function( amount )
    {
        return ASSET_TOOLS.SetMusicVolume( ASSET_TOOLS.musicVolume + amount );
    },

    AddSound: function( options )
    {
        sound = new Audio( options.file );
        sound.title = options.title;
        sound.loop = options.loop;
        sound.isMusic = options.isMusic;

        /*
            * */

        ASSET_TOOLS.sounds[ sound.title ] = sound;
    },

    UpdateItemVolume: function( title ) {
        if ( ASSET_TOOLS.sounds[ title ] == null )
        {
            return;
        }

        if ( ASSET_TOOLS.sounds[ title ].isMusic )
            ASSET_TOOLS.sounds[ title ].volume = ASSET_TOOLS.musicVolume / 100;
        else
            ASSET_TOOLS.sounds[ title ].volume = ASSET_TOOLS.soundVolume / 100;
    },

    PlaySound: function( title ) {
        if ( ASSET_TOOLS.sounds[ title ] != null ) {
            ASSET_TOOLS.UpdateItemVolume( title );

            if ( ASSET_TOOLS.sounds[ title ].isMusic == false ) {
                ASSET_TOOLS.sounds[ title ].play();
            }
        }
    },

    PlayMusic: function( title ) {
        if ( ASSET_TOOLS.sounds[ title ] != null ) {
            ASSET_TOOLS.UpdateItemVolume( title );

            if ( ASSET_TOOLS.sounds[ title ].isMusic
                 && ASSET_TOOLS.nowPlaying != title ) {
                ASSET_TOOLS.nowPlaying = title;
                ASSET_TOOLS.StopMusic();
                ASSET_TOOLS.sounds[ title ].play();
            }
        }
    },

    StopMusic: function() {
        for ( var i in ASSET_TOOLS.sounds ) {
            // Stop it if this is an Audio item
            if ( ASSET_TOOLS.sounds[ i ].isMusic ) {
                ASSET_TOOLS.sounds[ i ].pause();
                ASSET_TOOLS.nowPlaying = null;
            }
        }
    }
};
