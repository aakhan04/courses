# -*- mode: org -*-

#+TITLE: CS 200: Concepts of Programming using C++ (Summer 2024 version)  WORK IN PROGRESS
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style/rworgmode.css" />
#+HTML_HEAD: <link rel="icon" type="image/png" href="http://rachel.likespizza.com//web-assets/images/favicon-moose.png">
# #+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
# #+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
# #+HTML_HEAD: <script>hljs.initHighlightingOnLoad();</script>

#+OPTIONS: toc:4          (only include two levels in TOC)
# #+OPTIONS: num:nil

#+LaTeX_CLASS_OPTIONS: [a4paper,twoside]
#+LATEX_HEADER: \usepackage{style/rworgmode}
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage{graphicx}
#+LATEX_HEADER: \hypersetup{colorlinks=true,linkcolor=blue}
# #+LATEX_HEADER: \usepackage{mdframed}


#+ATTR_LATEX: :width 100px
[[file:images/semester-summer.png]]

Rachel Wil Sha Singh's Core C++ Course © 2024 by Rachel Wil Sha Singh is licensed under CC BY 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

- These course documents are written in *emacs orgmode* and the files can be found here: https://gitlab.com/moosadee/courses
- HTML Version: https://moosadee.gitlab.io/courses/202406_Summer/book_corecpp.html
- PDF Version: https://moosadee.gitlab.io/courses/202406_Summer/book_corecpp.pdf
- Dedicated to a better world, and those who work to try to create one.

#+BEGIN_LATEX
\newpage
#+END_LATEX


-----------------------------------------------
* Navigating this course
# Write in the book!!

Modules and unlocks

-----------------------------------------------
* Summer 2024 Schedule

| Week # | Monday  | Topics                                                   |
|--------+---------+----------------------------------------------------------|
|      1 | June 3  | Unit 00: Welcome and setup                               |
|        |         | Unit 01: C++: Variables, input, and output               |
|        |         | Unit 02: Tech Literacy: Exploring computers (fs, cli)    |
|--------+---------+----------------------------------------------------------|
|      2 | June 10 | Unit 03: C++: Functions                                  |
|        |         | Unit 04: Tech Literacy: Debugging and researching        |
|--------+---------+----------------------------------------------------------|
|      3 | June 17 | Unit 05: C++: Structs                                    |
|        |         | Unit 06: Tech Literacy: Careers in tech                  |
|--------+---------+----------------------------------------------------------|
|      4 | June 24 | Unit 07: C++: Looping                                    |
|        |         | Unit 08: C++: Arrays and vectors                         |
|--------+---------+----------------------------------------------------------|
|      5 | July 1  | Unit 09: C++: Branching                                  |
|        |         | Unit 10: C++ Searching and sorting                       |
|        |         | Unit 11: Tech Literacy: Current trends in tech           |
|--------+---------+----------------------------------------------------------|
|      6 | July 8  | Unit 12: Tech Literacy: Object Oriented Programming, UML |
|        |         | Unit 13: C++: Classes and inheritance                    |
|--------+---------+----------------------------------------------------------|
|      7 | July 15 | Unit 14: C++: Strings and File I/O                       |
|        |         | Unit 15: Tech Literacy: Ethics in tech                   |
|--------+---------+----------------------------------------------------------|
|      8 | July 22 | Unit 16: C++: Intro to recursion                         |




-----------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
* Reference
** C++ quick reference
#+INCLUDE: "../reference/quick_reference_cpp.org"

# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Basic computer skills
#+INCLUDE: "../reference/computer_skills.org"


# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Common C++ bugs and issues
#+INCLUDE: "../reference/common_issues_cpp.org"


# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Code and UI style guide
#+INCLUDE: "../reference/style_guide.org"


# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** How to use VSCode
#+INCLUDE: "../reference/ide_vscode_guide.org"

# # -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# #+INCLUDE: "../widgets/new-page.org"
# ** How to use GitLab

# # -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# #+INCLUDE: "../widgets/new-page.org"
# ** Resource archive
# #+INCLUDE: "../reference/resource_map.org"




-----------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
* Syllabus





-----------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
* Course contents

# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 00: Welcome and setup

*** Reading
[[file:images/banner-reading.png]]


**** *Welcome!*
#+INCLUDE: "Course2_CoreCPP/reading/reading_Welcome_YouBelong.org"
---------------------------------

**** *How to navigate the course*
#+INCLUDE: "Course2_CoreCPP/reading/reading_Welcome_NavigatingTheCourse.org"
---------------------------------

**** *Tools for software development*
#+INCLUDE: "Course2_CoreCPP/reading/reading_Welcome_ToolsSetup.org"

# --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"


# *** Review questions
# *Review questions:* Write down your answers and then use them for the
# answers for the corresponding quiz on Canvas!
# 
# 1. What kind of program is needed to convert human-readable code into computer binary executables?
#    #+INCLUDE: "../widgets/spacing3cm.org"
# 2. Computer CPUs only understand what "language"?
#    #+INCLUDE: "../widgets/spacing3cm.org"
# 3. What kind of program is used by professionals to store their code and keep track of changes?
#    #+INCLUDE: "../widgets/spacing3cm.org"
# 
# We will explore more about how computers work later on, but for now, let's get our tools set up!


*** Programming Lab

[[file:images/banner-lab.png]]








# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 01: C++: Variables, input, and output

*** Reading
[[file:images/banner-reading.png]]
*Reading: Welcome, how to navigate the course, and tools setup*

**** *main(): The starting function*
#+INCLUDE: "Course2_CoreCPP/reading/reading_CPPBasics_1_main.org"
---------------------------------

**** *Variables: Storing our data*
#+INCLUDE: "Course2_CoreCPP/reading/reading_CPPBasics_2_Variables.org"
---------------------------------

**** *Memory addresses and pointer variables*
#  #+INCLUDE: "Course2_CoreCPP/reading_.org"
---------------------------------
**** *Input and output: Interacting with the user*
#+INCLUDE: "Course2_CoreCPP/reading/reading_CPPBasics_3_IO.org"




# *** Review questions
# *Review questions:* Write down your answers and then use them for the
# answers for the corresponding quiz on Canvas!
# 
# 1. What function is the starting point of every C++ program?
#    #+INCLUDE: "../widgets/spacing1cm.org"
# 2. What is a code-block?
#    #+INCLUDE: "../widgets/spacing1cm.org"
# 3. What command means the program ended with no errors?
#    #+INCLUDE: "../widgets/spacing1cm.org"
# 4. When should a line of code end with a semicolon =;=, and when will it have a code block ={}= instead?
#    #+INCLUDE: "../widgets/spacing1cm.org"
# 5. How do you write a single-line comment?
#    #+INCLUDE: "../widgets/spacing1cm.org"
# 6. How do you write a multi-line comment?
#    #+INCLUDE: "../widgets/spacing1cm.org"
# 7. What are syntax errors?
#    #+INCLUDE: "../widgets/spacing1cm.org"
# 8. What are logic errors?
#    #+INCLUDE: "../widgets/spacing1cm.org"
# 9. What is the *output stream operator*?
#    #+INCLUDE: "../widgets/spacing1cm.org"
# 10. What is the *input stream operator*?
#    #+INCLUDE: "../widgets/spacing1cm.org"


*** Programming Lab
[[file:images/banner-lab.png]]





# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 02: Tech Literacy: Exploring computers

*** Reading
[[file:images/banner-reading.png]]

*Reading: DESCRIPTION*

**** *SUBTOPIC*

INCLUDE HOME, END, CTRL+F, COPY, PASTE, CUT, SELECT ALL

# (fs, cli)



# *** Review questions



*** Tech literacy



# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 03: C++: Functions

*** Reading
[[file:images/banner-reading.png]]

#+INCLUDE: "Course2_CoreCPP/reading/reading_Functions.org"



*** Programming Lab
[[file:images/banner-lab.png]]





# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 04: Tech Literacy: Debugging and researching

*** Reading
[[file:images/banner-reading.png]]



*** Tech literacy




# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 05: C++: Structs

*** Reading
[[file:images/banner-reading.png]]

#+INCLUDE: "Course2_CoreCPP/reading/reading_Structs.org"




*** Programming Lab
[[file:images/banner-lab.png]]



# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 06: Tech Literacy: Careers in tech

*** Reading
[[file:images/banner-reading.png]]



*** Tech literacy



# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 07: C++: Looping

*** Reading
[[file:images/banner-reading.png]]

**** *While loops*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_WhileLoops.org"

**** *For loops*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_ForLoops.org"




*** Programming Lab
[[file:images/banner-lab.png]]



# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 08: C++: Arrays and vectors

*** Reading
[[file:images/banner-reading.png]]

**** *Arrays and vectors*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_ArraysVectors.org"

**** *Dynamic memory allocation with pointers*
#  #+INCLUDE: "Course2_CoreCPP/reading_.org"



*** Programming Lab
[[file:images/banner-lab.png]]




# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 09: C++: Branching

*** Reading
[[file:images/banner-reading.png]]

**** *Boolean expressions*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_BooleanExpressions.org"

**** *If statements*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_IfStatements.org"

**** *Switch statements*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_SwitchStatements.org"




*** Programming Lab
[[file:images/banner-lab.png]]




# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 10: C++: Searching and sorting

*** Reading
[[file:images/banner-reading.png]]

#+INCLUDE: "Course2_CoreCPP/reading/reading_SearchSort.org"



*** Programming Lab
[[file:images/banner-lab.png]]





# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 11: Tech Literacy: Current trends in tech
# , UML


*** Reading
[[file:images/banner-reading.png]]


*** Tech literacy






# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 12: Tech Literacy: Object Oriented Programming
*** Reading
[[file:images/banner-reading.png]]



*** Tech literacy


** Unit 13: C++: Classes and Inheritance
*** Reading

[[file:images/banner-reading.png]]

**** *Classes*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_Classes.org"

**** *Inheritance*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_Inheritance.org"



*** Programming lab
[[file:images/banner-lab.png]]


*** Project updates


# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 14: C++: Strings and File I/O

*** Reading
[[file:images/banner-reading.png]]

**** *Strings*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_Strings.org"

**** *File I/O*
 #+INCLUDE: "Course2_CoreCPP/reading/reading_FileIO.org"


*** Programming lab
[[file:images/banner-lab.png]]

*** Project updates



# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 15: Tech Literacy: Ethics in tech

*** Reading
[[file:images/banner-reading.png]]


*** Tech literacy



# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Unit 16: C++: Intro to recursion

*** Reading
[[file:images/banner-reading.png]]

#+INCLUDE: "Course2_CoreCPP/reading/reading_Recursion.org"



*** Programming lab
[[file:images/banner-lab.png]]






