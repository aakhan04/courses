# -*- mode: org -*-


---------------------------------------------------------
* *Branching*

Branching is one of the core forms of *controlling the flow of the program*.
We ask a question, and based on the result we perhaps do /this code over here/
or maybe /that code over there/ - the program results change based
on variables and data accessible to it.


We will mostly be using *if / else if / else statements*,
though *switch* statements have their own use as well.
Make sure to get a good understanding of how each type of branching
mechanism "flows".




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *If statements*
If statements (usually the general term encompassing if / else if / else as well)
are a way we can ask a question and respond: If $a$ is less than $b$ then...
or if $a$ is greater than $c$ then... Otherwise do this default thing...


Let's look at some examples while ironing out how it works.



*** *If statements*

For a basic *if statement*,
we use the syntax:

#+BEGIN_SRC cpp :class cpp
  // Do things 1

  if ( CONDITION )
  {
    // Do things 1-a
  }

  // Do things 2
#+END_SRC

The CONDITION will be some *boolean expression*. If the
boolean expression result is *true*, then any code within
this if statement's *code block* (what's between \* and \*)
will get executed. If the result of the expression is *false*,
then the entire if statement's code block is skipped and the program
continues.

Example:

#+BEGIN_SRC cpp :class cpp
  cout << "Bank balance: " << balance;

  if ( balance < 0 )
  {
    cout << " (OVERDRAWN!)";
  }

  cout << " in account #" << accountNumber << endl;
#+END_SRC

Output with =balance = -50=

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Bank balance: -50 (OVERDRAWN!) in account #1234
#+END_SRC

Output with =balance = 100=

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Bank balance: 100 in account #1234
#+END_SRC

- The special message "OVERDRAWN!" only gets displayed when the balance is less than 0.
- The following =cout= that gives the account number is displayed in all cases.




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *If/Else statements*

In some cases, we will have code that we want to execute for the *false*
result as well. For an *if/else* statement, either the *if*
code block will be entered, or the *else* code block will be.

*NOTE:* The else statement NEVER has a CONDITION.

#+BEGIN_SRC cpp :class cpp
  // Do things 1

  if ( CONDITION )
  {
    // Do things 1-a
  }
  else
  {
    // Do things 1-b
  }

  // Do things 2
#+END_SRC

The *else* block is executed when the *if* check fails.
If the CONDITION is false, we can use *DeMorgan's Laws*
to figure out what the program's state is during the *else*.

Example:
#+BEGIN_SRC cpp :class cpp
  cout << "Enter your age: ";
  cin >> age;

  if ( age < 18 )
  {
    result = "can't vote";
  }
  else
  {
    result = "can vote";
  }

  cout << "Result: " << result << endl;
#+END_SRC

If the =age= is less than 18, it will set the =result=
variable to "can't vote". If that boolean expression is *false*,
that means =age= is $\geq$ 18, and then it will set the =result=
to "can vote". Finally, either way, it will display "Result: "
with the value of the =result= variable.






#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *If/Else if/Else statements*

In some cases, there will be multiple scenarios we want to search for,
each with their own logic. We can add as many *else if* statements
as we want - there must be a starting *if* statement, and each
*else if* statement will have a condition as well.


We can also end with a final *else* statement as a catch-all:
if none of the previous if / else if statements are true, then *else*
gets executed instead. However, the else is not required.

#+BEGIN_SRC cpp :class cpp
  // Do things 1
  if ( CONDITION1 )
  {
    // Do things 1-a
  }
  else if ( CONDITION2 )
  {
    // Do things 1-b
  }
  else if ( CONDITION3 )
  {
    // Do things 1-c
  }
  else
  {
    // Do things 1-d
  }
  // Do things 2
#+END_SRC

 Example:

#+BEGIN_SRC cpp :class cpp
  cout << "Enter grade: ";
  cin >> grade;

  if ( grade >= 90 ) 	   	{ letterGrade = 'A'; }
  else if ( grade >= 80 ) { letterGrade = 'B'; }
  else if ( grade >= 70 ) { letterGrade = 'C'; }
  else if ( grade >= 60 ) { letterGrade = 'D'; }
  else				           	{ letterGrade = 'F'; }

  cout << "Grade: " << letterGrade << endl;
#+END_SRC

With this example, I'm first checking if the grade is 90 or above.
If it is, then I know the letter grade is ='A'=.


However, if it's false, it will go on and check the next *else if*
statement. At this point, I know that since =grade >= 90= was FALSE,
that means =grade < 90=. Since the next statement checks if 
=grade >= 80=, if this one is true I know that =grade < 90 AND grade >= 80=.

