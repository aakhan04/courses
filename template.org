# -*- mode: org -*-

#+TITLE: TEMPLATE
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

* Introduction

- About ::
- Goals ::
  - Debugging
- Setup ::

-----


#+BEGIN_SRC cpp :class cpp
#+END_SRC

#+CAPTION: asdfasdf

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal

#+END_SRC

#+ATTR_HTML: :class fileout
#+BEGIN_SRC fileout :class fileout

#+END_SRC


#+begin_src artist

=^o.o^=

#+end_src

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

----------------------------

------------------------------------------------------
* *Review questions:*

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
1. asdfasdf
#+END_HTML


#+BEGIN_LATEX
\newpage
#+END_LATEX

#+BEGIN_LATEX
\vspace{0.2cm}
#+END_LATEX







#+LaTeX_CLASS_OPTIONS: [a4paper,twoside,twocolumn]
#+LATEX_HEADER: \usepackage{xyz}


#+ATTR_HTML: :width 100px
#+ATTR_LATEX: :width 100px
#+ATTR_ORG: :width 100
[[ image ...

- C++ ::
#+BEGIN_SRC cpp :class cpp
#+END_SRC

- Python ::
#+BEGIN_SRC python :class python
#+END_SRC

- Lua ::
#+BEGIN_SRC lua :class lua
#+END_SRC

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*Task:* asdfasdf
#+END_HTML

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
*Context:* asdfasdf
#+END_HTML


#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: What is an *index*?

#+ATTR_HTML: :class hidden-hint
The index is the *position* of an element in the array.
The lowest valid index is 0, and for an array of size /size/,
the highest valid index is /size-1/.
#+END_HTML




#+BEGIN_QUOTE
Quote thing
#+END_QUOTE



#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
LEFT
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
RIGHT
#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML



#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML





#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 highlight-col-6 :style width: 100%;
| Value: | "Cats" | "Dogs" | "Mice" | "Birds" | "Ferrets" | "" |
| Index: |      0 |      1 |      2 |       3 |         4 |  5 |


#+ATTR_HTML: :border 2 :rules all :frame border :class col-7
| Value: | "Cats" | "Dogs" | "Mice" | "Birds" | "Snakes" | "" |
| Index: |      0 |      1 |      2 |       3 |        4 |  5 |


#+ATTR_HTML: :class col-7 ds-queue
|       |   |   |   |      |
|     0 | 1 | 2 | 3 |    4 |
| FRONT |   |   |   | BACK |



UML Diagram:

#+ATTR_HTML: :class uml
| Class Name     |        |
|----------------+--------|
| - =m_variable= | : TYPE |
|----------------+--------|
| + Function()   | : void |



# LaTeX ONLY - Display code with formatting. Hide in the CSS.
# #+ATTR_HTML: :class invisible
# #+NAME: content
# #+BEGIN_HTML

# \begin{lstlisting}[style=code]
#   cout << arr[2];
# \end{lstlisting}

# #+END_HTML


# #+ATTR_HTML: :class invisible
# #+NAME: content
# #+BEGIN_HTML
# \iftoggle{exportlatex}{ LaTeX }{ HTML }
# #+END_HTML

# I want to hide this when exporting for LaTeX, but how??
#+BEGIN_SRC cpp :class cpp latexhide
  cout << arr[2];
#+END_SRC




-----
