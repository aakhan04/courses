p1_greeting, studentA version

# INTRODUCTION (Scroll down further for program instructions)

## REPLIT LAYOUT 
* On the left-hand side is the FILES view.
* Usually the middle column is the CODE view.
* Usually the right column is the CONSOLE/SHELL View.

You can click and drag any of the view names to rearrange them. I tend to prefer my CONSOLE/SHELL beneath the CODE view.

You can also move this instructions.txt file above or beneath the CODE view to be able to read both at the same time.


## SYNTAX ERRORS
Syntax errors happen when you’re trying to build your program but there is something wrong with a command – a typo, or perhaps a character in the wrong spot.

For example:
```
  program1.cpp: In function 'int main(int, char**)':
  program1.cpp:33:3 error: expected '}' at end of input
```

A build error will usually give what line the compiler is confused by, and try to give a message of what went wrong. If you can’t tell what the error message means, try to do a search for it and see what other people have done to fix the error message.

* Work on fixing the TOP-MOST ERROR FIRST. After fixing an error RE-BUILD THE PROGRAM. Sometimes errors create multiple error messages, so fixing one might get rid of several messages.
* Investigate the LINE NUMBER given in the error message. Sometimes the error is on that line, or perhaps the line immediately before or after it.


## WRITING PROGRAMS 
Do not try to implement the entire program all at once – this will cause a ton of build errors and it will be harder to fix everything. Get into the habit of writing only one to three lines of code at a time and building regularly to make sure you don't have syntax errors!


## CODE SHOULD BUILD
Programs submitted that do not build will receive a score of 0%.


## BUILDING YOUR PROGRAM
To build this program, you will need to NAVIGATE INTO THE CORRECT FOLDER/DIRECTORY. You will be working in the CONSOLE OR SHELL (either works) and using some basic COMMAND-LINE COMMANDS to navigate.

ls
List files and folders in the directory. You will start at the directory ~/U02EX-main,
which contains folders studentA, studentB, and studentC.

cd FOLDERNAME
Change Directory to a given folder. 

g++ FILENAME
Uses the g++ COMPILER to build your source code at FILENAME

1. NAVIGATE TO THIS FOLDER IN THE SHELL:
  cd studentA/p1_greeting
2. VIEW FILES:
  ls
3. YOU SHOULD SEE instructions.txt AND main.cpp.
4. BUILD THE PROGRAM:
  g++ main.cpp
5. THE FILE a.out SHOULD BE GENERATED. USE ls TO VIEW FILES.
6. RUN THE PROGRAM:
  ./a.out

## EXAMPLE PROGRAM OUTPUT
```
~/.../studentA/p1_greeting$ ./a.out 
Hi, I'm [NAME]. My favorite color is [COLOR]. My favorite food is [FOOD]. Some of my hobbies are [HOBBY1] and [HOBBY2].
```

-----

# PROGRAM INSTRUCTIONS

Use the `cout` (console-out) command to display text to the screen.
You can also use `endl` to end a line of text so everything doesn't
show up on the same line.

EXAMPLE STATEMENT:
```
cout << "Hello, world!" << endl;
```

You should display the following messages:
```
  Hi, I'm [studentA]!
  My favorite color is [COLOR],
  my favorite food is [FOOD],
  some of my hobbies are [HOBBY1], [HOBBY2]!
```

Replace [studentA] with your name, [COLOR] with a favorite color, and so on.