# 1. Create an integer variable `min` and assign it a value of 1.


# 2. Create an integer variable `max` and assign it a value of 10.


# 3. Tell the user to enter a number between the `min` and the `max`.


# 4. Get the user's input and store it as an integer in a variable `choice`.


# 5. Create a while loop. While `choice` is less than `min` OR `choice` is greater than `max`, do the following:


#  5a. Display "Invalid selection! Try again!"


#  5b. Get the user's input and store it as an the variable `choice`.



# 6. Display "Your selection was:", and then the value from `choice`.




# Say Goodbye at the end of the program!
print( "\n GOODBYE!" )