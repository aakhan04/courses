# Ingredients program

## Starter code

In *main.cpp* the program contains a `vector<Ingredient> ingredients` that will be used
in the program. main also contains a program loop with three options:

``````
--------------------------------------------------------------------------------
INGREDIENTS
ID   INGREDIENT                    #    UNIT
--------------------------------------------------------------------------------

OPTIONS
1. Add ingredient
2. Edit ingredient
3. Exit
>>
``````

main() and the functions are already set up, you just have to implement them.

Note that *Ingredient.h* contains the struct declaration for an ingredient:

```
struct Ingredient
{
  string name;
  float amount;
  string unit;
};
```

## Functions to implement

### void DisplayAllIngredients( const vector<Ingredient>& ingredients )

```
--------------------------------------------------------------------------------
INGREDIENTS
ID   INGREDIENT                    #    UNIT
--------------------------------------------------------------------------------
0    Vanilla Chips                 3    Cups
1    Flour                         4    Cups
2    Baking Soda                   2    Tsps
```

With this function, I've set up the top of the table. Each ingredient should be displayed below, using
a for loop to iterate over all the items in the vector.

Use the `setw( # )` function to set the width of each column. This function goes BEFORE the data it will display.
For example: `cout << setw( 10 ) << "Info";` - this will set the column to 10 characters wide, so even though
"Info" is 4 characters, it will take up the rest of the column with spaces, like "Info______" (except the
spaces are invisible.)

Remember that we're working with an array of ingredients, `vector<Ingredient> ingredients`.
You can access each ingredient with the subscript operator `[ ]` like:
`ingredients[i]`

and since this is a struct, you can use the dot operator `.` to access its internal variables, like:
`ingredients[i].name`.


### void AddIngredient( vector<Ingredient>& ingredients )

```
---------- ADD INGREDIENT --------------------
Enter ingredient name (e.g., Cheese): Choco Chips
Enter unit of measurement (e.g., Cups): Cups
Enter amount of measurement (e.g. 5): 3

Ingredient added!
PRESS ENTER TO CONTINUE
```

This function will ask the user to enter a `newName`, `newUnit`, and `newAmount`,
and then create a new Ingredient item, which it will then store in the `ingredients` vector.
Here are the steps:

*Get the new ingredient's data:**

1. Prompt the user "Enter ingredient name:", use `getline` to get their input and store it in the `newName` variable.
2. Prompt the user "Enter the unit of measurement:", use `getline` to get their input and store it in the `newUnit` variable.
3. Prompt the user "Enter the amount", use `cin >>` to get the user's input and store it in the `newAmount` variable.

*Create a new Ingredient:*
1. [ ] Create a variable named `newIngredient`. Its data type is `Ingredient`.
2. Set `newIngredient.name` to the `newName`.
3. Set `newIngredient.unit` to the `newUnit`.
4. Set `newIngredient.amount` to the `newAmount`.

*Add the ingredient to the vector:
1. For our `ingredients` vector, call its `push_back` function, passing in `newIngredient` as the parameter.


### void EditIngredient( vector<Ingredient>& ingredients )

```
---------- EDIT INGREDIENT --------------------
Edit which ingrediednt? (Enter ID): >> 0

What do you want to edit?
1. NAME    (currently Choco Chips)
2. UNIT    (currently Cups)
3. AMOUNT  (currently 3)
>> 1
Enter new name: Vanilla Chips

Item updated.
PRESS ENTER TO CONTINUE
```

Within this function you'll need to ask the user for the *index* of the ingredient they want to edit.
Then, you'll also give them a menu on whether to update the NAME, UNIT, or AMOUNT.
The user will enter a new value, and then you'll overwrite that ingredient's data with that new data.
Here are the steps:

*Get the index of the item:*
1. Prompt the user "Edit which ingredient? (Enter ID): "
2. Create an integer variable named `id`. Call the `GetChoice` function, passing in 0 as the minimum valid index
   and `ingredients.size()-1` as the maximum valid index.

*Display the menu to the user:*
1. Ask the user "What do you want to edit?"
2. Display option "1. NAME", and what the ingredient's current name is (e.g., `ingredients[id].name`).
3. Display option "2. UNIT", and what the ingredient's current unit is.
4. Display option "3. AMOUNT", and what the ingredient's current amount is.
5. Create an integer `choice` variable and use the `GetChoice` function, passing in 1 as the mininmum and 3 as the maximum.

*Let the user change one item based on their selection:*

If the user's `choice` was 1, ask the user to enter a new name.
Use the `getline` function to get input to the ingredient's name (`ingredients[id].name`).


Else, if the user's `choice` was 2, ask the user to enter a new unit.
Use the `getline` function to get input to the ingredient's unit.


Else, if the user's `choice` was 3, ask the user to enter a new amount.
Use the `cin >>` command to get input to the ingredient's amount.



-----

# Running the program

When you run the program, the ingredient vector will be empty, but once you've added a few items,
the table will show up on the main menu like this:

```
--------------------------------------------------------------------------------
INGREDIENTS
ID   INGREDIENT                    #    UNIT
--------------------------------------------------------------------------------
0    Butter                        2    Sticks
1    Sugar                         5    Cups

OPTIONS
1. Add ingredient
2. Edit ingredient
3. Exit
>>
```

Adding a new ingredient steps the user through the process of entering all the data:

```
---------- ADD INGREDIENT --------------------
Enter ingredient name (e.g., Cheese): Sugar
Enter unit of measurement (e.g., Cups): Cups
Enter amount of measurement (e.g. 5): 5

Ingredient added!
PRESS ENTER TO CONTINUE
```

And editing an ingredient allows the user to fix any mistakes:

```
---------- EDIT INGREDIENT --------------------
Edit which ingrediednt? (Enter ID): >> 1

What do you want to edit?
1. NAME    (currently Sugar)
2. UNIT    (currently Cups)
3. AMOUNT  (currently 5)
>> 3
Enter new amount: 2

Item updated.
PRESS ENTER TO CONTINUE
```
