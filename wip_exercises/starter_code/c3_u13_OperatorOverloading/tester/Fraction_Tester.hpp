#ifndef _TESTER_HPP
#define _TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "TesterBase.hpp"
#include "Helper.hpp"

#include "../Fraction.h"

class Tester : public TesterBase
{
public:
	Tester()
	{
    AddTest( TestListItem( "Test_GetDecimal",                     bind( &Tester::Test_GetDecimal, this ) ) );
    AddTest( TestListItem( "Test_CommonDenominatorize",           bind( &Tester::Test_CommonDenominatorize, this ) ) );
    AddTest( TestListItem( "Test_AssignmentOperator",             bind( &Tester::Test_AssignmentOperator, this ) ) );
    AddTest( TestListItem( "Test_AdditionOperator",               bind( &Tester::Test_AdditionOperator, this ) ) );
    AddTest( TestListItem( "Test_SubtractionOperator",            bind( &Tester::Test_SubtractionOperator, this ) ) );
    AddTest( TestListItem( "Test_MultiplicationOperator",         bind( &Tester::Test_MultiplicationOperator, this ) ) );
    AddTest( TestListItem( "Test_DivisionOperator",               bind( &Tester::Test_DivisionOperator, this ) ) );
    AddTest( TestListItem( "Test_EqualOperator",                  bind( &Tester::Test_EqualOperator, this ) ) );
    AddTest( TestListItem( "Test_NotEqualOperator",               bind( &Tester::Test_NotEqualOperator, this ) ) );
    AddTest( TestListItem( "Test_LessThanOrEqualToOperator",      bind( &Tester::Test_LessThanOrEqualToOperator, this ) ) );
    AddTest( TestListItem( "Test_GreaterThanOrEqualToOperator",   bind( &Tester::Test_GreaterThanOrEqualToOperator, this ) ) );
    AddTest( TestListItem( "Test_LessThanOperator",               bind( &Tester::Test_LessThanOperator, this ) ) );
    AddTest( TestListItem( "Test_GreaterOperator",                bind( &Tester::Test_GreaterOperator, this ) ) );
    AddTest( TestListItem( "Test_OutputStreamOperator",           bind( &Tester::Test_OutputStreamOperator, this ) ) );
    AddTest( TestListItem( "Test_InputStreamOperator",            bind( &Tester::Test_InputStreamOperator, this ) ) );
	}

	virtual ~Tester() { }

private:
	int Test_GetDecimal();
	int Test_CommonDenominatorize();
	int Test_AssignmentOperator();
	int Test_AdditionOperator();
	int Test_SubtractionOperator();
	int Test_MultiplicationOperator();
	int Test_DivisionOperator();
	int Test_EqualOperator();
	int Test_NotEqualOperator();
	int Test_LessThanOrEqualToOperator();
	int Test_GreaterThanOrEqualToOperator();
	int Test_LessThanOperator();
	int Test_GreaterOperator();
	int Test_OutputStreamOperator();
	int Test_InputStreamOperator();
};

#endif
