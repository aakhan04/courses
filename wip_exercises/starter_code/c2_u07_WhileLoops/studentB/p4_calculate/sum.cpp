/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main( int argCount, char* args[] ) 
{
  if ( argCount < 2 ) { cout << "Not enough arguments! Give: low high" << endl; return 1; }
  
  cout << fixed << setprecision( 2 );
  
  int low = stoi( args[1] );
  int high = stoi( args[2] );
  
  // -- PROGRAM CONTENTS GO HERE --------------------------------------------------



  return 0;
}
