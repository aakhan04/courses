--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- PROGRAM LOOPS --
In most programs these days they continue running until the user chooses to QUIT. To do this in our command line programs, we need to use a while loop to keep the program running, until a quit command is chosen - usually from a menu.

Your program loop can look like this:
```
bool done = false;
while ( !done )
{
  // Program code
}
```
In order to quit this program loop, you will need to set `done = true;`.

or:
```
bool running = true;
while ( running )
{
  // Program code
}
```
In order to quit this program loop, you will need to set `running = false;`.


-- MENU CREATION --
It is probably easiest to display a numbered menu for the user to select an option:
1. Display my favorite book
2. Display my favorite movie
3. Display my favorite game
4. Exit program

Then, you can either use a `switch` statement:
```
switch( choice )
{
  case 1:
    // Display favorite book
  break;
  
  case 2:
    // Display favorite movie
  break;
  
  // etc.
}
```

or a series of `if/else if` statements to decide what to execute:
``` 
if ( choice == 1 )
{
  // Display favorite book
}
else if ( choice == 2 )
{
  // Display favorite movie
}
// etc.
```


-- CUSTOMIZING THE PROGRAM --
Feel free to put in YOUR OWN favorite book/movie/game. The automated tests will fail, but the instructor will be able to see the output and see that the program works properly.


-- BUILDING YOUR PROGRAM --
g++ program.cpp -o program.out


-- EXAMPLE PROGRAM OUTPUT --
  -- MAIN MENU --
  1. Display my favorite book
  2. Display my favorite movie
  3. Display my favorite game
  4. Exit program

  SELECTION: 1
  You chose 1
  Masters of Doom

  -- MAIN MENU --
  1. Display my favorite book
  2. Display my favorite movie
  3. Display my favorite game
  4. Exit program

  SELECTION: 2
  You chose 2
  The Lost Skeleton of Cadavra

  -- MAIN MENU --
  1. Display my favorite book
  2. Display my favorite movie
  3. Display my favorite game
  4. Exit program

  SELECTION: 3
  You chose 3
  Divinity Original Sin 2

  -- MAIN MENU --
  1. Display my favorite book
  2. Display my favorite movie
  3. Display my favorite game
  4. Exit program

  SELECTION: 100
  You chose 100
  UNKNOWN COMMAND!

  -- MAIN MENU --
  1. Display my favorite book
  2. Display my favorite movie
  3. Display my favorite game
  4. Exit program

  SELECTION: 4
  You chose 4

  Goodbye.

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

Create a program loop, as illustrated in the introduction above. Within the program loop, do the following:

1. Display a MAIN MENU, such as:
  -- MAIN MENU --
  1. Display my favorite book
  2. Display my favorite movie
  3. Display my favorite game
  4. Exit program

2. Ask the user to enter their SELECTION. Store this in an integer variable.

3. Use a switch statement or if/else if statements to determine what they selected.
  * For 1, display your favorite book.
  * For 2, display your favorite movie.
  * For 3, display your favorite game.
  * For 4, tell your program loop to stop.
  * For anything else, display "UNKNOWN COMMAND!"

4. Outside of the program loop, before `return 0;`, display "Goodbye.".








