--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- FORCING VALID INPUT --
When the user enters a value, such as a number that is supposed to be between a range, we need to do error checking to make sure their input was valid. If we use an if statement, it will only validate their input ONCE. With a while loop, we can ensure that they're asked to enter a new value every time they enter something invalid, and only continue the program once they've entered something NOT invalid.


-- BUILDING YOUR PROGRAM --
g++ options.cpp -o options.out


-- EXAMPLE PROGRAM OUTPUT --
./inrange.out
min: 1, max: 10
Enter a number within the range: 2
Valid input
Enter a number within the range: 3
Valid input
Enter a number within the range: 10
Valid input
Enter a number within the range: 50
Invalid input

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

Create a char variable to store the user's `choice`.

Display the message:
   Options: A, B, C.
   
Ask the user to enter a letter. Store their choice in the `choice` variable.

Use a while statement to check if the letter is A, B, or C.
  -- you should have three CONDITIONS:
  is choice equal to A?
  is choice equal to B?
  is choice equal to C?
Use logic operators to combine these conditions for the if statement.

While `choice` is 'A', 'B', or 'C', display "Valid input", then have the user enter another value for `choice`.
Outside of the while loop is when `choice` is outside of that range. In this case, display "Invalid input" before the `return 0;`.




