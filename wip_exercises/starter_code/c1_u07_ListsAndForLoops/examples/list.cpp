#include <iostream>
#include <vector>
using namespace std;

int main()
{
  vector<string> classes = { "CS134", "CS200", "CS210" };

  cout << "The size of the classes vector is: " << classes.size() << endl;
  cout << "Class 0 is: " << classes[0] << endl;
  cout << "Class 1 is: " << classes[1] << endl;
  cout << "Class 2 is: " << classes[2] << endl;
  
  return 0;
}