# DEFINE CLASS:
# * Name: Ingredient
# * Purpose: Represents a single ingredient of a recipe
#
# * Member variables:
#   * m_name: string
#   * m_amount: float
#   * m_unit: string    Such as "cups", "tsps", etc.
#
# * Member functions (aka methods):
#   * Setup
#      * INPUTS: new_name (string), new_amount (float), new_unit (string)
#      * RETURNS: NONE
#      * FUNCTIONALITY: Set m_name to new_name, m_amount to new_amount, and m_unit to new_unit.
#   * Display
#      * INPUTS: NONE
#      * RETURNS: NONE
#      * Display all the ingredient info in one line (e.g. "1.5 cup(s) butter")



# PROGRAM CODE
# Create a LIST named `ingredients`. Make it an empty list.

# Create 1 Ingredient variable: `new_ing`

# Call Setup on the `new_ing`, setting its name, amount, and unit of measurement.
# Add `new_ing` to the list of `ingredients`.

# Call Setup on the `new_ing` again, setting a new name, amount, and unit of measurement.
# Add `new_ing` to the list of `ingredients`.

# Do this for as many ingredients as you'd like (at least 2).

# Display the name of the recipe to the screen.

# Afterwards, create a for loop (for item in ingredients)... Within the for loop, call the Display function on the item.

