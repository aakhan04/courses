#include "CourseManager.h"
#include <fstream>
#include <iomanip>
#include <iostream>
using namespace std;

// - YOU DON'T NEED TO IMPLEMENT ANYTHING BELOW THIS LINE! -
// ---------------------------------------------------------

void CourseManager::DisplayCourseTable()
{
    DisplayCourseTable(m_courses);
}

void CourseManager::DisplayCourseTable(vector<Course> courses)
{
    cout << left << setw(7) << "ID" << setw(10) << "CODE" << setw(10) << "HOURS"
         << setw(50) << "TITLE" << setw(30) << "DESCRIPTION" << endl
         << string(107, '-') << endl;
    for (unsigned int i = 0; i < courses.size(); i++)
    {
        cout << left << setw(7) << string(to_string(i) + ". ");
        courses[i].TableDisplay();
    }
}

void CourseManager::DisplayCourse(unsigned int index)
{
    if (index < 0 || index >= m_courses.size())
    {
        cout << "ERROR: Invalid index!" << endl;
        return;
    }

    m_courses[index].FullDisplay();
}

void CourseManager::LoadCourses(string filename)
{
    ifstream input(filename);
    Course newCourse;
    string buffer;
    int counter = 0;
    while (getline(input, buffer))
    {
        if (counter % 5 == 0)
        {
            newCourse.SetDepartment(buffer);
        }
        else if (counter % 5 == 1)
        {
            newCourse.SetTitle(buffer);
        }
        else if (counter % 5 == 2)
        {
            newCourse.SetHours(buffer);
        }
        else if (counter % 5 == 3)
        {
            newCourse.SetDescription(buffer);
        }
        else if (counter % 5 == 4)
        {
            m_courses.push_back(newCourse);
        }
        counter++;
    }
}

void CourseManager::SaveCourses(string filename, vector<Course> courses)
{
    ofstream output(filename);
    output << left;
    for (unsigned int i = 0; i < courses.size(); i++)
    {
        output << setw(10) << i << setw(10) << courses[i].GetDepartment() << setw(10)
               << courses[i].GetHours() << setw(50) << courses[i].GetTitle()
               << endl;
    }
}

bool CourseManager::FindText(string searchMe, string findMe)
{
    string lowerCaseSearch = "";
    string lowerCaseFind = "";

    for (unsigned int i = 0; i < searchMe.size(); i++)
    {
        lowerCaseSearch += tolower(searchMe[i]);
    }

    for (unsigned int i = 0; i < findMe.size(); i++)
    {
        lowerCaseFind += tolower(findMe[i]);
    }

    return (lowerCaseSearch.find(lowerCaseFind) != string::npos);
}


struct TestResults
{
    TestResults( string testing )
    {
        this->testing = testing;
        totalTests=0;
        totalPass=0;
        totalFail=0;
    }
    void AddTest()
    {
        totalTests++;
    }
    void AddPass()
    {
        totalPass++;
    }
    void AddFail()
    {
        totalFail++;
    }

    string testing;
    int totalTests;
    int totalPass;
    int totalFail;
};

void CourseManager::RunUnitTests()
{
    vector<TestResults> results;
    cout << left;

    {
        TestResults result( "Test - SearchCoursesByDepartment" );
        result.AddTest();

        cout << setw( 45 ) << "Test - SearchCoursesByDepartment... ";

        // Search by department test
        vector<Course> courseList =
        {
            Course("AAA", "Intro to Potato Peeling", "3",
                   "How to peel potatoes and avoid peeling yourself"),
            Course("AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?"),
            Course("BBB", "Intro to Cheese Grating", "5",
                   "How to grate cheese and avoid grating yourself"),
            Course("CCC", "Cupcakes of Doom", "6",
                   "Just because it's a small cake doesn't mean it's not loaded "
                   "with sugar"),
            Course("CCC", "Cakes and You", "6",
                   "The evolution of Cakes over the history of humankind")
        };

        vector<Course> expectedResult =
        {
            Course("AAA", "Intro to Potato Peeling", "3",
                   "How to peel potatoes and avoid peeling yourself"),
            Course("AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?"),
        };

        vector<Course> actualResult;
        string searchInput = "AAA";

        actualResult = SearchCoursesByDepartment(courseList, searchInput);

        // Check for matches
        bool allMatch = true;
        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "!" << endl;
                break;
            }
            if (expectedResult[i] != actualResult[i])
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Test set of courses: " << endl;
            DisplayCourseTable(courseList);

            cout << endl << "Search term: \"" << searchInput << "\"" << endl;

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourseTable(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourseTable(actualResult);
        }

        cout << "\033[0m";

        results.push_back( result );
    }

    {
        TestResults result( "Test - SearchCoursesByTitle" );
        result.AddTest();

        cout << setw( 45 ) << "Test - SearchCoursesByTitle... ";

        // Search by title test
        vector<Course> courseList =
        {
            Course("AAA", "Intro to Potato Peeling", "3",
                   "How to peel potatoes and avoid peeling yourself"),
            Course("AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?"),
            Course("BBB", "Intro to Cheese Grating", "5",
                   "How to grate cheese and avoid grating yourself"),
            Course("CCC", "Cupcakes of Doom", "6",
                   "Just because it's a small cake doesn't mean it's not loaded "
                   "with sugar"),
            Course("CCC", "Cakes and You", "6",
                   "The evolution of Cakes over the history of humankind")
        };

        vector<Course> expectedResult =
        {
            Course("AAA", "Intro to Potato Peeling", "3",
                   "How to peel potatoes and avoid peeling yourself"),
            Course("BBB", "Intro to Cheese Grating", "5",
                   "How to grate cheese and avoid grating yourself"),
        };

        vector<Course> actualResult;

        string searchInput = "Intro";

        actualResult = SearchCoursesByTitle(courseList, searchInput);

        // Check for matches
        bool allMatch = true;

        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "!" << endl;
                break;
            }
            if (expectedResult[i] != actualResult[i])
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Test set of courses: " << endl;
            DisplayCourseTable(courseList);

            cout << endl << "Search term: \"" << searchInput << "\"" << endl;

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourseTable(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourseTable(actualResult);
        }

        cout << "\033[0m";

        results.push_back( result );
    }

    {
        TestResults result( "Test - SearchCoursesByHours" );
        result.AddTest();

        cout << setw( 45 ) << "Test - SearchCoursesByHours... ";

        // Search by hours test
        vector<Course> courseList =
        {
            Course("AAA", "Intro to Potato Peeling", "3",
                   "How to peel potatoes and avoid peeling yourself"),
            Course("AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?"),
            Course("BBB", "Intro to Cheese Grating", "5",
                   "How to grate cheese and avoid grating yourself"),
            Course("CCC", "Cupcakes of Doom", "6",
                   "Just because it's a small cake doesn't mean it's not loaded "
                   "with sugar"),
            Course("CCC", "Cakes and You", "6",
                   "The evolution of Cakes over the history of humankind")
        };

        vector<Course> expectedResult =
        {
            Course("CCC", "Cupcakes of Doom", "6",
                   "Just because it's a small cake doesn't mean it's not loaded "
                   "with sugar"),
            Course("CCC", "Cakes and You", "6",
                   "The evolution of Cakes over the history of humankind")
        };

        vector<Course> actualResult;

        string searchInput = "6";

        actualResult = SearchCoursesByHours(courseList, searchInput);

        // Check for matches
        bool allMatch = true;
        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "! - TEST FAILS" << endl;
                break;
            }
            if (expectedResult[i] != actualResult[i])
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Test set of courses: " << endl;
            DisplayCourseTable(courseList);

            cout << endl << "Search term: \"" << searchInput << "\"" << endl;

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourseTable(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourseTable(actualResult);
        }

        cout << "\033[0m";

        results.push_back( result );
    }

    {
        TestResults result( "Test - BubbleSortCoursesByTitle" );
        result.AddTest();

        cout << setw( 45 ) << "Test - BubbleSortCoursesByTitle... ";

        // Bubble sort / title test
        vector<Course> courseList =
        {
            Course("asdf", "TitleB", "3", "Lorem ipsum"),
            Course("zxcv", "TitleZ", "4", "Dolor sit amet"),
            Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
            Course("rtyu", "TitleX", "6", "Elit sed do")
        };

        vector<Course> expectedResult =
        {
            Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
            Course("asdf", "TitleB", "3", "Lorem ipsum"),
            Course("rtyu", "TitleX", "6", "Elit sed do"),
            Course("zxcv", "TitleZ", "4", "Dolor sit amet")
        };

        vector<Course> actualResult;

        actualResult = BubbleSortCoursesByTitle(courseList);

        // Check for matches
        bool allMatch = true;
        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "!" << endl;
                break;
            }
            if (expectedResult[i] != actualResult[i])
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Course list before sort: " << endl;
            DisplayCourseTable(courseList);

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourseTable(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourseTable(actualResult);
        }

        cout << "\033[0m";

        results.push_back( result );
    } // End bubble sort test

    {
        TestResults result( "Test - SelectionSortCoursesByDepartment" );
        result.AddTest();

        cout << setw( 45 ) << "Test - SelectionSortCoursesByDepartment... ";

        // Selection sort / department test
        vector<Course> courseList =
        {
            Course("rtyu", "TitleX", "6", "Elit sed do"),
            Course("zxcv", "TitleZ", "4", "Dolor sit amet"),
            Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
            Course("asdf", "TitleB", "3", "Lorem ipsum"),
        };

        vector<Course> expectedResult =
        {
            Course("asdf", "TitleB", "3", "Lorem ipsum"),
            Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
            Course("rtyu", "TitleX", "6", "Elit sed do"),
            Course("zxcv", "TitleZ", "4", "Dolor sit amet")
        };

        vector<Course> actualResult;

        actualResult = SelectionSortCoursesByDepartment(courseList);

        // Check for matches
        bool allMatch = true;
        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "!" << endl;
                break;
            }
            if (expectedResult[i] != actualResult[i])
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Course list before sort: " << endl;
            DisplayCourseTable(courseList);

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourseTable(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourseTable(actualResult);
        }

        cout << "\033[0m";

        results.push_back( result );
    } // End selection sort test

    {
        TestResults result( "Test - InsertionSortCoursesByHours" );
        result.AddTest();

        cout << setw( 45 ) << "Test - InsertionSortCoursesByHours... ";

        // Insertion sort /  test
        vector<Course> courseList =
        {
            Course("rtyu", "TitleX", "6", "Elit sed do"),
            Course("zxcv", "TitleZ", "4", "Dolor sit amet"),
            Course("asdf", "TitleB", "3", "Lorem ipsum"),
            Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
        };

        vector<Course> expectedResult =
        {
            Course("asdf", "TitleB", "3", "Lorem ipsum"),
            Course("zxcv", "TitleZ", "4", "Dolor sit amet"),
            Course("qwer", "TitleA", "5", "Consectetur adipiscing"),
            Course("rtyu", "TitleX", "6", "Elit sed do")
        };

        vector<Course> actualResult;

        actualResult = InsertionSortCoursesByHours(courseList);

        // Check for matches
        bool allMatch = true;
        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "!" << endl;
                break;
            }
            if (expectedResult[i] != actualResult[i])
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Course list before sort: " << endl;
            DisplayCourseTable(courseList);

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourseTable(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourseTable(actualResult);
        }

        cout << "\033[0m";

        results.push_back( result );
    } // End bubble sort test

    cout << endl << string(60, '*') << endl;
    cout << "ALL TEST RESULTS" << endl;

    cout << left << setw( 45 ) << "TEST" << setw( 10 ) << "FAILS" << setw( 10 ) << "PASS" << setw( 10 ) << "TOTAL" << endl;
    cout << string( 65, '-' ) << endl;
    for ( auto& result : results )
    {
        cout << left << setw( 45 ) << result.testing
            << setw( 1 ) << "\033[0;31m"    << setw( 10 ) << result.totalFail
            << setw( 1 ) << "\033[0;32m"    << setw( 10 ) << result.totalPass
            << setw( 1 ) << "\033[0m"       << setw( 10 ) << result.totalTests << endl;
    }
}

unsigned int CourseManager::GetCourseCount()
{
    return m_courses.size();
}

vector<Course> CourseManager::GetCourses()
{
    return m_courses;
}
