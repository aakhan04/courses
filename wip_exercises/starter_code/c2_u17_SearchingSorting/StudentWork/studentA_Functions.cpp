/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include "../Courses/CourseManager.h"
#include "../Utilities/Helper.h"
#include <utility>
using namespace std;

vector<Course> CourseManager::SearchCoursesByDepartment(const vector<Course> &original, string searchTerm)
{
  vector<Course> matches;

  // TODO: IMPLEMENT LINEAR SEARCH, SEARCH BY CODE
  // 1. Use a for loop to iterate over all the items in the "original" vector
  // 2. Inside the for loop, use an if statement to check if the department of the current item is equal to the search term
  // 2a. Within the if statement, if the text is found, then push the current element onto the "matches" vector.
  // 3. Before the function ends, return the "matches" vector.

  return matches;
}

vector<Course> CourseManager::SelectionSortCoursesByDepartment(const vector<Course> &original)
{
  vector<Course> sortCourses = original;

  // TODO: IMPLEMENT SELECTION SORT HERE, SORT BY COURSE HOURS
  // See reading material for the algorithm!

  return sortCourses;
}
