#include "Helper.h"

bool StringContains( std::string haystack, std::string needle, bool caseSensitive /*= true*/ )
{
    std::string a = haystack;
    std::string b = needle;

    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ )
        {
            a[i] = tolower( a[i] );
        }

        for ( unsigned int i = 0; i < b.size(); i++ )
        {
            b[i] = tolower( b[i] );
        }
    }

    return ( a.find( b ) != std::string::npos );
}