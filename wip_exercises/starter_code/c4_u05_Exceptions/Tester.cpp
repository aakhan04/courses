#include "Tester.h"
#include "Exception.h"
#include "Functions.h"

#include <iostream>
#include <string>
#include <array>
using namespace std;

void RunTests()
{
    Test_Divide();
    Test_Display();
    Test_PtrDisplay();
    Test_SlicesPerPerson();
}

void Test_Divide()
{
    cout << string( 20, '-' ) << " Test_Divide() " << string( 20, '-' ) << endl;
    {
        float input_num = 1;
        float input_denom = 2;
        float expected_output = 0.5;
        float actual_output = Divide( input_num, input_denom );

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Divide TEST 1: Check math" << endl;
        cout << "* input_num:        " << input_num << endl;
        cout << "* input_denom:      " << input_denom << endl;
        cout << "* expected_output:  " << expected_output << endl;
        cout << "* actual_output:    " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
    {
        float input_num = 3;
        float input_denom = 4;
        float expected_output = 0.75;
        float actual_output = Divide( input_num, input_denom );

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Divide TEST 2: Check math" << endl;
        cout << "* input_num:        " << input_num << endl;
        cout << "* input_denom:      " << input_denom << endl;
        cout << "* expected_output:  " << expected_output << endl;
        cout << "* actual_output:    " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
    {
        float input_num = 2;
        float input_denom = 0;
        string expected_output = "invalid_argument exception was thrown!";
        string actual_output = "no exception was thrown!";

        try
        {
            Divide( input_num, input_denom );
        }
        catch( const invalid_argument& ex )
        {
            actual_output = "invalid_argument exception was thrown!";
        }
        catch( ... )
        {
            actual_output = "exception was thrown, but wrong type!";
        }

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Divide TEST 3: Check for exception" << endl;
        cout << "* input_num:        " << input_num << endl;
        cout << "* input_denom:      " << input_denom << endl;
        cout << "* expected_output:  " << expected_output << endl;
        cout << "* actual_output:    " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
}

void Test_Display()
{
    cout << string( 20, '-' ) << " Test_Display() " << string( 20, '-' ) << endl;
    {
        array<string, 5> input_arr = { "AB", "CD", "EF", "GH", "IJ" };
        float input_index = -1;
        string expected_output = "out_of_range exception was thrown!";
        string actual_output = "no exception was thrown!";

        try
        {
            Display( input_arr, input_index );
        }
        catch( const out_of_range& ex )
        {
            actual_output = "out_of_range exception was thrown!";
        }
        catch( ... )
        {
            actual_output = "exception was thrown, but wrong type!";
        }

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Display TEST 1: Check for exception" << endl;
        cout << "* input_arr size:   " << input_arr.size() << endl;
        cout << "* input_index:      " << input_index << endl;
        cout << "* expected_output:  " << expected_output << endl;
        cout << "* actual_output:    " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
    {
        array<string, 5> input_arr = { "AB", "CD", "EF", "GH", "IJ" };
        float input_index = input_arr.size();
        string expected_output = "out_of_range exception was thrown!";
        string actual_output = "no exception was thrown!";

        try
        {
            Display( input_arr, input_index );
        }
        catch( const out_of_range& ex )
        {
            actual_output = "out_of_range exception was thrown!";
        }
        catch( ... )
        {
            actual_output = "exception was thrown, but wrong type!";
        }

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Display TEST 2: Check for exception" << endl;
        cout << "* input_arr size:   " << input_arr.size() << endl;
        cout << "* input_index:      " << input_index << endl;
        cout << "* expected_output:  " << expected_output << endl;
        cout << "* actual_output:    " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
}

void Test_PtrDisplay()
{
    cout << string( 20, '-' ) << " Test_PtrDisplay() " << string( 20, '-' ) << endl;
    {
        int input_arr[] = { 1, 2, 3 };
        int input_size = 3;
        int input_index = -1;
        string expected_output = "out_of_range exception was thrown!";
        string actual_output = "no exception was thrown!";

        try
        {
            PtrDisplay( input_arr, input_size, input_index );
        }
        catch( const out_of_range& ex )
        {
            actual_output = "out_of_range exception was thrown!";
        }
        catch( const invalid_argument& ex )
        {
            actual_output = "invalid_argument exception was thrown!";
        }
        catch( ... )
        {
            actual_output = "exception was thrown, but wrong type!";
        }

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Display TEST 1: Check for exception (out of bounds index)" << endl;
        cout << "* input_arr:        " << input_arr << endl;
        cout << "* input_size:       " << input_size << endl;
        cout << "* input_index:      " << input_index << endl;
        cout << "* expected_output:  " << expected_output << endl;
        cout << "* actual_output:    " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
    {
        int input_arr[] = { 1, 2, 3 };
        int input_size = 3;
        int input_index = 3;
        string expected_output = "out_of_range exception was thrown!";
        string actual_output = "no exception was thrown!";

        try
        {
            PtrDisplay( input_arr, input_size, input_index );
        }
        catch( const out_of_range& ex )
        {
            actual_output = "out_of_range exception was thrown!";
        }
        catch( const invalid_argument& ex )
        {
            actual_output = "invalid_argument exception was thrown!";
        }
        catch( ... )
        {
            actual_output = "exception was thrown, but wrong type!";
        }

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Display TEST 2: Check for exception (out of bounds index)" << endl;
        cout << "* input_arr:        " << input_arr << endl;
        cout << "* input_size:       " << input_size << endl;
        cout << "* input_index:      " << input_index << endl;
        cout << "* expected_output:  " << expected_output << endl;
        cout << "* actual_output:    " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
    {
        int* input_arr = nullptr;
        int input_size = 3;
        int input_index = 0;
        string expected_output = "invalid_argument exception was thrown!";
        string actual_output = "no exception was thrown!";

        try
        {
            PtrDisplay( input_arr, input_size, input_index );
        }
        catch( const out_of_range& ex )
        {
            actual_output = "out_of_range exception was thrown!";
        }
        catch( const invalid_argument& ex )
        {
            actual_output = "invalid_argument exception was thrown!";
        }
        catch( ... )
        {
            actual_output = "exception was thrown, but wrong type!";
        }

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Display TEST 3: Check for exception (array is nullptr)" << endl;
        cout << "* input_arr:        " << input_arr << endl;
        cout << "* input_size:       " << input_size << endl;
        cout << "* input_index:      " << input_index << endl;
        cout << "* expected_output:  " << expected_output << endl;
        cout << "* actual_output:    " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
}

void Test_SlicesPerPerson()
{
    cout << string( 20, '-' ) << " Test_SlicesPerPerson() " << string( 20, '-' ) << endl;
    {
        int input_friends = 5;
        int input_pizzaSlices = 10;
        string expected_output = "no exception was thrown!";
        string actual_output = "no exception was thrown!";

        try
        {
            SlicesPerPerson( input_friends, input_pizzaSlices );
        }
        catch( const NotEnoughFriendsException& ex )
        {
            actual_output = "NotEnoughFriendsException exception was thrown!";
        }
        catch( const NotEnoughPizzaException& ex )
        {
            actual_output = "NotEnoughPizzaException exception was thrown!";
        }
        catch( ... )
        {
            actual_output = "exception was thrown, but wrong type!";
        }

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Display TEST 1: Check that no exception is thrown for non-zero inputs" << endl;
        cout << "* input_friends:       " << input_friends << endl;
        cout << "* input_pizzaSlices:   " << input_pizzaSlices << endl;
        cout << "* expected_output:     " << expected_output << endl;
        cout << "* actual_output:       " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
    cout << string( 20, '-' ) << " Test_SlicesPerPerson() " << string( 20, '-' ) << endl;
    {
        int input_friends = 0;
        int input_pizzaSlices = 10;
        string expected_output = "NotEnoughFriendsException exception was thrown!";
        string actual_output = "no exception was thrown!";

        try
        {
            SlicesPerPerson( input_friends, input_pizzaSlices );
        }
        catch( const NotEnoughFriendsException& ex )
        {
            actual_output = "NotEnoughFriendsException exception was thrown!";
        }
        catch( const NotEnoughPizzaException& ex )
        {
            actual_output = "NotEnoughPizzaException exception was thrown!";
        }
        catch( ... )
        {
            actual_output = "exception was thrown, but wrong type!";
        }

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Display TEST 2: Check for exception when 0 friends" << endl;
        cout << "* input_friends:       " << input_friends << endl;
        cout << "* input_pizzaSlices:   " << input_pizzaSlices << endl;
        cout << "* expected_output:     " << expected_output << endl;
        cout << "* actual_output:       " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
    cout << string( 20, '-' ) << " Test_SlicesPerPerson() " << string( 20, '-' ) << endl;
    {
        int input_friends = 5;
        int input_pizzaSlices = 0;
        string expected_output = "NotEnoughPizzaException exception was thrown!";
        string actual_output = "no exception was thrown!";

        try
        {
            SlicesPerPerson( input_friends, input_pizzaSlices );
        }
        catch( const NotEnoughFriendsException& ex )
        {
            actual_output = "NotEnoughFriendsException exception was thrown!";
        }
        catch( const NotEnoughPizzaException& ex )
        {
            actual_output = "NotEnoughPizzaException exception was thrown!";
        }
        catch( ... )
        {
            actual_output = "exception was thrown, but wrong type!";
        }

        if ( actual_output == expected_output ) { cout << "\033[0;32m"; cout << "[PASS] "; }
        else                                    { cout << "\033[0;31m"; cout << "[FAIL] "; }

        cout << "Display TEST 3: Check for exception when 0 pizza" << endl;
        cout << "* input_friends:       " << input_friends << endl;
        cout << "* input_pizzaSlices:   " << input_pizzaSlices << endl;
        cout << "* expected_output:     " << expected_output << endl;
        cout << "* actual_output:       " << actual_output << endl;
        cout << endl;
        cout << "\033[0m";
    }
}

