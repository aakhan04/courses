#include <iostream>
#include <cstdlib>
using namespace std;

#include "Functions.h"
#include "Programs.h"
#include "Tester.h"

int main()
{
    cout << "----------------------------------------" << endl;
    cout << "- MAIN MENU                            -" << endl;
    cout << "----------------------------------------" << endl;
    cout << "- 0. EXIT                              -" << endl;
    cout << "----------------------------------------" << endl;
    cout << "- 1. RUN TESTS                         -" << endl;
    cout << "----------------------------------------" << endl;
    cout << "- 2. Division example                  -" << endl;
    cout << "- 3. Array example                     -" << endl;
    cout << "- 4. Multiple exceptions example       -" << endl;
    cout << "- 5. Custom example                    -" << endl;
    cout << "----------------------------------------" << endl;
    cout << endl << "SELECTION: ";
    int choice;
    cin >> choice;

    cout << endl << endl;
    cout << "-------------[SELECTED " << choice << "]-------------" << endl;

    switch (choice)
    {
    case 1:     RunTests();         break;
    case 2:     Program1();         break;
    case 3:     Program2();         break;
    case 4:     Program3();         break;
    case 5:     Program4();         break;
    }

    cout << endl << endl;

  return 0;
}
