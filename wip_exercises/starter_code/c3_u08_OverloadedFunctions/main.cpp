#include "Product.h"

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  cout << left;
  
  cout << endl << "Test 1: Set up product with default constructor" << endl;
  Product product1;
  cout << setw( 30 ) << "EXPECTED Name:     UNSET"  << setw( 15 ) << "ACTUAL Name:     " << product1.GetName() << endl;
  cout << setw( 30 ) << "EXPECTED Quantity: 0"      << setw( 15 ) << "ACTUAL Quantity: " << product1.GetQuantityInStock() << endl;
  cout << setw( 30 ) << "EXPECTED Price:    0"      << setw( 15 ) << "ACTUAL Price:    " << product1.GetPrice() << endl;
  
  cout << endl << "Test 2: Set up product with parameterized constructor" << endl;
  Product product2( "ASDF", 123, 200 );
  cout << setw( 30 ) << "EXPECTED Name:     ASDF"   << setw( 15 ) << "ACTUAL Name:     " << product2.GetName() << endl;
  cout << setw( 30 ) << "EXPECTED Quantity: 123"    << setw( 15 ) << "ACTUAL Quantity: " << product2.GetQuantityInStock() << endl;
  cout << setw( 30 ) << "EXPECTED Price:    200"    << setw( 15 ) << "ACTUAL Price:    " << product2.GetPrice() << endl;
  
  cout << endl << "Test 3: Set up product with copy constructor" << endl;
  Product product3( product2 );
  cout << setw( 30 ) << "EXPECTED Name:     ASDF"   << setw( 15 ) << "ACTUAL Name:     " << product3.GetName() << endl;
  cout << setw( 30 ) << "EXPECTED Quantity: 123"    << setw( 15 ) << "ACTUAL Quantity: " << product3.GetQuantityInStock() << endl;
  cout << setw( 30 ) << "EXPECTED Price:    200"    << setw( 15 ) << "ACTUAL Price:    " << product3.GetPrice() << endl;
  
  cout << endl << "Test 4: Call 3-parameter Setup on a Product..." << endl;
  product1.Setup( "NAME", 20, 30 );
  cout << setw( 30 ) << "EXPECTED Name:     NAME"    << setw( 15 ) << "ACTUAL Name:     " << product1.GetName() << endl;
  cout << setw( 30 ) << "EXPECTED Quantity: 20"      << setw( 15 ) << "ACTUAL Quantity: " << product1.GetQuantityInStock() << endl;
  cout << setw( 30 ) << "EXPECTED Price:    30"      << setw( 15 ) << "ACTUAL Price:    " << product1.GetPrice() << endl;
  
  cout << endl << "Test 5: Call 1-parameter Setup on a Product..." << endl;
  product2.Setup( "ITEM" );
  cout << setw( 30 ) << "EXPECTED Name:     ITEM"   << setw( 15 ) << "ACTUAL Name:     " << product2.GetName() << endl;
  cout << setw( 30 ) << "EXPECTED Quantity: 123"    << setw( 15 ) << "ACTUAL Quantity: " << product2.GetQuantityInStock() << endl;
  cout << setw( 30 ) << "EXPECTED Price:    200"    << setw( 15 ) << "ACTUAL Price:    " << product2.GetPrice() << endl;
  
	return 0;
}
