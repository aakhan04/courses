--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- BUILDING YOUR PROGRAM --
cd studentB/p2_formula
g++ pythagorean.cpp -o pythagorean.out

The default program name is "a.out", but we can change the program's name by putting the "-o" flag, followed by our custom program name.


-- EXAMPLE PROGRAM OUTPUT --
When we run this program, we will provide two arguments, two different numbers. The program will then do a mathematical operation and display the result.

$ ./pythagorean.out 4 3
c^2=25

$ ./pythagorean.out 2 6
c^2=40

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------
We can use the `stof` function to convert one of the `args[]` from a string to a float variable:

float varname = stof( args[1] );

Create two float variables, such as `num1` and `num2`, and then load the following arguments into each one:

args[1]     args[2]   
a           b     

Create a third float to store the value of c^2 (c squared). Use this formula:

cSquared = a*a + b*b

We are using `a*a` because that's equivalent to a^2, and we don't have to add in the math library, which will just take up extra space.

Finally, display the result to the screen, like this:

c^2=40






