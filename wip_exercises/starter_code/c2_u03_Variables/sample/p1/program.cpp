#include <iostream>
using namespace std;

int main( int argCount, char* args[] )
{
  if ( argCount < 2 ) { cout << "Not enough arguments! Give: number" << endl; return 1; }

  float number = stof(args[1]);
  float doubleNumber = number * 2;

  cout << "Double is: " << doubleNumber << endl;

  return 0;
}