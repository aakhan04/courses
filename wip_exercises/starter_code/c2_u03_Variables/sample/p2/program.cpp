#include <iostream>
using namespace std;

int main( int argCount, char* args[] )
{
  if ( argCount < 3 ) { cout << "Not enough arguments! Expected: numerator denominator" << endl; return 1; }
  
  // -- PROGRAM CONTENTS GO HERE --------------------------------------------------
  float numerator = stof( args[1] );
  float denominator = stof( args[2] );
  float ratio = numerator / denominator;

  cout << numerator << "/" << denominator << "=" << ratio << endl;
  
  return 0;
}