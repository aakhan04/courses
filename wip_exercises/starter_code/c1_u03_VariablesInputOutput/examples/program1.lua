print( "What is your name?" )
name = io.read( "*l" )
print( "Hello,", name )
print()

print( "Enter a first number:" )
x = io.read( "*n" )
print( "Enter a second number:" )
y = io.read( "*n" )

sum = x + y
difference = x - y
product = x * y
quotient = x / y

print( x, "+", y, "=", sum )
print( x, "-", y, "=", difference )
print( x, "*", y, "=", product )
print( x, "/", y, "=", quotient )