echo ""
echo -e "\033[0;31m **************************************"
echo -e " YOU HAVE PRESSED THE \"RUN\" BUTTON! "
echo -e " \"RUN\" is used to run the EXAMPLES!! "
echo ""
echo -e " To run YOUR PROGRAM, use one of the following commands in the SHELL:"
echo -e "python3 recipe.py"
echo -e "lua recipe.lua"
echo -e "g++ recipe.cpp && ./a.out"
echo -e "\033[0;31m **************************************"
echo -e "\033[0m"
echo ""
echo "EXAMPLES!"
echo ""
echo "Building C++ program... (Please wait)"
cd examples
g++ program1.cpp
echo "-------------------------"
echo "Running C++ program..."
echo "-------------------------"
./a.out

echo ""
echo "PRESS ENTER TO CONTINUE"
read asdf

echo ""
echo "-------------------------"
echo "Running Python program..."
echo "-------------------------"
python3 program1.py

echo ""
echo "PRESS ENTER TO CONTINUE"
read asdf

echo ""
echo "-------------------------"
echo "Running Lua program..."
echo "-------------------------"
lua program1.lua
