#include <iostream>
using namespace std;

int main()
{
  // Copy/paste your recipe from the previous exercise and make the modifications outlined in the instructions.
  // If you don't have a recipe program done from last week, you can use this as your starter:
  cout << "Butter recipe" << endl;
  cout << "" << endl;
  cout << "INGREDIENTS" << endl;
  cout << "* 1 stick - Butter" << endl;
  cout << "* 5 tbsp - Salt" << endl;
  cout << "" << endl;
  cout << "STEPS" << endl;
  cout << "1. Put butter and salt in microwave safe bowl" << endl;
  cout << "2. Microwave butter" << endl;
  cout << "3. Enjoy!!" << endl;


  
  return 0;
}