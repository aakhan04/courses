#! /bin/bash
#clear
# $? = Specifies the exit status of the last command or the most recent execution process.
RED='\033[0;31m'; GREEN='\033[0;32m'; NC='\033[0m' # No Color
ACOL='\033[0;34m'; BCOL='\033[0;35m'; CCOL='\033[0;36m'
STUDENTCOL=CCOL
student='studentC'

programs=("${student}/p1_NoInNoOut/" "${student}/p2_YesInNoOut/" "${student}/p3_NoInYesOut/" "${student}/p4_YesInYesOut/" "${student}/p5_AllTogether/")
executables=()

declare -A TESTS
# TEST 0 - PROGRAM 0 ----------------------------------------------
i=0
TESTS[$i,0]=0                                      # programs index
TESTS[$i,1]=""                                     # arguments
TESTS[$i,2]="Recommended TV show: The Secret Rules of Modern Living: Algorithms"  # expected output
TESTS[$i,3]=""                                     # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 1 - PROGRAM 1 ----------------------------------------------
i=$((i+1))
TESTS[$i,0]=1                                      # programs index
TESTS[$i,1]=""                                     # arguments
TESTS[$i,2]="Enter numerator: Enter denominator: 2/3"    # expected output
TESTS[$i,3]="2 3"                                  # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 2 - PROGRAM 1 
i=$((i+1))
TESTS[$i,0]=1                                      # programs index
TESTS[$i,1]=""                                     # arguments
TESTS[$i,2]="Enter numerator: Enter denominator: 4/8"     # expected output
TESTS[$i,3]="4 8"                                  # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 3 - PROGRAM 2 ----------------------------------------------
i=$((i+1))
TESTS[$i,0]=2                                      # programs index
TESTS[$i,1]=""                                     # arguments
TESTS[$i,2]="Error code is: 404"                   # expected output
TESTS[$i,3]=""                                     # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 4 - PROGRAM 3 ----------------------------------------------
i=$((i+1))
TESTS[$i,0]=3                                      # programs index
TESTS[$i,1]=""                                     # arguments
TESTS[$i,2]="Enter numerator: Enter denominator: As a decimal, it's: 0.5" # expected output
TESTS[$i,3]="1 2"                                  # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 5 - PROGRAM 3
i=$((i+1))
TESTS[$i,0]=3                                      # programs index
TESTS[$i,1]=""                                     # arguments
TESTS[$i,2]="Enter numerator: Enter denominator: As a decimal, it's: 0.75" # expected output
TESTS[$i,3]="3 4"                                  # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 6 - PROGRAM 4 ----------------------------------------------
i=$((i+1))
TESTS[$i,0]=4                                      # programs index
TESTS[$i,1]=""                                     # arguments
TESTS[$i,2]="What is the price of the item? \$What is the amount the customer paid? \$Change back: \$-5.05" # expected output
TESTS[$i,3]="14.95 20"                             # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 7 - PROGRAM 4
i=$((i+1))
TESTS[$i,0]=4                                      # programs index
TESTS[$i,1]=""                                     # arguments
TESTS[$i,2]="What is the price of the item? \$What is the amount the customer paid? \$Change back: \$-43.00" # expected output
TESTS[$i,3]="7.75 50.75"                           # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?

i=$((i+1))
TOTALTESTS=$i

# ---------------------------------------------------------------------- WHICH STUDENT?
echo -e "\n TESTS FOR ${student}"; head -1 "${programs[0]}*.cpp"
# ---------------------------------------------------------------------- BUILD PROGRAMS
echo -e "\n === BUILD PROGRAMS ==="
for i in ${!programs[@]}; do
  niceIndex=$((i+1))
  outname="${programs[i]}${student}_program$niceIndex.out"
  g++ ${programs[i]}*.cpp -o ${outname}
  if [ "$?" -eq 0 ]; then
    echo -e "${GREEN}Successfully built [${outname}] ${NC}"
    executables+=(${outname})
  else
    echo -e "${RED}FAILED to build [${outname}]${NC}"
  fi  
done
# ---------------------------------------------------------------------- TEST PROGRAMS
echo -e "\n === TEST PROGRAMS ==="
for ((i=0; i<${TOTALTESTS}; i++))
do  
  PROGRAMI=${TESTS[$i,0]}
  PROGARGS=${TESTS[$i,1]}
  EXPECTED=${TESTS[$i,2]}
  RUNTIMEV=${TESTS[$i,3]}
  TESTTYPE=${TESTS[$i,4]}
  EXECUTBL=${executables[${PROGRAMI}]}

  RUNNER="./$EXECUTBL"
  if [ "$PROGARGS" != "" ]; then
    RUNNER="./$EXECUTBL $PROGARGS"
  elif [ "$RUNTIMEV" != "" ]; then
    RUNNER="echo $RUNTIMEV | ./$EXECUTBL"
  fi
  
    
  if [ "$TESTTYPE" == "manual" ]; then
    echo -e "TEST ${i}: (MANUAL-CHECK TEST)"
    eval "$RUNNER"
    echo -e "* EXPECTED OUTPUT: [${EXPECTED}]"
    
  else

    ACTUAL=$(eval "$RUNNER")
    if [ "${ACTUAL}" == "${EXPECTED}" ]; then
      echo -e "* ${GREEN}TEST ${i} SUCCESS! PROGRAM:[$RUNNER] OUTPUT: [${ACTUAL}]${NC}"
    else
      echo -e "* ${RED}TEST #${i} FAIL! PROGRAM:[$RUNNER] \n\t* EXPECTED OUTPUT: [${EXPECTED}] \n\t* ACTUAL OUTPUT:   [$ACTUAL]${NC}"
    fi
  fi
    
done
