--------------------------------------------------------

# INTRODUCTION

(Scroll down further for program instructions)

## BUILDING YOUR PROGRAM

use `cd` to navigate to the folder
use `g++` to build the code:

`g++ *.cpp *.h -o PROGRAMNAME.out`

## EXAMPLE PROGRAM OUTPUT

Runner:

```
./program.out
PRODUCTS
ID   PRICE     CALORIES  NAME
--------------------------------------------------------------------------------
1    1.89      180       Soft Taco
2    4.49      720       Grilled Cheese Burrito
3    5.39      520       Chicken Quesadilla
```

--------------------------------------------------------

# PROGRAM INSTRUCTIONS

## Grab your work from the struct lab

Copy your `main.cpp`, `Product.h`, and `Product.cpp` files from your struct lab.
We're going to reuse a lot of this code, but we're turning the `struct` into a `class`.

## CONVERTING Product FROM STRUCT TO CLASS

Our struct looked like this:

```
struct Product
{
  int productId;
  string name;
  float price;
  int calories;
};
```

But changing it to a class, we need to mark those member variables as `private`,
and create some `public` functions to interface with them. This helps us protect our data.

Update the class to look like this:

```
class Product
{
  public:
  void Setup( int newProdId, string newName, float newPrice, int newCalories );
  void PrintTableRow() const;
  
  private:
  int productId;
  string name;
  float price;
  int calories;
};

```

We have two functions now: A `Setup` function that takes in values for all the class' information,
and a `PrintTableRow` function that will handle displaying the variables to the screen.

## DEFINING OUR MEMBER FUNCTIONS (METHODS)

### Setup

Within the *.cpp* file, we're going to define our member functions.

Defining a function takes this form:

```
RETURNTYPE CLASSNAME::FUNCTIONNAME( PARAMETERS )
{
}
```

So, for the Setup function, it will start like this:

```
void Product::Setup( int newProdId, string newName, float newPrice, int newCalories )
{
}
```

Notice that we've put `Product::` before the function name `Setup`.

Within this function, we want to give new values to our *private member variables*:
`productId`, `name`, `price`, and `calories`.

The function takes in several *input parameters*, carrying new values:
`newProdId`, `newName`, `newPrice`, `and `newCalories`.

With each *private member variable*, write an assignment statement to set its new value
to the corresponding *input parameter variable*.


### PrintTableRow

For this function, cut part of the `PrintProductsTable` function where it's displaying
one of the items, like:

```
  cout
    << setw( 5 ) << p1.productId
    << setw( 10 ) << p1.price
    << setw( 10 ) << p1.calories
    << setw( 50 ) << p1.name
    << endl;
```

Paste it into the `PrintTableRow` function. We don't need the "`l1.`" anymore, so remove that
part, but the rest of it should be good for displaying the variables.


```
void Product::PrintTableRow() const
{
  cout
    << setw( 5 ) << productId
    << setw( 10 ) << price
    << setw( 10 ) << calories
    << setw( 50 ) << name
    << endl;
}
```

## UPDATING THE FUNCTIONS FROM BEFORE

### SetupLocations

Before, we were setting the member variables directly, like this:

```
  p1.productId = 1;
  p1.name = "Soft Taco";
  p1.price = 1.89;
  p1.calories = 18;
```

Now those variables are private, so they must be set by calling the `Setup` function:

```
p1.Setup( VALUE1, VALUE2, VALUE3, VALUE4 );`
```

Change all of the variables being set up (`p1`, `p2`, `p3`) to use the Setup function instead,
using the same data as before.

### PrintProductTable

For this function we will keep the header part of the table display, but we don't need
to have cout statements for `p1`, `p2`, and `p3` anymore. Instead, we call each of their
`PrintTableRow` functions:

```
p1.PrintTableRow();
```

Do this for all variables.
