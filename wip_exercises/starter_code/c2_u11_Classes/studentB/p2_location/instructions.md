--------------------------------------------------------

# INTRODUCTION

(Scroll down further for program instructions)

## BUILDING YOUR PROGRAM

use `cd` to navigate to the folder
use `g++` to build the code:

`g++ *.cpp *.h -o PROGRAMNAME.out`

## EXAMPLE PROGRAM OUTPUT

Runner:

```
./program.out
LOCATIONS
ID   SALES TAX      ADDRESS
--------------------------------------------------------------------------------
1    9.61           12208 College Blvd, Overland Park, KS 66210
2    8.49           9021 E State Rte 350, Raytown, MO 64133
```

--------------------------------------------------------

# PROGRAM INSTRUCTIONS

## Grab your work from the struct lab

Copy your `main.cpp`, `Location.h`, and `Location.cpp` files from your struct lab.
We're going to reuse a lot of this code, but we're turning the `struct` into a `class`.

## CONVERTING Location FROM STRUCT TO CLASS

Our struct looked like this:

```
struct Location
{
  int locationId;
  string streetAddress;
  float salesTaxPercent;
};
```

But changing it to a class, we need to mark those member variables as `private`,
and create some `public` functions to interface with them. This helps us protect our data.

Update the class to look like this:

```
class Location
{
  public:
  void Setup( int newLocId, string newAddress, float newTax );  
  void PrintTableRow() const;
  
  private:
  int locationId;
  string streetAddress;
  float salesTaxPercent;
};
```

We have two functions now: A `Setup` function that takes in values for all the class' information,
and a `PrintTableRow` function that will handle displaying the variables to the screen.

## DEFINING OUR MEMBER FUNCTIONS (METHODS)

### Setup

Within the *.cpp* file, we're going to define our member functions.

Defining a function takes this form:

```
RETURNTYPE CLASSNAME::FUNCTIONNAME( PARAMETERS )
{
}
```

So, for the Setup function, it will start like this:

```
void Location::Setup( int newLocId, string newAddress, float newTax )
{
}
```

Notice that we've put `Location::` before the function name `Setup`.

Within this function, we want to give new values to our *private member variables*:
`locationId`, `streetAddress`, and `hourlyWage`.

The function takes in several *input parameters*, carrying new values:
`newEmpId`, `newName`, and `salesTaxPercent`.

With each *private member variable*, write an assignment statement to set its new value
to the corresponding *input parameter variable*.


### PrintTableRow

For this function, cut part of the `PrintLocationsTable` function where it's displaying
one of the items, like:

```
  cout
    << setw( 5 ) << l1.locationId
    << setw( 15 ) << l1.salesTaxPercent
    << setw( 60 ) << l1.streetAddress
    << endl;
```

Paste it into the `PrintTableRow` function. We don't need the "`l1.`" anymore, so remove that
part, but the rest of it should be good for displaying the variables.


```
void Location::PrintTableRow() const
{
  cout
    << setw( 5 ) << locationId
    << setw( 15 ) << salesTaxPercent
    << setw( 60 ) << streetAddress
    << endl;
}
```

## UPDATING THE FUNCTIONS FROM BEFORE

### SetupLocations

Before, we were setting the member variables directly, like this:

```
  l1.locationId = 1;
  l1.streetAddress = "12208 College Blvd, Overland Park, KS 66210";
  l1.salesTaxPercent = 9.61;
```

Now those variables are private, so they must be set by calling the `Setup` function:

```
l1.Setup( VALUE1, VALUE2, VALUE3 );`
```

Change all of the variables being set up (`l1`, `l2`, `l3`) to use the Setup function instead,
using the same data as before.

### PrintEmployeeTable

For this function we will keep the header part of the table display, but we don't need
to have cout statements for `l1`, `l2`, and `l3` anymore. Instead, we call each of their
`PrintTableRow` functions:

```
l1.PrintTableRow();
```

Do this for all variables.
