/* STUDENT NAME:
 * SEMESTER/YEAR:
 * */
#include "Employee.h"
#include <iostream>
#include <iomanip>
using namespace std;

// -- CLASS FUNCTION DEFINITIONS -----------------------------------------------------

void Employee::Setup( int newEmpId, int newLocId, string newName, float newWage )
{
  // TODO: Implement me!
}

void Employee::PrintTableRow() const
{
  // TODO: Implement me!
}

// -- ADD FUNCTION DEFINITIONS HERE --------------------------------------------------
void SetupEmployees( Employee& e1, Employee& e2, Employee& e3 )
{
  // TODO: Implement me!
}

void PrintEmployeeTable( const Employee& e1, const Employee& e2, const Employee& e3 )
{
  cout << left;
  cout << "EMPLOYEES" << endl;
  cout
    << setw( 5 ) << "ID"
    << setw( 10 ) << "LOCATION"
    << setw( 10 ) << "WAGE"
    << setw( 50 ) << "NAME"
    << endl << string( 80, '-' ) << endl;
    
  // TODO: Implement me!
}
