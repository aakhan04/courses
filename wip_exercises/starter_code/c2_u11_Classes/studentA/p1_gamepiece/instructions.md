--------------------------------------------------------

# INTRODUCTION

(Scroll down further for program instructions)

## CLASSES

Like structs, classes are a way we can create our own data type and group related variables together under one name. With classes, we can also add MEMBER FUNCTIONS (aka METHODS) to the class declaration. These functions belong to the class, and are there to operate on the MEMBER VARIABLES within it. (Structs also support functions, but the best practice is to only use structs for very small structures).

Class declarations look like this:

```
class CLASSNAME
{
  public:
    // PUBLIC MEMBERS
  private:
    // PRIVATE MEMBERS
}; // << DON'T FORGET THE ;
```

## UML DIAGRAMS FOR CLASSES

| Day                               | << Class name                            |
| --------------------------------- | ---------------------------------------- |
| `- m_num : int`                   | << Member variables go here              |
| `-m_month : string`               | << "`-`" denotes "private" accessibility |
| --------------------------------- |                                          |
| `+ SetNum( newNum : int ) : void` | << Member functions (aka methods)        |
| `+GetNum() : int`                 |                                          |
| `+Display() : void`               |                                          |

**Parts of a UML diagram:** With UML diagrams, the top section is for the CLASS NAME. The middle contains the MEMBER VARIABLES. And the bottom contains the MEMBER FUNCTIONS (aka METHODS).

**Accessibility levels:** The "-" sign denotes that the member variable or function should be `private`, and the "+" sign denotes `public`.

**NAME : TYPE**  UML items are written in this format:  `VARNAME : TYPE` , this is to be
programming language agnostic, so it can be translated into other languages. You will
have to "translate" the UML to C++ code, this is an important part of software development.

| UML                               | C++ translation            |
| --------------------------------- | -------------------------- |
| `- total_cats : int`              | `int total_cats;`          |
| `+ Display() : void`              | `void Display();`          |
| `+ GetCatCount() : int`           | `int GetCatCount();`       |
| `+ Add( a : int, b : int ) : int` | `int Add( int a, int b );` |

## CLASS MEMBER VARIABLE NAMES

While not required, I prefix my CLASS MEMBER VARIABLES with the `m_` prefix (which stands for "member"). There are a couple of benefits to this:

* Easily tell what variables are MEMBER VARIABLES vs. function parameters or local variables.
* If a function contains a parameter with the same name ("month") you don't have a naming conflict with the member variable ("m_month").

## CLASS MEMBER FUNCTIONS (METHODS) DEFINITIONS

The function definitions for class' member functions looks a little different from functions we've used so far. In particular, EVERY FUNCTION NAME needs to be prefixed with the CLASS NAME and then `::`, the SCOPE RESOLUTION OPERATOR.
For example:

```
void Day::Display()
{
  cout << m_num << " of " << m_month << endl;
}
```

## BUILDING YOUR PROGRAM

use `cd` to navigate to the folder
use `g++` to build the code:

`g++ *.cpp *.h -o PROGRAMNAME.out`

## EXAMPLE PROGRAM OUTPUT

Runner:

```
./program.out run
How many sides? 6
Roll how many times? 3
Roll #0: 5
Roll #1: 6
Roll #2: 2
```

Tester:

```
./program.out test
DICE TESTS
 [PASS] TEST 1: The Die default constructor should initialize sides to 6.
     Die die;
     EXPECTATION:    die.m_sides to be 6
     ACTUAL:         die.m_sides is 6

 [PASS] TEST 2: The Die parameterized constructor should initialize sides to amount passed in.
     Die die(12);
     EXPECTATION:    die.m_sides to be 12
     ACTUAL:         die.m_sides is 12

 [PASS] TEST 3: The Die parameterized constructor should initialize sides to amount passed in.
     Die die(20);
     EXPECTATION:    die.m_sides to be 20
     ACTUAL:         die.m_sides is 20

 [PASS] TEST 4: The Die roll should return a value between 1 and the amount of sides.
     Die die;
     EXPECTATION:    die.Roll(); returns a result that should be between 1 and 6
     ACTUAL:         die.Roll() returned result: 3

 [PASS] TEST 5: The Die roll should return a value between 1 and the amount of sides.
     Die die;
     EXPECTATION:    die.Roll(); returns a result that should be between 1 and 20
     ACTUAL:         die.Roll() returned result: 3
```

--------------------------------------------------------

# PROGRAM INSTRUCTIONS

## CLASS DECLARATION

Within Die.h a class declaration has already been written:

```
class Die
{
    public:
    Die();                    // Initialize die with default info
    Die( int sideCount );     // Initialize die with some # of sides
    int Roll();               // Roll the die, return the result

    private:
    int m_sides;              // The amount of sides this die has


    friend void DiceTest();   // This just gives access to the UNIT TESTS, don't worry about it. (CS235 topic).
};
```

You will be implementing the CLASS METHODS, which are in the corresponding SOURCE (.cpp) FILE.

## CLASS MEMBER FUNCTION DEFINITIONS

1. `Die::Die() constructor`
    **Input parameters:** newRank - a string, newSuit - a char
    **Output return:** void
    **Functionality:** By default, set `m_sides` to 6 to create a standard 6-sided die.

2.  `Die::Die( int newSideCount ) constructor`
    **Input parameters:** `newSideCount` - an integer
    **Functionality:** Copy the value from the input `sideCount` to the
    private member variable `m_sides`.

2. `int Die::Roll() function`
    **Input parameters:** none
    **Output return:** void
    **Functionality:** 
    Generate a random value between 1 and the amount of sides using this command:
    `rand() % m_sides + 1`
    and return the result of the roll

## MAIN PROGRAM

The contents of `main()` have also already been written. Running the program requires a second argument, either "run" to run a simple program or "test" to run the unit tests.
You do not need to make any modifications to main.cpp, but look through the file and look at how the class variable object is created and used.
