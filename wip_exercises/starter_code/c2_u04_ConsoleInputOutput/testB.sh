#! /bin/bash
#clear
# $? = Specifies the exit status of the last command or the most recent execution process.
RED='\033[0;31m'; GREEN='\033[0;32m'; NC='\033[0m' # No Color
ACOL='\033[0;34m'; BCOL='\033[0;35m'; CCOL='\033[0;36m'
STUDENTCOL=BCOL
student='studentB'

programs=("${student}/p1_cinstream/" "${student}/p2_getline/")
executables=()

declare -A TESTS
TOTALTESTS=4
# TEST 0
TESTS[0,0]=0                                  # programs index
TESTS[0,1]=""                                 # arguments
TESTS[0,2]="A Bean Burrito is \$1.99
How many would you like to purchase? You entered: 5
Total cost: \$9.95"                           # expected output
TESTS[0,3]="5"                                # runtime values
TESTS[0,4]="automated"                        # automated or manual test?
# TEST 1                          
TESTS[1,0]=0                                  # programs index
TESTS[1,1]=""                                 # arguments
TESTS[1,2]="A Bean Burrito is \$1.99
How many would you like to purchase? You entered: 1
Total cost: \$1.99"                           # expected output
TESTS[1,3]="1"                                # runtime values
TESTS[1,4]="automated"                        # automated or manual test?
# TEST 2                    
TESTS[2,0]=1                                  # programs index
TESTS[2,1]=""                                 # arguments
TESTS[2,2]="Values entered are repeated"      # expected output
TESTS[2,3]=""                                 # runtime values
TESTS[2,4]="manual"                           # automated or manual test?
# TEST 3                    
TESTS[3,0]=1                                  # programs index
TESTS[3,1]=""                                 # arguments
TESTS[3,2]="Values entered are repeated"      # expected output
TESTS[3,3]=""                                 # runtime values
TESTS[3,4]="manual"                           # automated or manual test?

# ---------------------------------------------------------------------- WHICH STUDENT?
echo -e "\n TESTS FOR ${student}"; head -1 "${programs[0]}purchase.cpp"
# ---------------------------------------------------------------------- BUILD PROGRAMS
echo -e "\n === BUILD PROGRAMS ==="
for i in ${!programs[@]}; do
  niceIndex=$((i+1))
  outname="${programs[i]}${student}_program$niceIndex.out"
  g++ ${programs[i]}*.cpp -o ${outname}
  if [ "$?" -eq 0 ]; then
    echo -e "${GREEN}Successfully built [${outname}] ${NC}"
    executables+=(${outname})
  else
    echo -e "${RED}FAILED to build [${outname}]${NC}"
  fi  
done
# ---------------------------------------------------------------------- TEST PROGRAMS
echo -e "\n === TEST PROGRAMS ==="
for ((i=0; i<${TOTALTESTS}; i++))
do  
  PROGRAMI=${TESTS[$i,0]}
  PROGARGS=${TESTS[$i,1]}
  EXPECTED=${TESTS[$i,2]}
  RUNTIMEV=${TESTS[$i,3]}
  TESTTYPE=${TESTS[$i,4]}
  EXECUTBL=${executables[${PROGRAMI}]}

  RUNNER="./$EXECUTBL"
  if [ "$PROGARGS" != "" ]; then
    RUNNER="./$EXECUTBL $PROGARGS"
  elif [ "$RUNTIMEV" != "" ]; then
    RUNNER="echo $RUNTIMEV | ./$EXECUTBL"
  fi
  
    
  if [ "$TESTTYPE" == "manual" ]; then
    echo -e "TEST ${i}: (MANUAL-CHECK TEST)"
    eval "$RUNNER"
    echo -e "* EXPECTED OUTPUT: [${EXPECTED}]"
    
  else

    ACTUAL=$(eval "$RUNNER")
    if [ "${ACTUAL}" == "${EXPECTED}" ]; then
      echo -e "* ${GREEN}TEST ${i} SUCCESS! PROGRAM:[$RUNNER] OUTPUT: [${ACTUAL}]${NC}"
    else
      echo -e "* ${RED}TEST #${i} FAIL! PROGRAM:[$RUNNER] \n\t* EXPECTED OUTPUT: [${EXPECTED}] \n\t* ACTUAL OUTPUT:   [$ACTUAL]${NC}"
    fi
  fi
    
done
