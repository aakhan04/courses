#! /bin/bash
#clear
# $? = Specifies the exit status of the last command or the most recent execution process.
RED='\033[0;31m'; GREEN='\033[0;32m'; NC='\033[0m' # No Color
ACOL='\033[0;34m'; BCOL='\033[0;35m'; CCOL='\033[0;36m'
STUDENTCOL=ACOL
student='studentA'

programs=("${student}/p1_if/" "${student}/p2_ifelse/" "${student}/p3_ifelseif/" "${student}/p4_operators/")
executables=()

declare -A TESTS
TOTALTESTS=12
# TEST 0
TESTS[0,0]=0                                      # programs index
TESTS[0,1]="35 40"                                # arguments
TESTS[0,2]="Score: 87.50%"                        # expected output
TESTS[0,3]="4"                                    # runtime values
TESTS[0,4]="automated"                            # automated or manual test?
# TEST 1                              
TESTS[1,0]=0                                      # programs index
TESTS[1,1]="60 60"                                # arguments
TESTS[1,2]="Score: 100.00% (PERFECT)"             # expected output
TESTS[1,3]="2"                                    # runtime values
TESTS[1,4]="automated"                            # automated or manual test?
# TEST 2                        
TESTS[2,0]=1                                      # programs index
TESTS[2,1]="20"                                   # arguments
TESTS[2,2]="POSITIVE"                             # expected output
TESTS[2,3]=""                                     # runtime values
TESTS[2,4]="automated"                            # automated or manual test?
# TEST 3                        
TESTS[3,0]=1                                      # programs index
TESTS[3,1]="0"                                    # arguments
TESTS[3,2]="NEGATIVE OR ZERO"                     # expected output
TESTS[3,3]=""                                     # runtime values
TESTS[3,4]="automated"                            # automated or manual test?
# TEST 4                        
TESTS[4,0]=1                                      # programs index
TESTS[4,1]="-5"                                   # arguments
TESTS[4,2]="NEGATIVE OR ZERO"                     # expected output
TESTS[4,3]=""                                     # runtime values
TESTS[4,4]="automated"                            # automated or manual test?
# TEST 5                        
TESTS[5,0]=2                                      # programs index
TESTS[5,1]="90"                                   # arguments
TESTS[5,2]="Battery: [****]"                      # expected output
TESTS[5,3]=""                                     # runtime values
TESTS[5,4]="automated"                            # automated or manual test?
# TEST 6                      
TESTS[6,0]=2                                      # programs index
TESTS[6,1]="80"                                   # arguments
TESTS[6,2]="Battery: [***_]"                      # expected output
TESTS[6,3]=""                                     # runtime values
TESTS[6,4]="automated"                            # automated or manual test?
# TEST 7                        
TESTS[7,0]=2                                      # programs index
TESTS[7,1]="50"                                   # arguments
TESTS[7,2]="Battery: [**__]"                      # expected output
TESTS[7,3]=""                                     # runtime values
TESTS[7,4]="automated"                            # automated or manual test?
# TEST 8                        
TESTS[8,0]=2                                      # programs index
TESTS[8,1]="25"                                   # arguments
TESTS[8,2]="Battery: [*___]"                      # expected output
TESTS[8,3]=""                                     # runtime values
TESTS[8,4]="automated"                            # automated or manual test?
# TEST 9                        
TESTS[9,0]=2                                      # programs index
TESTS[9,1]="24"                                   # arguments
TESTS[9,2]="Battery: [____]"                      # expected output
TESTS[9,3]=""                                     # runtime values
TESTS[9,4]="automated"                            # automated or manual test?
# TEST 10                       
TESTS[10,0]=3                                      # programs index
TESTS[10,1]="6"                                    # arguments
TESTS[10,2]="6 is divisible by both 2 AND 3"       # expected output
TESTS[10,3]=""                                     # runtime values
TESTS[10,4]="automated"                            # automated or manual test?
# TEST 11                    
TESTS[11,0]=3                                      # programs index
TESTS[11,1]="4"                                    # arguments
TESTS[11,2]="4 is not divisible by both 2 AND 3"   # expected output
TESTS[11,3]=""                                     # runtime values
TESTS[11,4]="automated"                            # automated or manual test?

# ---------------------------------------------------------------------- WHICH STUDENT?
echo -e "\n TESTS FOR ${student}"; head -1 "${programs[0]}score.cpp"
# ---------------------------------------------------------------------- BUILD PROGRAMS
echo -e "\n === BUILD PROGRAMS ==="
for i in ${!programs[@]}; do
  niceIndex=$((i+1))
  outname="${programs[i]}${student}_program$niceIndex.out"
  g++ ${programs[i]}*.cpp -o ${outname}
  if [ "$?" -eq 0 ]; then
    echo -e "${GREEN}Successfully built [${outname}] ${NC}"
    executables+=(${outname})
  else
    echo -e "${RED}FAILED to build [${outname}]${NC}"
  fi  
done
# ---------------------------------------------------------------------- TEST PROGRAMS
echo -e "\n === TEST PROGRAMS ==="
for ((i=0; i<${TOTALTESTS}; i++))
do  
  PROGRAMI=${TESTS[$i,0]}
  PROGARGS=${TESTS[$i,1]}
  EXPECTED=${TESTS[$i,2]}
  RUNTIMEV=${TESTS[$i,3]}
  TESTTYPE=${TESTS[$i,4]}
  EXECUTBL=${executables[${PROGRAMI}]}

  RUNNER="./$EXECUTBL"
  if [ "$PROGARGS" != "" ]; then
    RUNNER="./$EXECUTBL $PROGARGS"
  elif [ "$RUNTIMEV" != "" ]; then
    RUNNER="echo $RUNTIMEV | ./$EXECUTBL"
  fi
  
    
  if [ "$TESTTYPE" == "manual" ]; then
    echo -e "TEST ${i}: (MANUAL-CHECK TEST)"
    eval "$RUNNER"
    echo -e "* EXPECTED OUTPUT: [${EXPECTED}]"
    
  else

    ACTUAL=$(eval "$RUNNER")
    if [ "${ACTUAL}" == "${EXPECTED}" ]; then
      echo -e "* ${GREEN}TEST ${i} SUCCESS! PROGRAM:[$RUNNER] OUTPUT: [${ACTUAL}]${NC}"
    else
      echo -e "* ${RED}TEST #${i} FAIL! PROGRAM:[$RUNNER] \n\t* EXPECTED OUTPUT: [${EXPECTED}] \n\t* ACTUAL OUTPUT:   [$ACTUAL]${NC}"
    fi
  fi
    
done
