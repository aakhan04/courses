--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- IF/ELSE STATEMENTS --
With an "if/else" statement, "if" statement codeblock is executed if the condition is true, and the "else" statement codeblock is executed if that condition is false.

if ( CONDITION )
{
  // CONDITION WAS TRUE
}
else
{
  // CONDITION WAS FALSE
}


-- BUILDING YOUR PROGRAM --
cd studentC/p2_ifelse
g++ poszero.cpp -o poszero.out


-- EXAMPLE PROGRAM OUTPUT --
./poszero.out 5
POSITIVE OR ZERO

./poszero.out 0
POSITIVE OR ZERO

./poszero.out -5
NEGATIVE

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

ARGUMENTS: This program expects additional arguments:
1. number             The number to assess

Create a float variable to store a number.

Convert args[1] to store in number.

If the number is greater than or equal to 0, then display "POSITIVE OR ZERO".

otherwise, display "NEGATIVE".
