/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ concat.cpp -o concat.exe
 * RUN PROGRAM: ./concat.exe string1 string2
 *
 * 
 * */
 
 #include <iostream>
 #include <string>
 using namespace std;
 
 int main( int argCount, char* args[] )
 {
   if ( argCount < 3 )
   {
     cout << "EXPECTED FORM: " << args[0] << " string1 string2" << endl;
     return 1;
   }
   
   string string1 = string( args[1] );
   string string2 = string( args[2] );
   string string3;
   
   // TODO: Concatenate strings

   cout << "string3 is " << string3 << endl;
   
   return 0;
 }
