--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- SWITCH STATEMENTS  --
Switch statements are a way you can branch your code based on the VALUE of a given VARIABLE. The Switch statement itself takes in a variable, then each CASE represents a value it could have.

switch( var )
{
  case 1:
    // CODE FOR var == 1
  break;
  
  case 2:
    // CODE FOR var == 2
  break;
  
  case 3:
    // CODE FOR var == 3
  break;
  
  default:
    // CODE FOR else
}

-- BUILDING YOUR PROGRAM --
cd studentB
g++ neighbor.cpp -o neighbor.out


-- EXAMPLE PROGRAM OUTPUT --
./neighbor.exe n
North of Kansas is Nebraska

./neighbor.exe s
South of Kansas is Oklahoma

./neighbor.exe e
East of Kansas is Missouri

./neighbor.exe w
West of Kansas is Colorado

./neighbor.exe x
? of Kansas is ?

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

ARGUMENTS: This program expects additional arguments:
1. direction          A direction compared to Kansas - n, w, s, e.

Get args[1][0] and store it as a char variable - the direction.

Create a string for state and assign it "Kansas".

Create a string for the direction text.

Create a string for the neighbor state.

Use a switch statement to assign a value to the neighbor variable, based on the direction:
  direction is 'n': then store "Nebraska" in the neighbor, store "North" in the direction text.
  direction is 's': then store "Oklahoma" in the neighbor, store "South" in the direction text.
  direction is 'e': then store "Missouri" in the neighbor, store "East" in the direction text.
  direction is 'w': then store "Colorado" in the neighbor, store "West" in the direction text.
  otherwise: store "?" in the neighbor variable, store "?" in the direction text.

Afterwards, display a message in the form "North of Kansas is Nebraska".
