# 1. Ask the user "How many points possible?". Get their input as a float, store in `points_possible`.


  
# 2. Ask the user "How many points earned?". Get their input as a float, store in `earned_points`.


  
# 3. Calculate their score percent with: earned_points / points_possible * 100. Store this in a new float variable, `score_percent`.


  
# 4. Display `score_percent` to the screen.


  
# 5. If the `score_percent` is greater than or equal to 100, then also display "PERFECT!".



  
# 6. Display "Goodbye" at the end of the program.
print( "\n\n Goodbye." )