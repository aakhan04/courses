#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  cout << fixed << setprecision( 2 ); // prepare for USD style output

  cout << "----------------------------" << endl;
  cout << "       MOVIE THEATER        " << endl;
  cout << "----------------------------" << endl;
  cout << endl;
  
  int age;
  cout << "Enter customer age: ";
  cin >> age;

  float price = 0;

  if ( age < 13 )
  {
    // Child
    price = 6.50;
  }
  else if ( age >= 13 && age <= 18 )
  {
    // Teenager
    price = 7.75;
  }
  else if ( age > 18 && age < 60 )
  {
    // Adult
    price = 9.95;
  }
  else
  {
    // Senior citizen
    price = 5.50;
  }

  cout << "Ticket price is: $" << price << endl;
  
  cout << endl << "Goodbye." << endl;
  
  return 0; // program end
}