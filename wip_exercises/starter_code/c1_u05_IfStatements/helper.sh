RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color
echo -e "$YELLOW ---------------------------- "
echo -e " To run your PYTHON programs:" 
echo -e " ---------------------------- "
echo -e "1. Go to the python folder:"
echo -e "cd python"
echo -e ""
echo -e "2. Run the file you want:"
echo -e "python3 FILE.py"
echo -e ""
echo -e "$BLUE ---------------------------- "
echo -e " To run your C++ programs:"
echo -e " ---------------------------- "
echo -e "1. Go to the cpp folder:"
echo -e "cd cpp"
echo -e ""
echo -e "2. Build your cpp file:"
echo -e "g++ FILE.cpp"
echo -e ""
echo -e "3. Run your built program:"
echo -e "./a.out"