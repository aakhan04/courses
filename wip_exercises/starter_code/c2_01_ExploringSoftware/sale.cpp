#include <iomanip>
#include <iostream>
using namespace std;
int main() 
{
  cout << setprecision( 2 ) << fixed;
  
  bool done = false;
  
  float totalPrice = 0;
  float tax = 0.091;
  float taxAmount = 0;
  float pricePlusTax = 0;
  
  while ( !done )
  {
    taxAmount = totalPrice * tax;
    pricePlusTax = totalPrice + taxAmount;
    
    cout << endl;
    cout << "TOTAL PRICE:     $" << totalPrice << endl;
    cout << "TAX:             $" << taxAmount << endl;
    cout << "PRICE AFTER TAX: $" << pricePlusTax << endl;
    cout << endl;
    cout << "1. Clear shopping cart" << endl;
    cout << "2. Add another item" << endl;
    cout << "3. Exit" << endl;
    cout << endl;
    
    cout << ">> ";
    int choice;
    cin >> choice;
    
    cout << endl;
    if ( choice == 1 )
    {
      totalPrice = 0;
      cout << "Shopping cart cleared." << endl;
    }
    else if ( choice == 2 )
    {
      float price;
      cout << "Enter price of next object: $";
      cin >> price;
      totalPrice += price;
    }
    else if ( choice == 3 )
    {
      done = true;
      cout << "Goodbye." << endl;
    }
  }

  return 0;
}
