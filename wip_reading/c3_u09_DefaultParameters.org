# -*- mode: org -*-

#+TITLE: Default parameters
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />

** Default parameters

When declaring a function, you can also set *default parameters*.
These are the default values assigned to the parameters if the user
doesn't pass anything in. The default parameters are
*only specified in a function declaration* - NOT the definition!
        
In this example, it could be a function that displays ingredients
for a recipe, and by default the batch is set to 1.0 (one batch).

#+BEGIN_SRC cpp :class cpp
  void OutputIngredients( float eggs, float sugar, float flour, float batch = 1.0 );
#+END_SRC

The function could be called without passing in a batch:

#+BEGIN_SRC cpp :class cpp
  cout << "Ingredients:" << endl;
  OutputIngredients( 1, 2.0, 3.5 );
#+END_SRC


Or they could pass a batch amount explicitly:

#+BEGIN_SRC cpp :class cpp
  cout << "Ingredients:" << endl;
  OutputIngredients( 1, 2.0, 3.5, 0.5 );  // half batch
#+END_SRC
   
You can have multiple *default parameters*
specified in your function declaration - but all variables
with default values must go /after/ any variables
/without default values/.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
- Example usage - Logger ::

In the *Overloaded Functions* part we showed having a =Logger= class with multiple constructors.
With this particular example, it would actually be better to use a *default parameter* instead of overloading!

Here's an example using the default paramter:

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Logger.h

#+BEGIN_SRC cpp :class cpp
  class Logger
  {
    public:
    Logger( std::string filename = "log.txt"  );
    ~Logger();                      // destructor
    // ... etc ...

    private:
    ofstream m_outfile;
    // ... etc ...
  };
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

Logger.cpp

#+BEGIN_SRC cpp :class cpp
  Logger::Logger( std::string filename /*="log.txt"*/ )
  {
    m_outfile.open( filename );
  }

  Loger::~Logger()
  {
    m_outfile.close();
  }
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

Elseware in the program, the Logger will be created, and that log file will be opened automatically.
We can specify a file or not, and both will use the same constructor:
 
#+BEGIN_SRC cpp :class cpp
  Logger logger();
  Logger logger( "log-sept-28.txt" );
#+END_SRC



#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*** Additional notes
- According to the C++ Core Guidelines: "F.51: Where there is a choice, prefer default arguments over overloading"
  https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#reason-67

-----
