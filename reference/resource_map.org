# -*- mode: org -*-

*CS 200 Topics*

- Introduction to C++
  - *60 Second Review:*  https://www.youtube.com/shorts/ScaOW_f2Ig0
  - *Old lecture video:*  http://lectures.moosader.com/cs200/00-Introduction.mp4
  - *Class archive:*  http://lectures.moosader.com/cs200/2021-01_Spring/2021-01-26_cs200_lecture.mp4
- Variables and data types
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/02-CPP-Basics.mp4
  # - *Class archive:*
- Input and output with cin/cout
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/02-CPP-Basics.mp4
  # - *Class archive:*
- Control flow - Branching and looping
  # - *60 Second Review:*
  - *Old lecture video:*
    - http://lectures.moosader.com/cs200/03-Branching.mp4
    - http://lectures.moosader.com/cs200/04-Loops.mp4
  - *Class archive:*
    - http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-02_cs200_lectureA_truth_tables.mp4
    - http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-02_cs200_lectureB_if_statements.mp4
    - http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-04_cs200_lecture.mp4
    - http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-09_cs200_lecture.mp4
    - http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-11_cs200_lecture.mp4
    - http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-17_cs200_lecture_branching.mp4
    - http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-22_cs200_lecture_whileloops.mp4
    - http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-24_cs200_lecture_forloops.mp4
- Arrays
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/08-Arrays.mp4
  - *Class archive:*
    - http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-29_cs200_lecture_arrays.mp4
    - http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-23_cs200_lecture.mp4
    - http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-25_cs200_lectureA_array_review.mp4
- Strings
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/07-Strings.mp4
  - *Class archive:* http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-25_cs200_lectureB_strings.mp4
- File input and output
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/14-File-IO.mp4
  - *Class archive:* http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-02_cs200_lecture_fileio.mp4
- Functions
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/05-Functions.mp4
  - *Class archive:*
    - http://lectures.moosader.com/cs200/2021-06_Summer/2021-07-01_cs200_lecture_functions.mp4
    - http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-16_cs200_lecture_functions.mp4
    - http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-18_cs200_lecture_functions_arrays_and_const.mp4
- Const
  # - *60 Second Review:*
  - *Old lecture video:*
    - http://lectures.moosader.com/cs200/09-Const.mp4
    - http://lectures.moosader.com/cs200/21-Const-2.mp4
  # - *Class archive:*
- Structs
  # - *60 Second Review:*
  - *Old lecture video:*
    - https://youtu.be/kcfwQ2duNIE
    - http://lectures.moosader.com/cs200/10-Struct.mp4
  # - *Class archive:*
- Classes
  # - *60 Second Review:*
  - *Old lecture video:*
    - http://lectures.moosader.com/cs200/11-Class-1.mp4
    - http://lectures.moosader.com/cs200/12-Class-2.mp4
    - http://lectures.moosader.com/cs200/13-Class-Design.mp4
  - *Class archive:* http://lectures.moosader.com/cs200/2021-06_Summer/2021-07-06_cs200_lecture_classes.mp4
- Inheritance
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/15-Inheritance.mp4
  - *Class archive:* http://lectures.moosader.com/cs200/2021-06_Summer/2021-07-08_cs200_lecture_inheritance.mp4
- Pointers and memory
  - *60 Second Review:* https://www.tiktok.com/@rwsskc/video/7146577130120318250
  - *Old lecture video:*
    - http://lectures.moosader.com/cs200/16-Pointer.mp4
    - http://lectures.moosader.com/cs200/17-Memory-Management.mp4
    - http://lectures.moosader.com/cs200/18-Dynamic-Arrays.mp4
  # - *Class archive:*
- Recursion
  # - *60 Second Review:*
  # - *Old lecture video:*
  - *Class archive:* http://lectures.moosader.com/cs250/2021-01_Spring/2021-03-24_cs250_lecture_recursion.mp4
- Searching and sorting
  - *60 Second Review:*
  - *Old lecture video:*
  - *Class archive:*



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*CS 235 Topics*

- Testing and Debugging
  # - *60 Second Review:*
  # - *Old lecture video:*
  - *Class archive:* http://lectures.moosader.com/cs250/2021-01_Spring/2021-01-26_cs250_lecture.mp4
- Function overloading
  - *60 Second Review:*
  - *Old lecture video:*
  - *Class archive:*
- Operator overloading
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/20-Operator-Overloading.mp4
  # - *Class archive:*
- Smart Pointers
  - *60 Second Review:*
  - *Old lecture video:*
  - *Class archive:*
- Polymorphism
  - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/22-Polymorphism.mp4
  - *Class archive:*
- Exceptions
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/06-Exceptions.mp4
  - *Class archive:*
    - https://www.youtube.com/watch?v=N269qOGckQE
    - http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-02_cs250_templates_and_exceptions.mp4
    - https://www.youtube.com/watch?v=hQVOOxCKl2s
- Templates
  # - *60 Second Review:*
  - *Old lecture video:* https://www.youtube.com/watch?v=1sLim0ln5UQ
  - *Class archive:*
    - https://www.youtube.com/watch?v=bUAqsECFc54
    - http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-02_cs250_templates_and_exceptions.mp4
    - https://www.youtube.com/watch?v=hQVOOxCKl2s
- Anonymous Functions
  # - *60 Second Review:*
  # - *Old lecture video:*
  # - *Class archive:*
- Static
  # - *60 Second Review:*
  # - *Old lecture video:*
  # - *Class archive:*
- Friends
  # - *60 Second Review:*
  # - *Old lecture video:*
  # - *Class archive:*
- The Standard Template Library
  # - *60 Second Review:*
  - *Old lecture video:* http://lectures.moosader.com/cs200/27-STL.mp4
  - *Class archive:*
    - https://youtu.be/iKyLwc8G4l0
    - https://www.youtube.com/watch?v=hQVOOxCKl2s


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*CS 250 Topics*

- Intro to Data Structures
  # - *60 Second Review:*
  - *Old lecture video:* https://www.youtube.com/watch?v=INGLTz6jBuk
  # - *Class archive:*
- Algorithm Efficiency
  # - *60 Second Review:*
  # - *Old lecture video:*
  - *Class archive:* http://lectures.moosader.com/cs250/2021-01_Spring/2021-03-16_cs250_lecture_algorithm_efficiency.mp4
- Smart Arrays
  # - *60 Second Review:*
  # - *Old lecture video:*
  - *Class archive:*
    - https://www.youtube.com/watch?v=74CZ3LmMgyU
    - http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-16_cs250_lecture.mp4
    - http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-16_cs250_lecture.mp4
- Linked Lists
  # - *60 Second Review:*
  - *Old lecture video:*
    - https://www.youtube.com/watch?v=Qm6ooxonq3w
    - https://www.youtube.com/watch?v=kKM9dpST-Z0
  - *Class archive:*
    - http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-23_cs250_lectureB_linked_lists.mp4
- Stacks and Queues
  # - *60 Second Review:*
  - *Old lecture video:* https://www.youtube.com/watch?v=hNfgrGFIAVs
  # - *Class archive:*
- Intro to Trees
  # - *60 Second Review:*
  # - *Old lecture video:*
  - *Class archive:* https://www.youtube.com/watch?v=7ZcZuYvBAVw
- Binary Search Trees
  # - *60 Second Review:*
  # - *Old lecture video:*
  # - *Class archive:*
- Hash Tables
  # - *60 Second Review:*
  # - *Old lecture video:*
  # - *Class archive:*
