# -*- mode: org -*-

#+ATTR_HTML: :class subheader
*Installing and configuring VSCode*

On the VSCode website you can download the program for free.

Website: https://code.visualstudio.com/Download

Select your operating system, the big blue button on top
is fine, or you can select one of the smaller buttons for
more refined options.

Once downloaded, double click on the installer to begin.

1. License Agreement: Click *Next >*
2. Select Destination Location: You can leave it as default and click *Next >*
3. Select Start Menu Folder: You can leave it as default and click *Next >*
4. Select Additional Tasks: Click the checkboxes that say
   1. Add "Open with Code" action to Windows Explorer file context menu
   2. Add "Open with Code" action to Windows Explorer directory context menu
      [[file:ide_vscode/vscode_install4.png]]
   3. (Note: if you're on Mac/Linux this may not be an option.)
5. Ready to Install: Click *Install*.





#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

#+ATTR_HTML: :class subheader
*Cloning your course repository*

Once you have a GitLab account, the instructor
will set up a *course repository* for you.
This is a folder on a server where you can keep
your work backed up. You can sync between
the files on your desktop and any changes on the
server (the instructor may add new files or make
updates to your code to add comments).

Go to your *GitLab repository webpage* and locate
the blue *code* button. Click on it, then copy the
URL underneath *Clone with HTTPS*.

#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_clonerepo_copyurl.png]]

Within *VSCode*, click on the icon on the left that looks
like a node branching into two nodes. This is the *source control*
tab. Click on the *Clone Repository* button.

[[file:ide_vscode/vscode_clonerepo_clonerepository.png]]

A little box will pop up at the top of VSCode asking
for a *repository URL*. Paste the copied URL from the
website here, then hit *enter*.

[[file:ide_vscode/vscode_clonerepo_repourl.png]]

It will ask you to set a destination for your course folder.
I would suggest putting in in your *Documents* or *Desktop*.
Once you've found a location, click the
*Select as Repository Destination* button.

#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_clonerepo_setdestination.png]]

Once it's been cloned, you will be able to see the folder
on your computer under the *file explorer*. You will
work from this directory.

#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_clonerepo_onexplorer.png]]

As you work through assignments you will periodically
back up your work and sync it with the server, but
we will cover that later on.




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

#+ATTR_HTML: :class subheader
*Opening your repository folder*

Once you're ready to start programming, from the *File*
menu, select *Open Folder...*.

[[file:ide_vscode/vscode_open1.png]]

Locate your *repository folder*
(named something like cs200-username-repo).
Highlight the folder and click *Open Folder*.

[[file:ide_vscode/vscode_open_repofolder.png]]

You will be able to see all the contents of your
repository on the left side.


#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_repo_opened.png]]

You can double-click one of the folders (which have a =>= to its left)
to open it and you can see the code within the folder.
If you double-click a file it will open it in the editor view.



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

#+ATTR_HTML: :class subheader
*VSCode terminal*

To open the terminal, click on the *View* dropdown
from the top menu and select *Terminal*.


#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_build_viewterminal.png]]

The *Terminal* is a text interface, we will use some basic
commands to work with our code. The terminal has a
*prompt*, which displays the current folders that you're
inside of (called the *path*), and ends with a =>= sign.
You can type commands after the =>=.

*ls - LiSt files and folders*

If you type =ls= (that is a lower-case L and S) and hit ENTER,
it will show you the files and folders in this location.


#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_open_repo_terminal.png]]

*cd - Change Directory*

If you type =cd= and the name of a folder, it will move
you into that folder. This can be useful to go between
your different projects.


#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_open_repo_terminal_cd.png]]

=cd FOLDERNAME= will put you inside the folder named, and
=cd ..= will take you out of the current folder you're in.




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

#+ATTR_HTML: :class subheader
*Building your C++ program*

Every time you make a change to your C++ source code
you will have to *compile* (aka *build*) the code
into an executable (*.exe*) file. To do this, we use
the *g++* compiler program.

To run the program, we list
the program name first (=g++=) then give it any options
after a space. For g++, the option will be the code file
we want to build:

#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_build_terminal_gpp.png]]

=g++ test.cpp= will build "test.cpp" into an executable file.
On *Windows*, it will generate *a.exe*. On Linux and Mac,
it will generate *a.out*. You can use the =ls= command
to make sure your program is shown.

If the build is successful, g++ won't give you any messages
and just return you to the prompt to enter a new command.
However, if there are syntax errors in your code, it will
display the errors afterwards. You will need to fix any
syntax errors in order to get the program to build.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

#+ATTR_HTML: :class subheader
*Running your program*

To run the program, use the =./PROGRAMNAME= command,
this runs your program. By default, g++ generates a
=a.exe= or =a.out= file, so to run you would use
=./a.exe= or =./a.out=:


#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_build_terminal_run.png]]




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML


#+ATTR_HTML: :class subheader
*Committing and syncing to back up your work*

Once you've made any changes to your repository,
under the *Source Control* tab (the branching icon),
it will show a little notification bubble with a number
in it.

#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_changefile.png]]

From this page, you can back up your changes. In the little
text box, add a useful message to keep note of what you
changed (e.g., "Fixed bug with input", "Finished with u02 lab"),
then click the *Commit* button to make a backup snapshot.

[[file:ide_vscode/vscode_changefile_commitmessage.png]]

After a commit, the button turns into a *Sync Changes* button.
This will send your changes to the server for long-term storage.
You will also need to save your files on the server so that
the instructor can see your work. You can click it any time
you're doing a commit to back up your work.

[[file:ide_vscode/vscode_changefile_sync.png]]

Once you've synced your work, on the *GitLab repo website*
you will be able to see your latest commit message:

#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_changefile_website1.png]]

If you click on the folder and file that you edited
you will be able to see the updates online as well:

#+ATTR_HTML: :width 100%
#+ATTR_LATEX: :width \textwidth
[[file:ide_vscode/vscode_changefile_website3.png]]



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

VSCode might ask if you want to "periodically run git fetch".
Go ahead and click *Yes* here. This will automatically pull
down any changes from the server, such as if the instructor
adds additional files.

[[file:ide_vscode/vscode_gitfetch.png]]








 
# #+ATTR_HTML: :class extra-space-invisible
# #+BEGIN_HTML
# --------------------------------------------------------------------------------
# #+END_HTML
# 
# *Handy IDE tips*




