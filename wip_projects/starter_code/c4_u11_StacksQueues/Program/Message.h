#ifndef _MESSAGE
#define _MESSAGE

#include <string>

class Message
{
public:
  void Setup( std::string newAction, std::string newValue );
  std::string GetAction();
  std::string GetValue();

private:
  std::string m_action;
  std::string m_value;
};

#endif
