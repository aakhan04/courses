#include "Sandwich.h"

void Sandwich::Clear()
{
  while ( m_toppings.size() > 0 )
  {
    m_toppings.pop();
  }
}

void Sandwich::Add( std::string topping )
{
  m_toppings.push( topping );
}

void Sandwich::Remove()
{
  m_toppings.pop();
}

// Overloading the stream operator
std::ostream& operator<<( std::ostream& out, const Sandwich& me )
{
  Sandwich dupe( me );

  while ( dupe.m_toppings.size() > 0 )
  {
    out << dupe.m_toppings.top() << std::endl;
    dupe.m_toppings.pop();
  }

  return out;
}
