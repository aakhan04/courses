#include "Program.h"

#include "../Utilities/Menu.h"

#include <iostream>
#include <iomanip>
#include <fstream>

void Program::SetDataPath(std::string path)
{
	m_dataPath = path;
	std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void Program::Run()
{
  m_log.open( "log.txt" );
  ReadMessages();
  ProcessMessages();
  SaveResults();
  m_log.close();
}

void Program::ReadMessages()
{
  m_log << endl << string( 80, '-' ) << endl << "void Program::ReadMessages()" << endl;
  cout << endl << string( 80, '-' ) << endl << "void Program::ReadMessages()" << endl;

  /*
  Normally with a message queue, we would be receiving messages via the network/internet
  and storing them in the queue to be processed in the order received.
  The text file here has all the commands, which is supposed to "simulate" the messages
  received.
  */

  ifstream input( m_dataPath + "message-input.txt" );
  std::string action, value;

  while ( input >> action )
  {
    // Really basic file parsing
    if      ( action == "ADD" ) { input >> value; }
    else if ( action == "REMOVE" ) { value = ""; }
    else if ( action == "FINISH" ) { value = ""; }

    Message newMessage;
    newMessage.Setup( action, value );
    m_messageQueue.push( newMessage );    // TODO: UPDATE ME!

    m_log << "Added [" << action << ", " << value << "] to message queue." << endl;
    cout << "Added [" << action << ", " << value << "] to message queue." << endl;
  }

  m_log << "Message queue size is now: " << m_messageQueue.size() << endl;    // TODO: UPDATE ME!
}

void Program::ProcessMessages()
{
  m_log << endl << string( 80, '-' ) << endl << "void Program::ProcessMessages()" << endl;
  cout << endl << string( 80, '-' ) << endl << "void Program::ProcessMessages()" << endl;

  /*
  We're assembling sandwiches here for the sake of conceptual simplicity, this isn't
  how sandwiches would be built on, say, a restaurant app. Stacks are usually used for
  things more like processing program code blocks, handling program views and the
  ability to "go back", and other things.
  */

  while ( m_messageQueue.size() > 0 )
  {
    m_log << "Process message [" << m_messageQueue.front().GetAction() << ", " << m_messageQueue.front().GetValue() << "]." << endl;    // TODO: UPDATE ME!
    cout << "Process message [" << m_messageQueue.front().GetAction() << ", " << m_messageQueue.front().GetValue() << "]." << endl;    // TODO: UPDATE ME!

    if ( m_messageQueue.front().GetAction() == "ADD" )    // TODO: UPDATE ME!
    {
      m_log << "Add \"" << m_messageQueue.front().GetValue() << "\" to the sandwich." << endl;    // TODO: UPDATE ME!
      cout << "Add \"" << m_messageQueue.front().GetValue() << "\" to the sandwich." << endl;    // TODO: UPDATE ME!

      m_workingStack.push( m_messageQueue.front().GetValue() );    // TODO: UPDATE ME!
    }
    else if ( m_messageQueue.front().GetAction() == "REMOVE" )    // TODO: UPDATE ME!
    {
      m_log << "Remove last added item from the sandwich." << endl;
      cout << "Remove last added item from the sandwich." << endl;

      m_workingStack.pop(); // TODO: UPDATE ME!
    }
    else if ( m_messageQueue.front().GetAction() == "FINISH" )    // TODO: UPDATE ME!
    {
      m_log << "Sandwich is finished, add to results list." << endl;
      cout << "Sandwich is finished, add to results list." << endl;

      Sandwich wipSandwich;

      while ( m_workingStack.size() > 0 )
      {
        wipSandwich.Add( m_workingStack.top() );
        m_workingStack.pop();
      }

      m_results.push_back( wipSandwich );    // TODO: UPDATE ME!
    }
  }
}

void Program::SaveResults()
{
  m_log << endl << string( 80, '-' ) << endl << "void Program::SaveResults()" << endl;
  cout << endl << string( 80, '-' ) << endl << "void Program::SaveResults()" << endl;

  string path = m_dataPath + "results-output.txt";
  ofstream output( path );

  for ( size_t i = 0; i < m_results.size(); i++ )    // TODO: UPDATE ME!
  {
    m_log << "Writing out sandwich:" << endl << m_results.at( i ) << endl;    // TODO: UPDATE ME!
    cout << "Writing out sandwich:" << endl << m_results.at( i ) << endl;    // TODO: UPDATE ME!

    output << m_results.at( i ) << endl;    // TODO: UPDATE ME!
  }

  output.close();
  m_log << endl << "Results written to: " << path << endl;
  cout << endl << "Results written to: " << path << endl;
}
