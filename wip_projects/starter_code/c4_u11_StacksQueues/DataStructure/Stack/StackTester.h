#ifndef _STACK_TESTER_HPP
#define _STACK_TESTER_HPP

// C++ Library includes
#include <iostream>
#include <string>

#include "ArrayStack.h"
#include "LinkedStack.h"
#include "../../CuTest/TesterBase.h"
#include "../../Utilities/Menu.h"
#include "../../Utilities/StringUtil.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

namespace DataStructure
{

//! TESTER for the Stack
class StackTester : public cuTest::TesterBase
{
public:
    StackTester()
        : TesterBase( "test_result_stack.html" )
    {
        AddTest(cuTest::TestListItem("Test_Push",                 std::bind(&StackTester::Test_Push, this)));
        AddTest(cuTest::TestListItem("Test_Pop",                  std::bind(&StackTester::Test_Pop, this)));
        AddTest(cuTest::TestListItem("Test_Top",                  std::bind(&StackTester::Test_Top, this)));
        AddTest(cuTest::TestListItem("Test_Size",                 std::bind(&StackTester::Test_Size, this)));
        AddTest(cuTest::TestListItem("Test_IsEmpty",              std::bind(&StackTester::Test_IsEmpty, this)));
    }

    virtual ~StackTester() { }

private:
    int Test_Push();
    int Test_Pop();
    int Test_Top();
    int Test_Size();
    int Test_IsEmpty();
};

int StackTester::Test_Push()
{
    std::string functionName = "Stack Push";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Stack Test_" + functionName, { functionName } );

    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayStack<int> arr; arr.Push( 123 ); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedStack<int> arr; arr.Push( 123 ); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Push one item (Array and Linked)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 12 to each stack" );

        int input = 12;
        int expectedOutput = 12;

        ArrayStack<int> arrayStack;
        LinkedStack<int> linkedStack;

        arrayStack.Push( input );
        linkedStack.Push( input );

        /*
        MAKE SURE THIS IS ADDED TO SMART DYNAMIC ARRAY AND LINKED LIST:
        friend class QueueTester;
        friend class StackTester;
        */

        int actualOutputArray = arrayStack.m_vector.m_array[0];
        int actualOutputLinked = linkedStack.m_list.m_ptrFirst->m_data;

        if      ( !Set_Outputs( "Array Stack item 0", expectedOutput, actualOutputArray ) )                 { TestFail(); }
        else if ( !Set_Outputs( "Linked Stack item 0", expectedOutput, actualOutputLinked ) )               { TestFail(); }
        else if ( !Set_Outputs( "arrayStack.m_vector.m_itemCount", 1, arrayStack.m_vector.m_itemCount ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedStack.m_list.m_itemCount", 1, linkedStack.m_list.m_itemCount ) )     { TestFail(); }
        else                                                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Push two items (Array and Linked)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 12, 20 to each stack" );

        int input1 = 12;
        int input2 = 20;

        ArrayStack<int> arrayStack;
        LinkedStack<int> linkedStack;

        arrayStack.Push( input1 );
        arrayStack.Push( input2 );
        linkedStack.Push( input1 );
        linkedStack.Push( input2 );

        int arrayFirstItem = arrayStack.m_vector.m_array[0];
        int arraySecondItem = arrayStack.m_vector.m_array[1];
        int linkedFirstItem = linkedStack.m_list.m_ptrFirst->m_data;
        int linkedSecondItem = linkedStack.m_list.m_ptrFirst->m_ptrNext->m_data;

        if      ( !Set_Outputs( "Array Stack item 0",  input1, arrayFirstItem ) )                           { TestFail(); }
        else if ( !Set_Outputs( "Array Stack item 1",  input2, arraySecondItem ) )                          { TestFail(); }
        else if ( !Set_Outputs( "Linked Stack item 0", input1, linkedFirstItem ) )                          { TestFail(); }
        else if ( !Set_Outputs( "Linked Stack item 1", input2, linkedSecondItem ) )                         { TestFail(); }
        else if ( !Set_Outputs( "arrayStack.m_vector.m_itemCount", 2, arrayStack.m_vector.m_itemCount ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedStack.m_list.m_itemCount", 2, linkedStack.m_list.m_itemCount ) )     { TestFail(); }
        else                                                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int StackTester::Test_Pop()
{
    std::string functionName = "Stack Pop";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Stack Test_" + functionName, { functionName } );

    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayStack<int> arr; arr.Pop(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedStack<int> arr; arr.Pop(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Add item to stack and call Pop" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 50 to the stack" );

        int input = 50;
        bool exceptionThrown = false;

        ArrayStack<int> arrayVersion;
        LinkedStack<int> linkedVersion;

        arrayVersion.m_vector.m_array = new int[3];
        arrayVersion.m_vector.m_arraySize = 3;
        arrayVersion.m_vector.m_array[0] = input;
        arrayVersion.m_vector.m_itemCount = 1;

        DoublyLinkedListNode<int>* node = new DoublyLinkedListNode<int>;
        node->m_data = input;
        linkedVersion.m_list.m_ptrFirst = node;
        linkedVersion.m_list.m_ptrLast = node;
        linkedVersion.m_list.m_itemCount = 1;

        try
        {
          arrayVersion.Pop();
          linkedVersion.Pop();
        }
        catch( ... )
        {
          exceptionThrown = true;
        }

        if      ( !Set_Outputs( "Exception thrown?",  false, exceptionThrown ) )                            { TestFail(); }
        else if ( !Set_Outputs( "arrayQueue.m_vector.m_itemCount", 0, arrayVersion.m_vector.m_itemCount ) ) { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.m_list.m_itemCount", 0, linkedVersion.m_list.m_itemCount ) )   { TestFail(); }
        else                                                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Add two items to stack and call Pop, check Top item" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 50, 100 to the stack" );

        int input1 = 50;
        int input2 = 100;
        bool exceptionThrown = false;

        ArrayStack<int> arrayVersion;
        LinkedStack<int> linkedVersion;

        arrayVersion.m_vector.m_array = new int[3];
        arrayVersion.m_vector.m_arraySize = 3;
        arrayVersion.m_vector.m_array[0] = input1;
        arrayVersion.m_vector.m_array[1] = input2;
        arrayVersion.m_vector.m_itemCount = 2;

        DoublyLinkedListNode<int>* nodeA = new DoublyLinkedListNode<int>;
        DoublyLinkedListNode<int>* nodeB = new DoublyLinkedListNode<int>;
        nodeA->m_data = input1;
        nodeA->m_ptrNext = nodeB;
        nodeB->m_data = input2;
        nodeB->m_ptrPrev = nodeA;
        linkedVersion.m_list.m_ptrFirst = nodeA;
        linkedVersion.m_list.m_ptrLast = nodeB;
        linkedVersion.m_list.m_itemCount = 2;

        int expectedFrontItem = input1;

        try
        {
          arrayVersion.Pop();
          linkedVersion.Pop();
        }
        catch( ... )
        {
          exceptionThrown = true;
        }

        int arrayFirstItem = arrayVersion.m_vector.m_array[0];
        int linkedFirstItem = linkedVersion.m_list.m_ptrFirst->m_data;

        if      ( !Set_Outputs( "Exception thrown?",  false, exceptionThrown ) )                            { TestFail(); }
        else if ( !Set_Outputs( "Array Queue front item",  expectedFrontItem, arrayFirstItem ) )            { TestFail(); }
        else if ( !Set_Outputs( "Linked Queue front item", expectedFrontItem, linkedFirstItem ) )           { TestFail(); }
        else if ( !Set_Outputs( "arrayQueue.m_vector.m_itemCount", 1, arrayVersion.m_vector.m_itemCount ) ) { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.m_list.m_itemCount", 1, linkedVersion.m_list.m_itemCount ) )   { TestFail(); }
        else                                                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int StackTester::Test_Top()
{
    std::string functionName = "Stack Top";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Stack Test_" + functionName, { functionName } );

    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayStack<int> arr; int a = arr.Top(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedStack<int> arr; int a = arr.Top(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Add two items to stack and call Top, verify correct Front item" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 50, 100 to the queue" );

        int input1 = 50;
        int input2 = 100;
        bool exceptionThrown = false;

        ArrayStack<int> arrayVersion;
        LinkedStack<int> linkedVersion;

        arrayVersion.m_vector.m_array = new int[3];
        arrayVersion.m_vector.m_arraySize = 3;
        arrayVersion.m_vector.m_array[0] = input1;
        arrayVersion.m_vector.m_array[1] = input2;
        arrayVersion.m_vector.m_itemCount = 2;

        DoublyLinkedListNode<int>* nodeA = new DoublyLinkedListNode<int>;
        DoublyLinkedListNode<int>* nodeB = new DoublyLinkedListNode<int>;
        nodeA->m_data = input1;
        nodeA->m_ptrNext = nodeB;
        nodeB->m_data = input2;
        nodeB->m_ptrPrev = nodeA;
        linkedVersion.m_list.m_ptrFirst = nodeA;
        linkedVersion.m_list.m_ptrLast = nodeB;
        linkedVersion.m_list.m_itemCount = 2;

        int expectedTopItem = input2;
        int arrayTopItem = arrayVersion.Top();
        int linkedTopItem = linkedVersion.Top();

        if      ( !Set_Outputs( "Array Queue top item",  expectedTopItem, arrayTopItem ) )            { TestFail(); }
        else if ( !Set_Outputs( "Linked Queue top item", expectedTopItem, linkedTopItem ) )           { TestFail(); }
        else                                                                                          { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int StackTester::Test_Size()
{
    std::string functionName = "Stack Size";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Stack Test_" + functionName, { functionName } );

    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayStack<int> arr; int s = arr.Size(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedStack<int> arr; int s = arr.Size(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Check size on empty stack" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        int expectedOutput = 0;

        ArrayStack<int> arrayStack;
        LinkedStack<int> linkedStack;

        if      ( !Set_Outputs( "arrayStack.Size()",  expectedOutput, arrayStack.Size() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedStack.Size()", expectedOutput, linkedStack.Size() ) )  { TestFail(); }
        else                                                                                  { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Check size on stack with 1 item in it" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        int expectedOutput = 1;

        ArrayStack<int> arrayVersion;
        LinkedStack<int> linkedVersion;

        arrayVersion.m_vector.m_itemCount = 1;
        linkedVersion.m_list.m_itemCount = 1;

        if      ( !Set_Outputs( "arrayQueue.Size()",  expectedOutput, arrayVersion.Size() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.Size()", expectedOutput, linkedVersion.Size() ) )  { TestFail(); }
        else                                                                                  { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. Check size on stack with 3 items in it" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        int expectedOutput = 3;

        ArrayStack<int> arrayVersion;
        LinkedStack<int> linkedVersion;

        arrayVersion.m_vector.m_itemCount = 3;
        linkedVersion.m_list.m_itemCount = 3;

        if      ( !Set_Outputs( "arrayQueue.Size()",  expectedOutput, arrayVersion.Size() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.Size()", expectedOutput, linkedVersion.Size() ) )  { TestFail(); }
        else                                                                                  { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int StackTester::Test_IsEmpty()
{
    std::string functionName = "Stack IsEmpty";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Stack Test_" + functionName, { functionName } );


    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayStack<int> arr; bool a = arr.IsEmpty(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedStack<int> arr; bool a = arr.IsEmpty(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Check size on IsEmpty with empty stack" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool expectedOutput = true;

        ArrayStack<int> arrayVersion;
        LinkedStack<int> linkedVersion;

        arrayVersion.m_vector.m_itemCount = 0;
        linkedVersion.m_list.m_itemCount = 0;

        if      ( !Set_Outputs( "arrayQueue.IsEmpty()",  expectedOutput, arrayVersion.IsEmpty() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.IsEmpty()", expectedOutput, linkedVersion.IsEmpty() ) )  { TestFail(); }
        else                                                                                          { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Check size on IsEmpty with non-empty stack" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool expectedOutput = false;

        ArrayStack<int> arrayVersion;
        LinkedStack<int> linkedVersion;

        arrayVersion.m_vector.m_itemCount = 1;
        linkedVersion.m_list.m_itemCount = 1;

        if      ( !Set_Outputs( "arrayQueue.IsEmpty()",  expectedOutput, arrayVersion.IsEmpty() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.IsEmpty()", expectedOutput, linkedVersion.IsEmpty() ) )  { TestFail(); }
        else                                                                                          { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

} // end of namespace
#endif
