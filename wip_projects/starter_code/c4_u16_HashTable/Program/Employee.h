#ifndef _EMPLOYEE
#define _EMPLOYEE

#include <string>
#include <iostream>

struct Employee
{
	std::string m_name;
	std::string m_email;
	size_t m_passwordHash;
	size_t m_id;

	void Display()
	{
		std::cout << "ID:    " << m_id << std::endl;
		std::cout << "NAME:  " << m_name << std::endl;
		std::cout << "EMAIL: " << m_email << std::endl;
	}
};

#endif