#ifndef _INVALID_INDEX_EXCEPTION
#define _INVALID_INDEX_EXCEPTION

// C++ Library includes
#include <stdexcept>
#include <string>

namespace Exception
{

//! EXCEPTION for an invalid index detected
class InvalidIndexException : public std::runtime_error
{
    public:
    InvalidIndexException( std::string functionName, std::string message )
        : std::runtime_error( "[" + functionName + "] " + message  ) { ; }
};

} // End of namespace

#endif
