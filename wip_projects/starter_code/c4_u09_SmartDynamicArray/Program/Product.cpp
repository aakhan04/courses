#include "Product.h"

#include "../Utilities/StringUtil.h"

#include <iostream>
#include <iomanip>

Product::Product()
{
	Setup("UNSET", 0, 0);
}

Product::Product(std::string name, float price, int quantity)
{
	Setup(name, price, quantity);
}

void Product::Setup(std::string name, float price, int quantity)
{
	SetName(name);
	SetPrice(price);
	SetQuantity(quantity);
}

void Product::Display() const
{
	std::cout << std::left << std::fixed << std::setprecision(2);
	std::cout << std::setw(50) << m_name
		<< std::setw(10) << std::string("$" + Utility::StringUtil::ToString(m_price))
		<< std::setw(10) << m_quantity << std::endl;
}

void Product::SetName(std::string name)
{
	m_name = name;
}

void Product::SetPrice(float price)
{
	if (price < 0) return;
	m_price = price;
}

void Product::SetQuantity(int quantity)
{
	if (quantity < 0) return;
	m_quantity = quantity;
}

std::string Product::GetName() const
{
	return m_name;
}

float Product::GetPrice() const
{
	return m_price;
}

int Product::GetQuantity() const
{
	return m_quantity;
}
