#include "Program.h"

#include "../Utilities/Menu.h"

#include <iostream>
#include <iomanip>
#include <fstream>

void Program::SetDataPath(std::string path)
{
	m_dataPath = path;
	std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void Program::Setup()
{
    // TODO: Implement
}

void Program::Cleanup()
{
    // TODO: Implement
}

void Program::Run()
{
    // TODO: Implement
}

void Program::ReceiveCall()
{
    // TODO: Implement
}

void Program::ResolveCall()
{
    // TODO: Implement
}

void Program::RequeueCall()
{
    // TODO: Implement
}

void Program::SaveSummary()
{
    // TODO: Implement
}

void Program::SetupLog()
{
    // TODO: Implement
}

void Program::CleanupLog()
{
    // TODO: Implement
}
