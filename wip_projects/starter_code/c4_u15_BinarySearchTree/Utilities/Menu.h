#ifndef _MENU
#define _MENU

// C++ Library includes
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cstdlib>
#include <limits>
#include <map>
#include <functional>

namespace Utility
{

//! A class to help with CLI user interface formatting
class Menu
{
    public:
    // OUTPUT
    static void Header( const std::string& header );
    static void DrawHorizontalBar( int width, char symbol = '-' );

    // MENUS and INPUT/OUTPUT
    static void ShowMenu( const std::vector<std::string> options, bool vertical = true, bool zeroToQuit = false );
    static int ShowIntMenuWithPrompt( const std::vector<std::string> options, bool vertical = true, bool zeroToQuit = false );
    static std::string ShowStringMenuWithPrompt( const std::vector<std::string> options, bool vertical = true );
    static std::function<void(void)> ShowCallbackMenuWithPrompt( std::map<std::string, std::function<void(void)> > options, bool vertical = true );
    static int GetValidChoice( int min, int max, const std::string& message = "" );
    static std::string GetStringChoice( const std::string& message = "" );
    static std::string GetStringLine( const std::string& message = "" );
    static int GetIntChoice( const std::string& message = "" );
    template <class T> static void DrawTable( const std::vector<std::string>& headers, const std::vector<T>& data );

    // HANDY TRICKS
    static void ClearScreen();
    static void Pause();
    static void PrintPwd();

    private:
    static bool s_lastCinWasStream;

    static std::string CinStreamString();
    static int CinStreamInt();
    static std::string CinGetlineString();
};

template <class T>
void Utility::Menu::DrawTable( const std::vector<std::string>& headers, const std::vector<T>& data )
{
    int totalColumns = data.size();
    int screenColumns = 80;
    int blocksPerColumn = screenColumns / totalColumns;

    if ( headers.size() != 0 )
    {
        for ( auto& header : headers )
        {
            std::cout << std::left << std::setw( blocksPerColumn ) << header;
        }
        std::cout << std::endl;
        DrawHorizontalBar( 80, '-' );
    }

    for ( auto& dataItem : data )
    {
        std::cout << std::left << std::setw( blocksPerColumn ) << dataItem;
    }
    std::cout << std::endl;
}

} // End of namespace

#endif

