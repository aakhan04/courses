#ifndef _PROGRAM
#define _PROGRAM

#include "Product.h"

#include <vector> // temporary
#include <string>

//! The program class, contains the data and the menu functionality
class Program
{
public:
    //! Begin the program
	void Run();
	//! Set up path to external data file
	void SetDataPath(std::string path);

private:
    //! Get the program ready to start / load data
	void Setup();

	//! Clean up the program / save data
	void Cleanup();

    //! Main menu screen
	void Menu_Main();

	//! Screen for add product, utilizes PushFront, PushBack, and PushAt
	void Menu_AddProduct();

	//! Screen to edit an existing product, utilizes GetBack, GetFront, and GetAt
	void Menu_EditProduct();

	//! Screen to delete an existing product, utilizes PopFront, PopBack, and PopAt
	void Menu_DeleteProduct();

	//! Function to display all products in a list
	void DisplayProducts(const std::vector<Product>& productList);

    // TODO: Update this to use your SmartFixedArray
	std::vector<Product> m_products;
	std::string m_dataPath;
};

#endif
