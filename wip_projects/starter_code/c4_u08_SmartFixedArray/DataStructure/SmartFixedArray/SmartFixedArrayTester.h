#ifndef _SMART_FIXED_ARRAY_TESTER_HPP
#define _SMART_FIXED_ARRAY_TESTER_HPP

// C++ Library includes
#include <iostream>
#include <string>

// Project includes
#include "SmartFixedArray.h"
#include "../../CuTest/TesterBase.h"
#include "../../Utilities/Menu.h"
#include "../../Utilities/StringUtil.h"
#include "../../Utilities/Logger.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

using namespace Exception;

namespace DataStructure
{

//! TESTER for the SmartFixedArray
class SmartFixedArrayTester : public cuTest::TesterBase
{
public:
    SmartFixedArrayTester()
        : TesterBase( "test_result_smart_fixed_array.html" )
    {
        AddTest(cuTest::TestListItem("Constructor()",       std::bind(&SmartFixedArrayTester::Test_Constructor, this)));

        AddTest(cuTest::TestListItem("ShiftLeft()",         std::bind(&SmartFixedArrayTester::Test_ShiftLeft, this)));
        AddTest(cuTest::TestListItem("ShiftRight()",        std::bind(&SmartFixedArrayTester::Test_ShiftRight, this)));

        AddTest(cuTest::TestListItem("PopBack()",           std::bind(&SmartFixedArrayTester::Test_PopBack, this)));
        AddTest(cuTest::TestListItem("PopFront()",          std::bind(&SmartFixedArrayTester::Test_PopFront, this)));
        AddTest(cuTest::TestListItem("PopAt()",             std::bind(&SmartFixedArrayTester::Test_PopAt, this)));

        AddTest(cuTest::TestListItem("GetBack()",           std::bind(&SmartFixedArrayTester::Test_GetBack, this)));
        AddTest(cuTest::TestListItem("GetFront()",          std::bind(&SmartFixedArrayTester::Test_GetFront, this)));
        AddTest(cuTest::TestListItem("GetAt()",             std::bind(&SmartFixedArrayTester::Test_GetAt, this)));

        AddTest(cuTest::TestListItem("IsFull()",            std::bind(&SmartFixedArrayTester::Test_IsFull, this)));
        AddTest(cuTest::TestListItem("IsEmpty()",           std::bind(&SmartFixedArrayTester::Test_IsEmpty, this)));

        AddTest(cuTest::TestListItem("PushBack()",          std::bind(&SmartFixedArrayTester::Test_PushBack, this)));
        AddTest(cuTest::TestListItem("PushFront()",         std::bind(&SmartFixedArrayTester::Test_PushFront, this)));
        AddTest(cuTest::TestListItem("PushAt()",            std::bind(&SmartFixedArrayTester::Test_PushAt, this)));

        AddTest(cuTest::TestListItem("Search()",            std::bind(&SmartFixedArrayTester::Test_Search, this)));
    }

    virtual ~SmartFixedArrayTester() { }

private:
    int Test_Constructor();
    int Test_PushBack();
    int Test_PushFront();
    int Test_PushAt();
    int Test_PopBack();
    int Test_PopFront();
    int Test_PopAt();
    int Test_GetBack();
    int Test_GetFront();
    int Test_GetAt();
    int Test_IsFull();
    int Test_IsEmpty();
    int Test_ShiftLeft();
    int Test_ShiftRight();
    int Test_Search();
};

int SmartFixedArrayTester::Test_Constructor()
{
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_Constructor", 3 );
    StartTestSet( "Test_Constructor", { } );
//    std::ostringstream oss;

    StartTest( "0a. Check if functions are implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( "Prerequisite functions", std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;             }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( "Prerequisite functions" ); }
        else                        { return PrereqTest_Abort( "Prerequisite functions" ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Check initial member variable values" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "When a new SmartFixedArray is created, m_itemCount and should be 0 and ARRAY_SIZE should be 100." );

        SmartFixedArray<int> arr;

        if      ( !Set_Outputs( "ARRAY_SIZE", 100, arr.ARRAY_SIZE ) )   { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 0, arr.m_itemCount ) )   { TestFail(); }
        else                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_PushBack()
{
    std::string functionName = "PushBack";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName, "Resize" } );

    StartTest( "0a. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     /**/ arr.PushBack( 5 ); /**/   }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Add three items, check they're in the correct position" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        std::string values[] = { "a", "b", "c" };
        int expectedItemCount = 3;

        SmartFixedArray<std::string> arr;
        for ( int i = 0; i < 3; i++ )   { arr.PushBack( values[i] ); }

        Set_ExpectedOutput( "m_array", std::string( "not nullptr" ) );
        for ( int i = 0; i < 3; i++ )   { Set_ExpectedOutput( "m_array[" + Utility::StringUtil::ToString( i ) + "]", values[i] ); }

//        std::ostringstream oss;
//        oss << arr.m_array;
//        Set_ActualOutput    ( "m_array", oss.str() );
        if ( arr.m_array == nullptr )
        {
            Set_ActualOutput    ( "m_array", "nullptr" );
        }
        else
        {
            Set_ActualOutput    ( "m_array", "not nullptr" );
        }

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", expectedItemCount, arr.m_itemCount ) )   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", values[0], arr.m_array[0] ) )             { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", values[1], arr.m_array[1] ) )             { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", values[2], arr.m_array[2] ) )             { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check if StructureFullException is thrown if array is full" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool exceptionThrown = false;

        try                                     {   SmartFixedArray<int> arr;     arr.m_itemCount = arr.ARRAY_SIZE;      arr.PushBack( 2 );       }
        catch( StructureFullException& ex )     {   exceptionThrown = true;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        Set_Outputs( std::string( "Exception is thrown" ), "true", exceptionThrown );
        if  ( exceptionThrown )     { TestPass(); }
        else                        { TestFail(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_PushFront()
{
    std::string functionName = "PushFront";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName, "AllocateMemory", "Resize" } );
//    std::ostringstream oss;

    StartTest( "0a. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     /**/ arr.PushFront( 5 ); /**/             }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Add three items, check they're in the correct position" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.PushFront( "a" );
        arr.PushFront( "b" );
        arr.PushFront( "c" );

//        std::ostringstream oss;
//        oss << arr.m_array;
        Set_ExpectedOutput( "m_array", std::string( "not nullptr" ) );
        if ( arr.m_array == nullptr )
        {
            Set_ActualOutput    ( "m_array", "nullptr" );
        }
        else
        {
            Set_ActualOutput    ( "m_array", "not nullptr" );
        }

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 3, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "c" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "b" ), arr.m_array[1] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string( "a" ), arr.m_array[2] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check if StructureFullException is thrown if array is full" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool exceptionThrown = false;

        try                                {   SmartFixedArray<int> arr;     arr.m_itemCount = arr.ARRAY_SIZE;      arr.PushFront( 2 );       }
        catch( StructureFullException& ex )    {   exceptionThrown = true;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        Set_Outputs( std::string( "Exception is thrown" ), "true", exceptionThrown );
        if  ( exceptionThrown )     { TestPass(); }
        else                        { TestFail(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */


    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_PushAt()
{
    std::string functionName = "PushAt";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0a. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.PushAt( 5, 5 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, insert at some position" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        arr.PushAt( "z", 1 );

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 4, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "a" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "z" ), arr.m_array[1] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string( "b" ), arr.m_array[2] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[3]", std::string( "c" ), arr.m_array[3] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check if StructureFullException is thrown if array is full" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool exceptionThrown = false;

        try                                {   SmartFixedArray<int> arr;     arr.m_itemCount = arr.ARRAY_SIZE;      arr.PushAt( 0, 2 );       }
        catch( StructureFullException& ex )    {   exceptionThrown = true;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        Set_Outputs( std::string( "Exception is thrown" ), "true", exceptionThrown );
        if  ( exceptionThrown )     { TestPass(); }
        else                        { TestFail(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. ERROR CHECK: Check for exception when inserting outside of range" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array itemCount is 5, trying to call InsertAt( \"z\", 50 )." );
        SmartFixedArray<std::string> arr;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_array[3] = "d";
        arr.m_array[4] = "e";
        arr.m_itemCount = 5;

        bool exceptionOccurred = false;

        try
        {
            arr.PushAt( "z", 50 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_PopBack()
{
    std::string functionName = "PopBack";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.PopBack();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, remove back" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        arr.PopBack();

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 2, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "a" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "b" ), arr.m_array[1] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception with empty array" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty, calling PopBack()." );
        SmartFixedArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.PopBack();
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_PopFront()
{
    std::string functionName = "PopFront";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.PopFront();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, remove front" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        arr.PopFront();

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 2, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "b" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "c" ), arr.m_array[1] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception with empty array" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty, calling PopFront()." );
        SmartFixedArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.PopFront();
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_PopAt()
{
    std::string functionName = "PopAt";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.PopAt( 1 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;   Utility::Logger::Out( ex.what() );   }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, remove at position" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        arr.PopAt( 1 );

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 2, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "a" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "c" ), arr.m_array[1] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception with invalid index" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "ARRAY_SIZE is 100. Calling PopAt( 150 )." );
        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 3;

        bool exceptionOccurred = false;

        try
        {
            arr.PopAt( 150 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. ERROR CHECK: Check for exception with empty array" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty, calling PopAt( 0 )." );
        SmartFixedArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.PopAt( 0 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_GetBack()
{
    std::string functionName = "GetBack";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.GetBack();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, get back" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        if      ( !Set_Outputs( "GetBack()", std::string( "c" ), arr.GetBack() ) )       { TestFail(); }
        else                                                                        { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception when array is empty" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty. Calling GetBack()." );
        SmartFixedArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.GetBack();
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_GetFront()
{
    std::string functionName = "GetFront";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.GetFront();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;   Utility::Logger::Out( ex.what() );   }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, get front" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        if      ( !Set_Outputs( "GetFront()", std::string( "a" ), arr.GetFront() ) )      { TestFail(); }
        else                                                                         { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception when array is empty" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty. Calling GetFront()." );
        SmartFixedArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.GetFront();
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_GetAt()
{
    std::string functionName = "GetAt";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.GetAt( 5 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, get at 1" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        if      ( !Set_Outputs( "GetAt( 1 )", std::string( "b" ), arr.GetAt( 1 ) ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception with invalid index" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "ARRAY_SIZE is 100. Calling GetAt( 250 )." );
        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 3;

        bool exceptionOccurred = false;

        try
        {
            arr.GetAt( 250 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. ERROR CHECK: Check for exception when array is empty" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty. Calling GetAt( 0 )." );
        SmartFixedArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.GetAt( 0 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_IsFull()
{
    std::string functionName = "IsFull";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.IsFull();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Check if IsFull returns true when full" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 100;
        if      ( !Set_Outputs( "IsFull()", true, arr.IsFull() ) )         { TestFail(); }
        else                                                               { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Check if IsFull returns false when not full" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 3;
        if      ( !Set_Outputs( "IsFull()", false, arr.IsFull() ) )         { TestFail(); }
        else                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_IsEmpty()
{
    std::string functionName = "IsEmpty";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.IsEmpty();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Check if " + functionName + " returns false when an item is in the array" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 100;
        if      ( !Set_Outputs( functionName, false, arr.IsEmpty() ) )     { TestFail(); }
        else                                                               { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Check if " + functionName + " returns true when empty" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 0;
        if      ( !Set_Outputs( functionName, true, arr.IsEmpty() ) )       { TestFail(); }
        else                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_ShiftLeft()
{
    std::string functionName = "ShiftLeft";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    functionName = "Clear (prereq)";
    StartTest( "0a. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     /**/ arr.ShiftLeft( 0 ); /**/   }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    functionName = "ShiftLeft";
    StartTest( "0b. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        SmartFixedArray<int> arr;
        try
        {
            arr.ShiftLeft( 0 );
        }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Populate array, call ShiftLeft, check positions" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        std::string originalValues[] = { "a", "b", "c", "d", "-" };

        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 4;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_array[3] = "d";
        arr.ShiftLeft( 1 );

        if      ( !Set_Outputs( "m_array[0]", std::string("a"), arr.m_array[0] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string("c"), arr.m_array[1] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string("d"), arr.m_array[2] ) )        { TestFail(); }
        else                                                                        { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Try to shift array out-of-bounds..." ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "ARRAY_SIZE is 100, calling ShiftLeft( 300 )." );
        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 5;

        bool exceptionOccurred = false;

        try
        {
            arr.ShiftLeft( 300 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        Set_ExpectedOutput( "Exception occurred", true );
        Set_ActualOutput( "Exception occurred", exceptionOccurred );

        if      ( !exceptionOccurred )          { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}


int SmartFixedArrayTester::Test_ShiftRight()
{
    std::string functionName = "ShiftRight";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    functionName = "Clear (prereq)";
    StartTest( "0a. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     /**/ arr.ShiftLeft( 0 ); /**/   }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    functionName = "ShiftRight";
    StartTest( "0b. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        SmartFixedArray<int> arr;

        try                                     {
            arr.ShiftRight( 0 );
        }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Populate array, call ShiftRight, check positions" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        std::string originalValues[] = { "a", "b", "c", "d", "-" };

        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 4;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_array[3] = "d";
        arr.ShiftRight( 1 );

        if      ( !Set_Outputs( "m_array[0]", std::string("a"), arr.m_array[0] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string("b"), arr.m_array[2] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[3]", std::string("c"), arr.m_array[3] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[4]", std::string("d"), arr.m_array[4] ) )        { TestFail(); }
        else                                                                        { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Try to shift array out-of-bounds..." ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "ARRAY_SIZE is 100, calling ShiftRight( 300 )." );
        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 5;

        bool exceptionOccurred = false;

        try
        {
            arr.ShiftRight( 300 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        Set_ExpectedOutput( "Exception occurred", true );
        Set_ActualOutput( "Exception occurred", exceptionOccurred );

        if      ( !exceptionOccurred )          { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartFixedArrayTester::Test_Search()
{
    std::string functionName = "Search";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartFixedArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
//    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartFixedArray<int> arr;     arr.Search( 10 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, search for an item." ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartFixedArray<std::string> arr;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        if      ( !Set_Outputs( "Search( \"b\" )", 1, arr.Search( "b" ) ) )             { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Search for an item that isn't in the array." ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Searching for \"z\" in the array." );
        SmartFixedArray<std::string> arr;
        arr.m_itemCount = 3;

        bool exceptionOccurred = false;

        try
        {
            arr.Search( "z" );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

} // End of namespace

#endif
