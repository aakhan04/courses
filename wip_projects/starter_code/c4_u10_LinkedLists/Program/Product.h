#ifndef _PRODUCT
#define _PRODUCT

#include <string>

class Product
{
public:
	Product();
	Product(std::string name, float price, int quantity);

	void Setup(std::string name, float price, int quantity);
	void Display() const;

	void SetName(std::string name);
	void SetPrice(float price);
	void SetQuantity(int quantity);
	std::string GetName() const;
	float GetPrice() const;
	int GetQuantity() const;

private:
	std::string m_name;
	float m_price;
	int m_quantity;
};

#endif