var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuw~",
  1: "ilmnpst",
  2: "cdeu",
  3: "dilmnprst",
  4: "abcdefghimnoprstw~",
  5: "acmnpst",
  6: "s",
  7: "cg"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "related",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Friends",
  7: "Pages"
};

