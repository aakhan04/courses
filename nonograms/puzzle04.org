# -*- mode: org -*-

#+TITLE: Nonogram Puzzle 04
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

[[file:images/nonogram-07.png]]

Hint: Tree

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

[[file:images/nonogram-08.png]]

Hint: Chocolate chip cookie

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


-----
