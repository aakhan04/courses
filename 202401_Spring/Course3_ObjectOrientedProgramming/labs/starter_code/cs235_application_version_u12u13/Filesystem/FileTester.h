#ifndef _FILE_TESTER
#define _FILE_TESTER

#include "File.h"

// !! The tests have already been implemented !!

class FileTester
{
public:
    FileTester();
    void Run();
    void Test_Constructors();
    void Test_OpenFile();
    void Test_Write();

private:
    const std::string RED;
    const std::string GRN;
    const std::string BOLD;
    const std::string CLR;

    std::string BoolToStr( bool val )
    {
        return ( val ) ? "true" : "false";
    }
};

#endif
