#ifndef _ACCOUNT
#define _ACCOUNT

#include <string>

class Account
{
    public:
    Account( size_t id = 0, std::string username = "", size_t password_hash = 0 );

    void SetUsername( std::string username );
    void SetPasswordHash( size_t password_hash );

    size_t GetId() const;
    std::string GetUsername() const;

    bool CorrectPassword( size_t attempt_hash );

    private:
    size_t id;
    std::string username;
    size_t password_hash;

    friend class AccountManager;
    friend class AccountTester;
    friend class AccountManagerTester;
};

#endif
