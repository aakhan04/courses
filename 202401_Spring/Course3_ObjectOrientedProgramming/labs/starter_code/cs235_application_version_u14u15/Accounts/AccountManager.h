#ifndef _ACCOUNT_MANAGER
#define _ACCOUNT_MANAGER

#include "Account.h"

#include <vector>
#include <string>

class AccountManager
{
    public:
    static void Setup( std::string data_path );
    static void Teardown();
    static void Clear();

    static void SaveData();
    static void LoadData();

    static size_t CreateAccount( std::string username, size_t password_hash );
    static Account GetAccount( size_t id );
    static size_t GetIndexOfAccount( size_t id );
    static void DeleteAccount( size_t id );
    static void DisplayAccountsTable();

    private:
    static std::string save_path;
    static std::vector<Account> accounts;

    friend class AccountManagerTester;
};

#endif
