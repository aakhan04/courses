#include "AccountManager.h"
#include "../Exceptions/NotImplementedException.h"

#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <fstream>

// STATIC MEMBER VARIABLE INITIALIZATION:
std::string AccountManager::save_path;
std::vector<Account> AccountManager::accounts;

// ** MAKE SURE TO REFERENCE Account.h TO SEE MEMBER FUNCTIONS AND VARIABLES OF THE Account CLASS! **

/**
@param      username        The new account's username
@param      password_hash   The new account's hashed password
@return                     The id of the new user (NOT index)

1. Create a new Account, set its ID to 1000 plus the current size of the `accounts` vector.
2. Set the new account's username to the corresponding parameter.
3. Set the new account's password hash to the corresponding parameter.
4. Push the new account into the `accounts` vector.
5. Return the ID of the account you just created (NOT the index).
*/
size_t AccountManager::CreateAccount( std::string username, size_t password_hash )
{
//    throw Exception::NotImplementedException( "AccountManager::CreateAccount" ); // TODO: REMOVE ME!!
    Account new_account( 1000 + accounts.size() );
    new_account.SetUsername( username );
    new_account.SetPasswordHash( password_hash );
    accounts.push_back( new_account );
    return new_account.GetId();
}

/**
@param      id      The account ID of a certain user.
@return             The index of the Account in the `accounts` vector.

1. Iterate over all the elements of the `accounts` vector using a for loop.
    1a. Inspect each element's ID. If its ID matches the `id` passed in, then return this index (`i`).
2. After the for loop, at this point nothing has been found. In this case, throw an `out_of_range` exception because no matching ID was found.
*/
size_t AccountManager::GetIndexOfAccount( size_t id )
{
    throw Exception::NotImplementedException( "AccountManager::GetIndexOfAccount" ); // TODO: REMOVE ME!!
}

/**
@param      id      The account ID of a certain user.
@return             A copy of the Account element from `accounts` with that matching ID.

Use the `GetIndexOfAccount` function to get the index of the account that has the `id` given. Return the matching Account.
*/
Account AccountManager::GetAccount( size_t id )
{
    throw Exception::NotImplementedException( "AccountManager::GetAccount" ); // TODO: REMOVE ME!!
}

/**
@param      id      The account ID of a certain user.

Use the `GetIndexOfAccount` function to get the index of the account that has the `id` given.
Use the `accounts` vector's `erase` function to remove this item.

Refer to the vector documentation for how to use the erase function:
https://cplusplus.com/reference/vector/vector/erase/
*/
void AccountManager::DeleteAccount( size_t id )
{
    throw Exception::NotImplementedException( "AccountManager::DeleteAccount" ); // TODO: REMOVE ME!!
}




void AccountManager::Setup( std::string data_path )             // Already implemented
{
    // Create default 0 user
    save_path = data_path;
    LoadData();
}

void AccountManager::Teardown()                                 // Already implemented
{
    SaveData();
}

void AccountManager::Clear()
{
    accounts.clear();
}

void AccountManager::SaveData()                                 // Already implemented
{
    std::ofstream output( save_path );
    for ( size_t i = 0; i < accounts.size(); i++ )
    {
        output << accounts[i].GetId() << " ";
        output << accounts[i].GetUsername() << " ";
        output << accounts[i].password_hash << std::endl;
    }
}

void AccountManager::LoadData()                                 // Already implemented
{
    std::ifstream input( save_path );
    if ( input.fail() )
    {
        // No data file; create default user
        CreateAccount( "default", 0 );
        return;
    }

    std::string buffer, user;
    size_t id, pass;

    while ( input >> id >> user >> pass )
    {
        accounts.push_back( Account( id, user, pass ) );
    }
}

void AccountManager::DisplayAccountsTable()                     // Already implemented
{
    std::cout << std::left;
    std::cout << std::setw( 10 ) << "ID"
              << std::setw( 20 ) << "USERNAME"
              << std::setw( 20 ) << "PASSHASH"
              << std::endl;
    std::cout << std::string( 80, '-' ) << std::endl;
    for ( size_t i = 0; i < accounts.size(); i++ )
    {
        std::cout << std::setw( 10 ) << accounts[i].GetId()
                  << std::setw( 20 ) << accounts[i].GetUsername()
                  << std::setw( 20 ) << accounts[i].password_hash
                  << std::endl;
    }
}

