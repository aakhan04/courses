# -*- mode: org -*-

#+CAPTION: Screenshot of VisiCalc running on an Apple II computer, by Gortu, CC0, https://en.wikipedia.org/wiki/VisiCalc#/media/File:Visicalc.png
[[file:images/reading_u01_ExploringDesign_Visicalc_Wikipedia_Gorthu.png]]

Software on computers have come a long way over the decades. Desktop software, mobile apps, and websites handle so much more
data and have much more functionality than what was around 20 or more years ago. As a code base expands in *scope*,
*software architects* try to structure the software they work on to be *scalable*, *efficient*, and *maintainable*
(though businesses often don't prioritize code maintenance so often times corporate code bases are big and messy and terrible. :)

If you get a job as a software developer, chances are the code base you join will be designed around the *object oriented programming* paradigm.
An interconnected network of *objects* that have *attributes* (variables) and *functionality*, and which interact with other objects across the code base.
Many objects represent data that will get stored in a *database*, some objects' purpose may be to facilitate interactions between other objects.
With languages like Java and C#, everything must be built into a class, though C++ makes this optional, allowing for different styles of coding as well.

In this section we will look at some example designs for apps and websites you may commonly use.

--------------------------------------------
* Example: Spotify

We can get some kind of idea of the underlying structure of a system by looking at the *API* (Application Programming Interface)
that it exposes to the outside world. Many modern websites offer APIs in order to allow third parties to create helper programs
and services that work with the data stored by the original service.

Looking at the Spotify API documentation (https://developer.spotify.com/documentation/web-api), we can see several main "objects" it provides:

1. Albums
2. Artists
3. Audiobooks
4. Categories
5. Chapters
6. Episodes
7. Genres
8. Markets
9. Player
10. Playlists
11. Search
12. Shows
13. Tracks
14. Users

If you click on one of the objects, it will show an example of requests that an external program can call, such as *Get Album*, *Get Album Tracks*, *Save Albums for Current User*, and so on.

Clicking on a request type will also show a *Response Sample*, which shows what kind of information may be returned with a call, such as *Get Album*:

#+CAPTION: Example response data from the Spotify API documentation
#+BEGIN_SRC json :class json
{
  "album_type": "compilation",
  "total_tracks": 9,
  "available_markets": [
    "CA",
    "BR",
    "IT"
  ],
  "id": "2up3OPMp9Tb4dAKM2erWXQ",
  "images": [
    {
      "url": "https://i.scdn.co/image/ab67616d00001e02ff9ca10b55ce82ae553c8228",
      "height": 300,
      "width": 300
    }
  ],
  "release_date": "1981-12",
  "type": "album",
  "artists": [
    {
      "id": "string",
      "name": "string",
    }
  ],
  "tracks": {
    "total": 4,
    "href": "https://api.spotify.com/v1/me/shows?offset=0&limit=20",
    "next": "https://api.spotify.com/v1/me/shows?offset=1&limit=1",
    "previous": "https://api.spotify.com/v1/me/shows?offset=1&limit=1"
  },
  "copyrights": [
    {
      "text": "string",
      "type": "string"
    }
  ],
  "genres": [
    "Egg punk",
    "Noise rock"
  ],
  "popularity": 0
}
#+END_SRC

The data and the form it's returned in via API response isn't necessarily how the data is stored in the database itself,
or how the data is stored in objects when worked on by the Spotify servers themselves. Usually, API systems pulls data
from multiple sources and bundles it together to return as a response. But, this can at least give us some idea of how
the actual system may be organized.

[[file:images/reading_u01_ExploringDesign_Spotify.png]]

This is just a small example, containing a "Music" section (Artist, Album, Track) and a "User" section (User, Playlist).
The arrows show relationships /between/ the objects. But this diagram is also lacking any of the back-end functionality
that helps everything actually /do/ something. On their own, it's just data stored somewhere. A program has to be written
around it.



--------------------------------------------
* Software layers

Software is built in *layers*. The architecture itself may change over time, so we're going to keep this pretty general.
Basically, you'll have a *front-end*, which is the interface the user sees when working with the program.
The user isn't exposed to all of the complexity of the program itself - we give them some kind of nice interface to work with,
and they don't care /how/ anything actually works.

[[file:images/reading_u01_ExploringDesign_interface3.png]]

The *front-end* is hooked into the program *back-end*, which handles logic. This may also read from or write to
storage, or a *database*.

[[file:images/reading_u01_ExploringDesign_interface1.png]]


A lot of utilities we use these days are *websites*, not desktop software, so the *server* we're contacting
has the *back-end* and all its logic, as well as access to its *databases*. It then sends webpages or other forms
of information to our computer (the *client*), which will be displayed - such as rendered by a *web browser*.

[[file:images/reading_u01_ExploringDesign_interface2.png]]


Again, architecture can change quickly. At the moment, *microservices* are a popular way to architect websites. We aren't
going to go into depth into this, however, since we're going to just be working with console-based desktop software in this course.


--------------------------------------------
* Basic desktop program design for this class

Here's an example of how we can may design programs in this course for larger projects.
I have a "database" labeled but data saving and loading for us will just be via text documents
or CSV files. (We're not going to set up database stuff because everyone's platforms are different
and we don't have time for that. ;)

[[file:images/reading_u01_ExploringDesign_Program.png]]

In the yellow, =BankTellerProgram= and =CustomerATMProgram= are two different, separate programs.
One would theoretically be used by a bank employee, and they have access to more sensitive information
as part of their job. The customer's ATM might only have some basic features like checking balance,
depositing, and withdrawing. For us, these act as the "interface layer".

We have basic objects like =BankAccount= and =Customer=, which are more obvious instances of turning
real-world concepts into objects (classes) in C++.

Then we have the =Manager= objects, which contain all of the data for whatever they manage
(=BankAccountManager= has a =vector<BankAccount>=, =CustomerManager= has a =vector<Customer>=)
and interfaces to allow other parts of programs to access the data. This way, we're not cluttering
up the Programs themselves with the logic needed to manage Bank Accounts or Customers.

How does this design "flow"? Here's an example diagram of the process for a bank teller to add a new account:

[[file:images/reading_u01_ExploringDesign_ProgramFlow.png]]

- When the program begins, we enter =main()=, but we don't put much program logic in there - we just create and run a =BankTellerProgram=.
- It starts at the main menu, and then the bank teller would select an option from the list.
- We're adding a new account, so it calls =Menu_AddNewAccount()= in the =BankTellerProgram=.
- Within this function, it will display a list of all customers by calling =CustomerManager= 's =GetAllCustomers()= function (or perhaps we could add a Display function).
- Next, it asks the bank teller to enter the ID of the customer we're creating an account for.
- Then, we create a new =BankAccount= object and set up its initial information.
- After that's done, we call the =BankAccountManager= 's =AddAccount= function, passing in the new account object we created.
- At the end of the =Menu_AddNewAccount()= function, it returns the teller back to the main menu so they can select the next operation that they'll perform.

Utilize this example for the "*🧠 Unit 01 Tech Literacy - Exploring software design (U01.TEC.202401CS235)*" assignment,
where you will come up with some example objects.
