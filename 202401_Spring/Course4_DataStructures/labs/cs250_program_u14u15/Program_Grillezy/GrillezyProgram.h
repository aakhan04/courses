#ifndef _GRILLEZY_PROGRAM
#define _GRILLEZY_PROGRAM

#include "../DataStructure/Queue/ArrayQueue.h"
#include "../DataStructure/Queue/LinkedQueue.h"
#include "../DataStructure/Stack/ArrayStack.h"
#include "../DataStructure/Stack/LinkedStack.h"
#include "../DataStructure/LinkedList/LinkedList.h"
#include "Message.h"
#include "Sandwich.h"

#include <vector> // temporary
#include <stack> // temporary
#include <queue> // temporary
#include <string>
#include <fstream>

namespace Grillezy
{

class GrillezyProgram
{
public:
    void Run();
    void SetDataPath(std::string path);

private:
    void ReadMessages();
    void ProcessMessages();
    void SaveResults();

	DataStructure::ArrayQueue<Message> m_messageQueue;

	DataStructure::ArrayStack<std::string> m_workingStack;

	DataStructure::LinkedList<Sandwich> m_results;

    std::string m_dataPath;
    std::ofstream m_log;
};

}

#endif
