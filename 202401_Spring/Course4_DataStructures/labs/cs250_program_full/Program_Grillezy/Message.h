#ifndef _GRILLEZY_MESSAGE
#define _GRILLEZY_MESSAGE

#include <string>

namespace Grillezy
{

class Message
{
public:
    void Setup( std::string newAction, std::string newValue );
    std::string GetAction();
    std::string GetValue();

private:
    std::string m_action;
    std::string m_value;
};

}

#endif
