#include "GrillezyProgram.h"

#include <iostream>
#include <iomanip>
#include <fstream>

namespace Grillezy
{

void Program::SetDataPath(std::string path)
{
    m_dataPath = path;
    std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void Program::Run()
{
    m_log.open( m_dataPath + "log.txt" );
    ReadMessages();
    ProcessMessages();
    SaveResults();
    m_log.close();
}

void Program::ReadMessages()
{
    m_log << endl << string( 80, '-' ) << endl << "void Program::ReadMessages()" << endl;
    cout << endl << string( 80, '-' ) << endl << "void Program::ReadMessages()" << endl;

    /*
    Normally with a message queue, we would be receiving messages via the network/internet
    and storing them in the queue to be processed in the order received.
    The text file here has all the commands, which is supposed to "simulate" the messages
    received.
    */

    ifstream input( m_dataPath + "message-input.txt" );
    if ( input.fail() )
    {
        std::cout << "ERROR! Could not find data file at \"" << m_dataPath + "message-input.txt" << "\"! Please double check the path!" << std::endl;
        return;
    }

    std::string action, value;

    while ( input >> action )
    {
        // Really basic file parsing
        if      ( action == "ADD" )
        {
            input >> value;
        }
        else if ( action == "REMOVE" )
        {
            value = "";
        }
        else if ( action == "FINISH" )
        {
            value = "";
        }

        Message newMessage;
        newMessage.Setup( action, value );
        m_messageQueue.Push( newMessage );

        m_log << "Added [" << action << ", " << value << "] to message queue." << endl;
        cout << "Added [" << action << ", " << value << "] to message queue." << endl;
    }

    m_log << "Message queue size is now: " << m_messageQueue.Size() << endl;
}

void Program::ProcessMessages()
{
    m_log << endl << string( 80, '-' ) << endl << "void Program::ProcessMessages()" << endl;
    cout << endl << string( 80, '-' ) << endl << "void Program::ProcessMessages()" << endl;

    /*
    We're assembling sandwiches here for the sake of conceptual simplicity, this isn't
    how sandwiches would be built on, say, a restaurant app. Stacks are usually used for
    things more like processing program code blocks, handling program views and the
    ability to "go back", and other things.
    */

    while ( !m_messageQueue.IsEmpty() )
    {
        m_log << "Process message [" << m_messageQueue.Front().GetAction() << ", " << m_messageQueue.Front().GetValue() << "]." << endl;
        cout << "Process message [" << m_messageQueue.Front().GetAction() << ", " << m_messageQueue.Front().GetValue() << "]." << endl;

        if ( m_messageQueue.Front().GetAction() == "ADD" )
        {
            m_log << "Add \"" << m_messageQueue.Front().GetValue() << "\" to the sandwich." << endl;
            cout << "Add \"" << m_messageQueue.Front().GetValue() << "\" to the sandwich." << endl;

            m_workingStack.Push( m_messageQueue.Front().GetValue() );
        }
        else if ( m_messageQueue.Front().GetAction() == "REMOVE" )
        {
            m_log << "Remove last added item from the sandwich." << endl;
            cout << "Remove last added item from the sandwich." << endl;

            m_workingStack.Pop();
        }
        else if ( m_messageQueue.Front().GetAction() == "FINISH" )
        {
            m_log << "Sandwich is finished, add to results list." << endl;
            cout << "Sandwich is finished, add to results list." << endl;

            Sandwich wipSandwich;

            while ( m_workingStack.Size() > 0 )
            {
                wipSandwich.Add( m_workingStack.Top() );
                m_workingStack.Pop();
            }

            m_results.PushBack( wipSandwich );
        }

        m_messageQueue.Pop();
    }
}

void Program::SaveResults()
{
    m_log << endl << string( 80, '-' ) << endl << "void Program::SaveResults()" << endl;
    cout << endl << string( 80, '-' ) << endl << "void Program::SaveResults()" << endl;

    string path = m_dataPath + "results-output.txt";
    ofstream output( path );

    for ( size_t i = 0; i < m_results.Size(); i++ )
    {
        m_log << "Writing out sandwich:" << endl << m_results.GetAt( i ) << endl;
        cout << "Writing out sandwich:" << endl << m_results.GetAt( i ) << endl;

        output << m_results.GetAt( i ) << endl;
    }

    output.close();
    m_log << endl << "Results written to: " << path << endl;
    cout << endl << "Results written to: " << path << endl;
}

}
