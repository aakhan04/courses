#ifndef _SHOPAZON_PROGRAM
#define _SHOPAZON_PROGRAM

#include "Product.h"

#include "../DataStructure/LinkedList/LinkedList.h"
//#include <vector> // temporary
#include <string>

namespace Shopazon
{

class Program
{
public:
    void Run();
    void SetDataPath(std::string path);

private:
    void Setup();
    void Cleanup();

    void Menu_Main();
    void Menu_AddProduct();
    void Menu_EditProduct();
    void Menu_DeleteProduct();

    void DisplayProducts(const DataStructure::LinkedList<Product>& productList);

    DataStructure::LinkedList<Product> m_products;
    std::string m_dataPath;
};

}

#endif
