#include "ShopazonProgram.h"

#include <iostream>
#include <iomanip>
#include <fstream>

#include "../Namespace_Utilities/Menu.h"
#include "../Namespace_Utilities/Logger.h"

namespace Shopazon
{

void Program::SetDataPath(std::string path)
{
    m_dataPath = path;
    std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void Program::Setup()
{
    // Load in the data file
    std::ifstream input(m_dataPath + "products.txt");
    if (input.fail())
    {
        std::cout << "ERROR! Could not find data file at \"" << m_dataPath + "products.txt" << "\"! Please double check the path!" << std::endl;
        return;
    }

    int size;
    input >> size;

    for (int i = 0; i < size; i++)
    {
        std::string name;
        float price;
        int quantity;

        input.ignore();
        std::getline(input, name);
        input >> price;
        input >> quantity;

        m_products.PushBack(Product(name, price, quantity));
    }
}

void Program::Cleanup()
{
    // Save data
    std::ofstream output(m_dataPath);
    output << m_products.Size() << std::endl;

    for (size_t i = 0; i < m_products.Size(); i++)
    {
        output << m_products.GetAt(i).GetName() << std::endl;
        output << m_products.GetAt(i).GetPrice() << std::endl;
        output << m_products.GetAt(i).GetQuantity() << std::endl;
    }
}

void Program::Run()
{
    Setup();
    Menu_Main();
    Cleanup();
}

void Program::Menu_Main()
{
    bool done = false;
    while (!done)
    {
        Utilities::Menu::Header("MAIN MENU");

        DisplayProducts(m_products);

        std::cout << std::endl << "OPTIONS" << std::endl;
        int choice = Utilities::Menu::ShowIntMenuWithPrompt(
        {
            "Add product",
            "Edit product",
            "Delete product",
        }, true, true);

        switch (choice)
        {
        case 0:
            done = true;
            break;

        case 1:
            Menu_AddProduct();
            break;

        case 2:
            Menu_EditProduct();
            break;

        case 3:
            Menu_DeleteProduct();
            break;
        }

    }
}

void Program::Menu_AddProduct()
{
    Utilities::Menu::Header("ADD NEW PRODUCT");

    std::string name;
    float price;
    int quantity;

    std::cout << "Enter new product's NAME: ";
    std::cin.ignore();
    getline(std::cin, name);

    std::cout << "Enter new product's PRICE: $";
    std::cin >> price;

    std::cout << "Enter new product's QUANTITY: ";
    std::cin >> quantity;

    m_products.PushBack(Product(name, price, quantity));

    std::cout << std::endl << "Product added." << std::endl;

    Utilities::Menu::Pause();
}

void Program::Menu_EditProduct()
{
    Utilities::Menu::Header("EDIT EXISTING PRODUCT");

    DisplayProducts(m_products);

    int index;
    index = Utilities::Menu::GetValidChoice(0, m_products.Size() - 1, "Enter ID of item to edit: ");

    std::cout << std::endl << "Edit which field?" << std::endl;
    int field = Utilities::Menu::ShowIntMenuWithPrompt(
    {
        "Name",
        "Price",
        "Quantity"
    });

    switch (field)
    {
    case 1:
    {
        std::string newName;
        std::cout << "Enter new name: ";
        std::cin.ignore();
        std::getline(std::cin, newName);
        m_products.GetAt(index).SetName(newName);
        break;
    }

    case 2:
    {
        float newPrice;
        std::cout << "Enter new price: $";
        std::cin >> newPrice;
        m_products.GetAt(index).SetPrice(newPrice);
        break;
    }

    case 3:
    {
        int newQuantity;
        std::cout << "Enter new quantity: ";
        std::cin >> newQuantity;
        m_products.GetAt(index).SetQuantity(newQuantity);
        break;
    }
    }

    std::cout << std::endl << "Product updated." << std::endl;

    Utilities::Menu::Pause();
}

void Program::Menu_DeleteProduct()
{
    Utilities::Menu::Header("DELETE PRODUCT");

    DisplayProducts(m_products);

    int index;
    index = Utilities::Menu::GetValidChoice(0, m_products.Size() - 1, "Enter ID of item to remove: ");

    m_products.PopAt(index);

    std::cout << std::endl << "Product removed." << std::endl;

    Utilities::Menu::Pause();
}

void Program::DisplayProducts(const DataStructure::LinkedList<Product>& productList)
{
    std::cout << std::left;
    std::cout << std::setw(5) << "ID"
              << std::setw(50) << "NAME"
              << std::setw(10) << "PRICE"
              << std::setw(10) << "QUANTITY"
              << std::endl << std::string(80, '-') << std::endl;
    for (size_t i = 0; i < m_products.Size(); i++)
    {
        std::cout << std::setw(5) << i;
        m_products.GetAt(i).Display();
    }
}

}
