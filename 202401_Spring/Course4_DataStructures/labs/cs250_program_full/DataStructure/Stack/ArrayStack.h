#ifndef ARRAY_STACK_HPP
#define ARRAY_STACK_HPP

#include "../SmartDynamicArray/SmartDynamicArray.h"
#include "../../Namespace_Utilities/Logger.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

namespace DataStructure
{

template <typename T>
//! A last-in-first-out (LIFO) stack structure built on top of an array
class ArrayStack
{
public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop();
    //! Access the data at the front of the queue
    T& Top();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

private:
    SmartDynamicArray<T> m_vector;

    friend class StackTester;
};

template <typename T>
void ArrayStack<T>::Push( const T& newData )
{
//    throw Exception::NotImplementedException( "ArrayStack::Push is not implemented" );
    m_vector.PushBack( newData );
}

template <typename T>
void ArrayStack<T>::Pop()
{
//    throw Exception::NotImplementedException( "ArrayStack::Pop is not implemented" );
    m_vector.PopBack();
}

template <typename T>
T& ArrayStack<T>::Top()
{
//    throw Exception::NotImplementedException( "ArrayStack::Front is not implemented" );
    return m_vector.GetBack();
}

template <typename T>
int ArrayStack<T>::Size()
{
//    throw Exception::NotImplementedException( "ArrayStack::Size is not implemented" );
    return m_vector.Size();
}

template <typename T>
bool ArrayStack<T>::IsEmpty()
{
//    throw Exception::NotImplementedException( "ArrayStack::IsEmpty is not implemented" );
    return m_vector.IsEmpty();
}

} // End of namespace

#endif
