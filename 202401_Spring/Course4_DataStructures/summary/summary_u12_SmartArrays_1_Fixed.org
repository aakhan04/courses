# -*- mode: org -*-

1. What does the term *element* mean, in relation to an array?
2. What does the term *index* mean, in relation to an array?
3. Our Smart Fixed-Length Array has *contiguous elements* by design. What does this mean?
4. What index do the *Front* functions (PushFront, PopFront, GetFront) operate on?
5. What index do the *Back* functions (PushBack, PopBack, GetBack) operate on?
6. What error checks are needed for the *Get* functions (GetFront, GetBack, GetAt)?
7. What error checks are needed for the *Push* functions (PushFront, PushBack, PushAt)?

- Push :: Draw the "After" state for each of the following:

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
Original array

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: | "C" | "A" | "T" | "S" | "" | "" |
| Index: |   0 |   1 |   2 |   3 |  4 |  5 |
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
After =PushBack( "X" );=

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: |   |   |   |   |   |   |
| Index: | 0 | 1 | 2 | 3 | 4 | 5 |
#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
Original array

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: | "C" | "A" | "T" | "S" | "" | "" |
| Index: |   0 |   1 |   2 |   3 |  4 |  5 |
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
After =PushFront( "X" );=

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: |   |   |   |   |   |   |
| Index: | 0 | 1 | 2 | 3 | 4 | 5 |
#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
Original array

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: | "C" | "A" | "T" | "S" | "" | "" |
| Index: |   0 |   1 |   2 |   3 |  4 |  5 |
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
After =PushAt( 2, "X" );=

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: |   |   |   |   |   |   |
| Index: | 0 | 1 | 2 | 3 | 4 | 5 |
#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


- Pop :: Draw the "After" state for each of the following:

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
Original array

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: | "C" | "A" | "T" | "S" | "" | "" |
| Index: |   0 |   1 |   2 |   3 |  4 |  5 |
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
After =PopBack();=

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: |   |   |   |   |   |   |
| Index: | 0 | 1 | 2 | 3 | 4 | 5 |
#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
Original array

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: | "C" | "A" | "T" | "S" | "" | "" |
| Index: |   0 |   1 |   2 |   3 |  4 |  5 |
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
After =PopFront();=

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: |   |   |   |   |   |   |
| Index: | 0 | 1 | 2 | 3 | 4 | 5 |
#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
Original array

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: | "C" | "A" | "T" | "S" | "" | "" |
| Index: |   0 |   1 |   2 |   3 |  4 |  5 |
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
After =PopAt( 2 );=

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 :style width: 100%;
| Value: |   |   |   |   |   |   |
| Index: | 0 | 1 | 2 | 3 | 4 | 5 |
#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

