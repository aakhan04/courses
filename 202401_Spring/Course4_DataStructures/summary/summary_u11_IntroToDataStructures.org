# -*- mode: org -*-

1. What is a Data Structure?
2. Why should a Data Structure be...
   1. Generic?
   2. Reusable?
   3. Robust?
   4. Encapsulated?
3. What is Algorithm Analysis?
4. Why are we learning to write data structures from scratch?


