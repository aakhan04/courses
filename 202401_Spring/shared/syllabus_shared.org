# -*- mode: org -*-

* ADA compliance / disabilities
JCCC provides a range of services to allow persons with disabilities to participate in educational programs and activities. If you are a student with a disability and if you are in need of accommodations or services, it is your responsibility to contact Access Services and make a formal request. To schedule an appointment with an Access Advisor or for additional information, you can contact Access Services at (913) 469-3521 or accessservices@jccc.edu. Access Services is located on the 2nd floor of the Student Center (SC202)

* Attendance standards of JCCC
Educational research demonstrates that students who regularly attend and participate in all scheduled classes are more likely to succeed in college. Punctual and regular attendance at all scheduled classes, for the duration of the course, is regarded as integral to all courses and is expected of all students. Each JCCC faculty member will include attendance guidelines in the course syllabus that are applicable to that course, and students are responsible for knowing and adhering to those guidelines. Students are expected to regularly attend classes in accordance with the attendance standards implemented by JCCC faculty.

The student is responsible for all course content and assignments missed due to absence. Excessive absences and authorized absences are handled in accordance with the Student Attendance Operating Procedure.

* Academic Dishonesty
No student shall attempt, engage in, or aid and abet behavior that, in the judgment of the faculty member for a particular class, is construed as academic dishonesty. This includes, but is not limited to, cheating, plagiarism or other forms of academic dishonesty.

Examples of academic dishonesty and cheating include, but are not limited to, unauthorized acquisition of tests or other academic materials and/or distribution of these materials, unauthorized sharing of answers during an exam, use of unauthorized notes or study materials during an exam, altering an exam and resubmitting it for re-grading, having another student take an exam for you or submit assignments in your name, participating in unauthorized collaboration on coursework to be graded, providing false data for a research paper, using electronic equipment to transmit information to a third party to seek answers, or creating/citing false or fictitious references for a term paper. Submitting the same paper for multiple classes may also be considered cheating if not authorized by the faculty member.

Examples of plagiarism include, but are not limited to, any attempt to take credit for work that is not your own, such as using direct quotes from an author without using quotation marks or indentation in the paper, paraphrasing work that is not your own without giving credit to the original source of the idea, or failing to properly cite all sources in the body of your work. This includes use of complete or partial papers from internet paper mills or other sources of non-original work without attribution.

A faculty member may further define academic dishonesty, cheating or plagiarism in the course syllabus.

* Campus Health Guidelines for COVID-19
Follow College COVID-19 safety guidelines: https://www.jccc.edu/media-resources/covid-19/

- Stay home when you're sick
- Wash hands frequently
- Cover your mouth when coughing or sneezing
- Clean surfaces
- Facial coverings are available and welcomed but not required
- Wear yoru name badge or carry your JCCC photo id while on campus

*  College emergency response plan
https://www.jccc.edu/student-resources/police-safety/police-department/college-emergency-response-plan/

* Student code of conduct policy
http://www.jccc.edu/about/leadership-governance/policies/students/student-code-of-conduct/student-code-conduct.html

* Student handbook
http://www.jccc.edu/student-resources/student-handbook.html

* Campus safety
Information regarding student safety can be found at http://www.jccc.edu/student-resources/police-safety/. Classroom and campus safety are of paramount importance at Johnson County Community College and are the shared responsibility of the entire campus population. Please review the following:

- *Report emergencies:* to Campus Police (available 24 hours a day)
  - In person at the Midwest Trust Center (MTC 115)
  - Call 913-469-2500 (direct line) – Tip: program in your cell phone
  - Phone app - download JCCC Guardian (the free campus safety app: www.jccc.edu/guardian) - instant panic button and texting capability to Campus Police
  - Anonymous reports to KOPS-Watch - https://secure.ethicspoint.com/domain/en/report_company.asp?clientid=25868 or 888-258-3230
- *Be Alert:*
  - You are an extra set of eyes and ears to help maintain campus safety
  - Trust your instincts
  - Report suspicious or unusual behavior/circumstances to Campus Police (see above)
- *Be Prepared:*
  - Identify the red/white stripe Building Emergency Response posters throughout campus and online that show egress routes, shelter, and equipment
  - View A.L.I.C.E. training (armed intruder response training - Alert, Lockdown, Inform, Counter and/or Evacuate) – Student training video: https://www.youtube.com/watch?v=kMcT4-nWSq0
  - Familiarize yourself with the College Emergency Response Plan (https://www.jccc.edu/student-resources/police-safety/college-emergency-response-plan/)
- *During an emergency:* Notifications/Alerts (emergencies and inclement weather) are sent to all employees and students using email and text messaging
  - students are automatically enrolled, see JCCC Alert - Emergency Notification (https://www.jccc.edu/student-resources/police-safety/jccc-alert.html)
- *Weapons policy:* Effective July 1, 2017, concealed carry handguns are permitted in JCCC buildings subject to the restrictions set forth in the Weapons Policy. Handgun safety training is encouraged of all who choose to conceal carry. Suspected violations should be reported to JCCC Police Department 913-469-2500 or if an emergency, you can also call 911.

  
