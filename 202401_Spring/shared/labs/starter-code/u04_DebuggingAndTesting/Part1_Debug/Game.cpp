#include "Game.h"
#include "engine/ScreenDrawer.h"
#include "engine/PpmImage.h"
#include "engine/Helper.h"

#include <iostream>
#include <iomanip>
using namespace std;

Game::Game()
{
    m_day = 1;
    m_done = false;
    m_locations.push_back( "Overland Park" );
    m_locations.push_back( "Raytown" );
    m_locations.push_back( "Kansas City" );
    m_locations.push_back( "Olathe" );
}

void Game::Run()
{
    Menu_GameStart();

    while ( !m_done )
    {
        Menu_Main();
    }

    // Game over
    cout << endl << endl << "You survived the apocalpyse on your own for " << m_day << " days." << endl;
}

void Game::Menu_GameStart()
{
    PpmImage image( "../images/zombie1.ppm" );

    ScreenDrawer::Fill( '~', "black", "red" );
    ScreenDrawer::DrawWindow( "", 1, 1, 78, 13 );
    ScreenDrawer::Set( 30, 2, "Z O M B I E - A P O C A L P Y S E", "red", "black" );
    ScreenDrawer::Set( 30, 3, "It is the apocalypse, and you're a survivor.", "white", "black" );
    ScreenDrawer::Set( 30, 4, "Each round, scavenge around the town,", "white", "black" );
    ScreenDrawer::Set( 30, 5, "or relocate to a better location.", "white", "black" );
    ScreenDrawer::Set( 30, 6, "Try to avoid looters and other dangers.", "white", "black" );
    ScreenDrawer::Set( 30, 8, "Stay alive!", "white", "black" );
    image.Draw( 3, 3 );
    ScreenDrawer::Draw();

    cout << "Enter your name: ";
    getline( cin, m_player.name );
    cout << endl;
}

void Game::EnterToContinue()
{
    cout << endl << "Press ENTER to continue...";
    cin.ignore();
    string dump;
    getline( cin, dump );
}

void Game::Menu_Main()
{
    DisplayMenu();
    int choice = GetChoice( 1, 2 ); // Scavenge or Travel

    if ( choice == 1 )          // Scavenge
    {
        Menu_Scavenge();
    }
    else if ( choice == 2 )     // Travel
    {
        Menu_Travel();
    }

    EndOfDayStats();
}

void Game::Menu_Scavenge()
{
    bool actionSuccessful = true;
    int random = rand() % 5;
    RandomEvent_Scavenging( random );
}

void Game::RandomEvent_Scavenging( int event )
{
    ScreenDrawer::Fill( '~', "black", "red" );
    ScreenDrawer::DrawWindow( "", 1, 1, 78, 13 );
    ScreenDrawer::Set( 3, 2, "DAY: " + Helper::ToString( m_day ), "red", "black" );

    int mod;
    if ( event == 0 )
    {
        mod = rand() % 3 + 2;
        ScreenDrawer::Set( 3, 4, "You find a stash of food.", "green", "black" );
        ScreenDrawer::Set( 3, 5, "+ " + Helper::ToString( mod ) + " food gained", "green", "black" );
        m_player.food += mod;
        PpmImage image( "../images/food.ppm" );
        image.Draw( 50, 2 );
    }
    else if ( event == 1 )
    {
        mod = rand() % 8 + 2;
        ScreenDrawer::Set( 3, 4, "A zombie surprises you!", "red", "black" );
        ScreenDrawer::Set( 3, 5, "You get hurt in the encounter.", "red", "black" );
        ScreenDrawer::Set( 3, 6, "- " + Helper::ToString( mod ) + " health lost", "red", "black" );
        m_player.health -= mod;
        PpmImage image( "../images/zombie2.ppm" );
        image.Draw( 50, 2 );
    }
    else if ( event == 2 )
    {
        mod = rand() % 3 + 2;
        ScreenDrawer::Set( 3, 4, "You find some medical supplies.", "green", "black" );
        ScreenDrawer::Set( 3, 5, "+ " + Helper::ToString( mod ) + " health gained", "green", "black" );
        m_player.health += mod;
        PpmImage image( "../images/medical.ppm" );
        image.Draw( 50, 2 );
    }
    else if ( event == 3 )
    {
        mod = rand() % 4 + 2;
        ScreenDrawer::Set( 3, 4, "Another scavenger ambushes you!", "red", "black" );
        ScreenDrawer::Set( 3, 5, "They take some of your supplies.", "red", "black" );
        ScreenDrawer::Set( 3, 6, "- " + Helper::ToString( mod ) + " food lost", "red", "black" );
        m_player.food -= mod;
        PpmImage image( "../images/scavenger.ppm" );
        image.Draw( 50, 2 );
    }
    else if ( event == 4 )
    {
        ScreenDrawer::Set( 3, 4, "You don't find anything.", "white", "black" );
    }
}

void Game::Menu_Travel()
{
    cout << "Walk to where?" << endl;
    DisplayLocations();
    int choice = GetChoice( 1, 4 ) - 1;

    bool moved = false;

    if ( m_player.location == m_locations[choice] )
    {
        cout << endl << "You're already there!" << endl;
        EnterToContinue();
    }
    else
    {
        m_player.location = m_locations[choice];
        moved = true;
    }

    if ( moved )
    {
        cout << endl << "* You travel to " << m_locations[choice] << "." << endl;

        int random = rand() % 5;
        RandomEvent_Moving( random );
    }
}

void Game::RandomEvent_Moving( int event )
{
    ScreenDrawer::Fill( '~', "black", "red" );
    ScreenDrawer::DrawWindow( "", 1, 1, 78, 13 );

    int mod;
    if ( event == 0 )
    {
        ScreenDrawer::Set( 3, 4, "A zombie surprises you!", "red", "black" );
        ScreenDrawer::Set( 3, 5, "You successfully fight it off.", "green", "black" );
        PpmImage image( "../images/zombie2.ppm" );
        image.Draw( 50, 2 );
    }
    else if ( event == 1 )
    {
        mod = rand() % 8 + 2;

        ScreenDrawer::Set( 3, 4, "A zombie surprises you!", "red", "black" );
        ScreenDrawer::Set( 3, 5, "It bites you as you fight it!", "red", "black" );
        ScreenDrawer::Set( 3, 6, "- " + Helper::ToString( mod ) + " health lost", "red", "black" );
        m_player.health -= mod;
        PpmImage image( "../images/zombie2.ppm" );
        image.Draw( 50, 2 );
        m_player.health -= mod;
    }
    else if ( event == 2 )
    {
        mod = rand() % 3 + 2;
        ScreenDrawer::Set( 3, 4, "You find another scavenger and trade goods.", "green", "black" );
        ScreenDrawer::Set( 3, 5, "+ " + Helper::ToString( mod ) + " food gained", "green", "black" );
        m_player.food -= mod;
        PpmImage image( "../images/scavenger.ppm" );
        image.Draw( 50, 2 );
        m_player.food += mod;
    }
    else if ( event == 3 )
    {
        mod = rand() % 4 + 2;
        ScreenDrawer::Set( 3, 4, "You find another scavenger and trade goods.", "red", "black" );
        ScreenDrawer::Set( 3, 6, "+ " + Helper::ToString( mod ) + " health gained", "red", "black" );
        m_player.food -= mod;
        PpmImage image( "../images/safehouse.ppm" );
        image.Draw( 50, 2 );
        m_player.health += mod;
    }
    else if ( event == 4 )
    {
        ScreenDrawer::Set( 3, 4, "The journey is uneventful.", "white", "black" );
    }
}

void Game::EndOfDayStats()
{
    int mod;
    ScreenDrawer::Set( 3, 8, "The day passes. (+1 day)", "white", "black" );
    m_day++;

    if ( m_player.food > 0 )
    {
        ScreenDrawer::Set( 3, 9, "You eat a meal (-1 food, +1 health)", "yellow", "black" );
        m_player.food--;
        m_player.health++;
    }
    else
    {
        mod = rand() % 4 + 1;
        ScreenDrawer::Set( 3, 9, "You are starving! (-" + Helper::ToString( mod ) + " health)", "red", "black" );
        m_player.health -= mod;
    }

    // Check for special states
    if ( m_player.health > 20 )
    {
        m_player.health = 20;
    }

    if ( m_player.food <= 0 )
    {
        m_player.food = 0;
    }

    if ( m_player.health <= 0 )
    {
        ScreenDrawer::Set( 3, 10, "You have died.", "red", "black" );
        m_done = true;
    }
    else if ( m_day >= 20 )
    {
        ScreenDrawer::Set( 3, 11, "In the morning, a group of scavengers find you.", "green", "black" );
        ScreenDrawer::Set( 3, 12, "They have a fortification nearby and are rebuilding a society.", "green", "black" );
        ScreenDrawer::Set( 3, 13, "You agree to live in their town.", "green", "black" );
        m_done = true;
    }

    ScreenDrawer::Draw();
    EnterToContinue();
}

void Game::DisplayLocations()
{
    for ( size_t i = 0; i < m_locations.size(); i++ )
    {
        cout << (i+1) << ". " << m_locations[i] << endl;
    }
}

int Game::GetChoice( int min, int max )
{
    cout << "CHOICE (" << min << " - " << max << "): ";
    int choice;
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "INVALID SELECTION, TRY AGAIN: ";
        cin >> choice;
    }

    return choice;
}

void Game::DisplayMenu()
{
    ScreenDrawer::Fill( '~', "black", "red" );
    ScreenDrawer::DrawWindow( "", 1, 1, 78, 13 );
    ScreenDrawer::Set( 3, 2, "DAY: " + Helper::ToString( m_day ), "red", "black" );
    ScreenDrawer::Set( 3, 4, "Food: " + Helper::ToString( m_player.food ) );
    ScreenDrawer::Set( 3, 5, "Health: " + Helper::ToString( m_player.health ) + "/" + Helper::ToString( m_player.maxHealth ) );
    ScreenDrawer::Set( 3, 6, "Location: " + m_player.location );
    ScreenDrawer::Set( 3, 10, "[1] Scavenge here" );
    ScreenDrawer::Set( 3, 11, "[2] Travel elseware" );

    if ( m_player.location == "Overland Park" )
    {
      PpmImage image( "../images/overlandpark.ppm" );
      image.Draw( 50, 2 );
    }
    else if ( m_player.location == "Raytown" )
    {
      PpmImage image( "../images/raytown.ppm" );
      image.Draw( 50, 2 );
    }
    else if ( m_player.location == "Kansas City" )
    {
      PpmImage image( "../images/kansascity.ppm" );
      image.Draw( 50, 2 );
    }
    else if ( m_player.location == "Olathe" )
    {
      PpmImage image( "../images/olathe.ppm" );
      image.Draw( 50, 2 );
    }

    ScreenDrawer::Draw();
}
