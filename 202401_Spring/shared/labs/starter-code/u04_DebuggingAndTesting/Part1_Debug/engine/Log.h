#ifndef _LOG
#define _LOG

#include <fstream>
#include <string>

class Log
{
  public:
  static void Setup();
  static void Teardown();

  static void Out( std::string text, std::string where = "" );
  static void Err( std::string text, std::string where = "" );

  private:
  static std::ofstream s_outfile;
};

#endif
