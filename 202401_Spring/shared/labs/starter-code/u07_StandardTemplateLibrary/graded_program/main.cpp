#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <fstream>
using namespace std;

#include "GroceryStoreProgram.h"

void Program();
void Tester();

int main()
{
  srand( time( NULL ) );
  std::cout << "1. Run AUTOMATED TESTS" << std::endl;
  std::cout << "2. Run PROGRAMS" << std::endl;
  int choice;
  std::cout << ">> ";
  std::cin >> choice;

  switch( choice )
  {
    case 1:
    Tester();
    break;

    case 2:
    Program();
    break;
  }

  return 0;
}

void Program()
{
  GroceryStoreProgram program;

  ifstream input( "../events.txt" );
  string action, product, customer;
  float price;

  while ( input >> action )
  {
    if ( action == "STOCK" )      // STOCK food price
    {
      input >> product >> price;
      program.Stock( product, price );
    }
    else if ( action == "ENTER" ) // ENTER name
    {
      input >> customer;
      program.CustomerEnterStore( customer );
    }
    else if ( action == "CART" )  // CART name food
    {
      input >> customer >> product;
      program.CustomerCartAdd( customer, product );
    }
    else if ( action == "OOPS" ) // OOPS name
    {
      program.CustomerOops( customer );
    }
    else if ( action == "LINEUP" ) // LINEUP name
    {
      input >> customer;
      program.CustomerLineup( customer );
    }
    else if ( action == "PROCESS" )
    {
      program.Process();
    }
  }
}

void Tester()
{
    Tester_GroceryStoreProgram();
}
