#ifndef _GROCERY_STORE_PROGRAM_H
#define _GROCERY_STORE_PROGRAM_H

#include <vector>
#include <map>
#include <stack>
#include <queue>
#include <string>
#include <fstream>
using namespace std;

#include "Customer.h"

class GroceryStoreProgram
{
public:
    GroceryStoreProgram();

    void Stock( string product, float price );
    void CustomerEnterStore( string customer );
    void CustomerCartAdd( string customer, string product );
    void CustomerOops( string customer );
    void CustomerLineup( string customer );
    void Process();
    void PrintTimestamp();

private:
    queue<Customer> checkout_queue;
    map<string, float> product_prices;
    map<string, Customer> customers_in_store;
    int minutes, hours;

    friend void Tester_GroceryStoreProgram();
};

void Tester_GroceryStoreProgram();

#endif
