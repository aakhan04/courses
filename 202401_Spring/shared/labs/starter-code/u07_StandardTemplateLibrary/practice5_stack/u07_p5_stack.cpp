#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
  cout << "STACK PROGRAM" << endl;
  
  // TODO: Declare a stack
  
  bool done = false;
  while ( !done )
  {
    cout << string( 50, '-' ) << endl;
    // TODO:  If stack is empty, display "STACK IS EMPTY".
    //        Otherwise display the top of the stack
  
    cout << string( 50, '-' ) << endl;
    cout << "0. Quit" << endl;
    cout << "1. PUSH item" << endl;
    cout << "2. POP item" << endl;
    cout << ">> ";
    
    int choice;
    cin >> choice;
    
    switch( choice )
    {
      case 0:
      done = true;
      break;
      
      case 1:
      {
        string text;
        cout << "Enter new text to push on stack: ";
        cin >> text;
        // TODO: Push the text item onto the stack
        
      }
      break;
      
      case 2:
        cout << "Popped top item off stack" << endl;
        // TODO: Pop the top of the stack
        
      break;
    }
  }
  
  cout << endl << string( 50, '-' ) << endl;
  
  // TODO: While stack is not empty...:
    // TODO: Display the item on the top of the stack
    // TODO: Pop the top of the stack
  
  cout << endl << "THE END" << endl;
  return 0;
}
