#ifndef _COLORS
#define _COLORS

const std::string TEXT_DEFAULT  = "\033[0m";
const std::string TEXT_RED      = "\033[0;31m";
const std::string TEXT_GREEN    = "\033[0;32m";
const std::string TEXT_YELLOW   = "\033[0;33m";
const std::string TEXT_BLUE     = "\033[0;34m";
const std::string TEXT_PINK     = "\033[0;35m";
const std::string TEXT_CYAN     = "\033[0;36m";
const std::string TEXT_WHITE    = "\033[0;37m";

#endif
