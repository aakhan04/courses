// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`

namespace U03
{

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------
// TODO: Add DisplayMenu here
void DisplayMenu()
{
}

// TODO: Add FormatUSD here
void FormatUSD( float price )
{
}

// TODO: Add GetTaxPercent here
float GetTaxPercent()
{
  return -1; // TODO: Remove me!
}

// TODO: Add GetPricePlusTax here
float GetPricePlusTax( float original_price, float tax_percent )
{
  return -1; // TODO: Remove me!
}

// TODO: Add GetNewPrice function. Inputs: None, Output: float
// Within the function ask the user to enter the price of a new item, use `cin` to get their input.
// Store it in a float variable. `return` this float at the end of the function.
float GetNewPrice()
{
  return -1; // TODO: Remove me!
}

// TODO: Add GetChoice function. Inputs: min and max (both ints), Output: int
// Within this function ask the user to enter a number between the `min` and `max`.
// Store their choice in an integer variable, then use a while loop - while their input
// is invalid, ask them to re-enter their selection.
// Make sure to use `return` to return their selection at the end.
int GetChoice( int min, int max )
{
  return -1; // TODO: Remove me!
}

float SaleProgram()
{
  // TODO: Implement me!
  return -1; // TODO: Remove me!
}

// - PROGRAM STARTER --------------------------------------------------------//
void U03_Program1_Program()
{
  float result = SaleProgram();
  std::cout << "RESULT: " << result << std::endl;
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// - AUTOMATED TESTER -------------------------------------------------------//
#include "../../INFO.h"
void U03_Program1_Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U03-P1-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  std::cout << BOLD;
  std::cout << std::endl;
  std::cout << "MANUAL TEST, check functionality:" << std::endl;
  std::cout << "* DisplayMenu() - main menu shown in loop" << std::endl;
  std::cout << "* FormatUSD( p ) - make sure dollar amounts are formatted" << std::endl;
  std::cout << "* GetTaxPercent() - Make sure on checkout the tax % shows up" << std::endl;
  std::cout << "* GetPricePlusTax( p, t ) - Make sure After Tax price is correct" << std::endl;
  std::cout << "* GetChoice( min, max ) - Make sure it disallows numbers less than min / greater than max" << std::endl;
  std::cout << "* GetNewPrice() - Make sure function is called to get float values" << std::endl;
  std::cout << std::string( 80, '-' ) << std::endl << std::endl;
  std::cout << CLR;

  const int TOTAL_TESTS = 2;
  std::string  in1[TOTAL_TESTS]; // inputs 1
  float   exo[TOTAL_TESTS]; // expected output
  float   aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = " 2.25, 4.00, 5.30";
  exo[0] = 12.66;

  // Setup test 2
  in1[1] = "1.10, 3.00";
  exo[1] = 4.49;

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    std::cout << BOLD << std::endl;
    std::cout << "Enter prices:    " << in1[i] << std::endl;
    std::cout << "Expected output: " << exo[i] << std::endl;

    std::cout << CLR;
    aco[i] = SaleProgram();

    if ( aco[i] >= exo[i] - 0.1 && aco[i] <= exo[i] + 0.1 )
    {
      // PASS
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << ", StudentCode() = " << aco[i] << std::endl;
    }
    else
    {
      // FAIL
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << ", StudentCode()" << std::endl;
      std::cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << std::endl;
      std::cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << std::endl;
    }
  }
  std::cout << CLR;

//  float result = SaleProgram();

  std::cout << CLR;
}

}

