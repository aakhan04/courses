#include "Functions.h"
#include <iostream>
#include <memory>
#include <string>
using namespace std;

void DisplayContents( shared_ptr<FilesystemNode> current_node, int tab )
{
  // Every item: Display the name of the current file/folder we're on
  cout << string( tab, ' ' ) << current_node->name << endl;

  // If this is a folder, it may contain files and folders.
  if ( current_node->node_type == NodeType::FOLDER )
  {
    // Iterate through all the children.
    for ( auto& child : current_node->contents )
    {
      // Recurse: Do the same thing for each child file/folder.
      DisplayContents( child, tab+1 );
    }
  }
}

/**
@param    current_node      Pointer to the current file/folder node we're looking at
@param    find_me           Partial name of file/folder we're looking for

Returns the path to the file/folder we're searching for, uses recursion.

1. If the current node's name has a partial match with the `find_me` parameter, then
    RETURN this current node's name as the result.
    PARTIAL STRING MATCH:
    `if ( current_node->name.find( find_me ) != string::npos )` << This is true if the name contains `find_me` anywhere within.

2. Afterwards, use a loop to iterate over all of the current node's contents. Within the loop:
    RECURSE to the FindFile function, passing in this CHILD node and the `find_me` data. STORE the result in a string variable.
    IF the result is NOT an empty string "", then we've found the file: Return current_node->name + result

3. After the for loop, this means nothing was found in this recursive branch.
    In this case, just return an empty string "".
*/
string FindFile( shared_ptr<FilesystemNode> current_node, string find_me )
{
  // TODO: Implement me!
  return "";
}


//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// - PROGRAM STARTER --------------------------------------------------------//
void Program()
{
  cin.ignore();
  auto ptr_root = SetupFilesystem();

  DisplayContents( ptr_root, 0 );

  while ( true )
  {
    string find_name;
    cout << endl << "Enter name of FILE or FOLDER to find, or QUIT to stop: ";
    getline( cin, find_name );

    if ( find_name == "QUIT" )
    {
      return;
    }

    string path = FindFile( ptr_root, find_name );
    cout << "Path: " << path << endl;
  }
}

// - DATA SETUP --------------------------------------------------------//
shared_ptr<FilesystemNode> SetupFilesystem()
{
  auto ptr_root     = shared_ptr<FilesystemNode>( new FilesystemNode( "/", NodeType::FOLDER ) );
  auto ptr_home     = shared_ptr<FilesystemNode>( new FilesystemNode( "home/", NodeType::FOLDER ) );
  auto ptr_school   = shared_ptr<FilesystemNode>( new FilesystemNode( "school/", NodeType::FOLDER ) );
  auto ptr_english  = shared_ptr<FilesystemNode>( new FilesystemNode( "english/", NodeType::FOLDER ) );
  auto ptr_compsci  = shared_ptr<FilesystemNode>( new FilesystemNode( "compsci/", NodeType::FOLDER ) );
  auto ptr_projects = shared_ptr<FilesystemNode>( new FilesystemNode( "projects/", NodeType::FOLDER ) );
  auto ptr_game     = shared_ptr<FilesystemNode>( new FilesystemNode( "my_game/", NodeType::FOLDER ) );

  auto ptr_file1 = shared_ptr<FilesystemNode>( new FilesystemNode( "essay1.txt", NodeType::FILE ) );
  auto ptr_file2 = shared_ptr<FilesystemNode>( new FilesystemNode( "essay2.txt", NodeType::FILE ) );
  auto ptr_file3 = shared_ptr<FilesystemNode>( new FilesystemNode( "game.cpp", NodeType::FILE ) );
  auto ptr_file4 = shared_ptr<FilesystemNode>( new FilesystemNode( "homeworkA.cpp", NodeType::FILE ) );
  auto ptr_file5 = shared_ptr<FilesystemNode>( new FilesystemNode( "homeworkB.cpp", NodeType::FILE ) );

  ptr_root->contents.push_back( ptr_home );
  ptr_home->contents.push_back( ptr_school );
  ptr_home->contents.push_back( ptr_projects );
  ptr_school->contents.push_back( ptr_english );
  ptr_school->contents.push_back( ptr_compsci );
  ptr_english->contents.push_back( ptr_file1 );
  ptr_english->contents.push_back( ptr_file2 );
  ptr_compsci->contents.push_back( ptr_file4 );
  ptr_compsci->contents.push_back( ptr_file5 );
  ptr_projects->contents.push_back( ptr_game );
  ptr_game->contents.push_back( ptr_file3 );

  return ptr_root;
}

// - AUTOMATED TESTER -------------------------------------------------------//
#include "INFO.h"
void Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U06-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  std::string test_name;
  std::string input;
  std::string expect_out;
  std::string actual_out;
  int test_count = 1;

  // Test 1
  {
    auto ptr_root     = std::shared_ptr<FilesystemNode>( new FilesystemNode( "ROOTFOLDER/", NodeType::FOLDER ) );
    test_name  = "Check that match is found for FIRST call to function";
    input = "FOL";
    expect_out = "ROOTFOLDER/";
    actual_out = FindFile( ptr_root, input );

    if ( actual_out == expect_out )
    {
      std::cout << GRN << "[PASS] ";
      std::cout << "  FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
    }
    else
    {
      std::cout << RED << "[FAIL] " << test_name << " test " << test_count << ": " << std::endl;
      std::cout << "  EXPECTED FindFile( ptr_root, \"" << input << "\" ) = \"" << expect_out << "\"" << std::endl;
      std::cout << "  ACTUAL   FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
      std::cout << std::endl;
      // HINT:
      std::cout << "  Are you doing a partial string match?" << std::endl;
      std::cout << "  if ( current_node->name.find( find_me ) != string::npos )" << std::endl;
      std::cout << "    return current_node->name;" << std::endl;
    }
  }
  test_count++;

  // Test 2
  {
    auto ptr_root     = std::shared_ptr<FilesystemNode>( new FilesystemNode( "ROOTFOLDER/", NodeType::FOLDER ) );
    auto ptr_child1   = shared_ptr<FilesystemNode>( new FilesystemNode( "CHILDFOLDER/", NodeType::FOLDER ) );
    auto ptr_child2   = shared_ptr<FilesystemNode>( new FilesystemNode( "CHILDFILE.TXT", NodeType::FILE ) );
    ptr_root->contents.push_back( ptr_child1 );
    ptr_root->contents.push_back( ptr_child2 );

    test_name  = "Check that recursion to child FOLDER finds match";
    input = "CHILDFOLD";
    expect_out = "ROOTFOLDER/CHILDFOLDER/";
    actual_out = FindFile( ptr_root, input );

    if ( actual_out == expect_out )
    {
      std::cout << GRN << "[PASS] ";
      std::cout << "  FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
    }
    else
    {
      std::cout << RED << "[FAIL] " << test_name << " test " << test_count << ": " << std::endl;
      std::cout << "  EXPECTED FindFile( ptr_root, \"" << input << "\" ) = \"" << expect_out << "\"" << std::endl;
      std::cout << "  ACTUAL   FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
      std::cout << std::endl;
      // HINT:
      std::cout << "  FILESYSTEM for this test:" << std::endl;
      DisplayContents( ptr_root, 3 );
    }
  }
  test_count++;

  // Test 3
  {
    auto ptr_root     = std::shared_ptr<FilesystemNode>( new FilesystemNode( "ROOTFOLDER/", NodeType::FOLDER ) );
    auto ptr_child1   = shared_ptr<FilesystemNode>( new FilesystemNode( "CHILDFOLDER/", NodeType::FOLDER ) );
    auto ptr_child2   = shared_ptr<FilesystemNode>( new FilesystemNode( "CHILDFILE.TXT", NodeType::FILE ) );
    ptr_root->contents.push_back( ptr_child1 );
    ptr_root->contents.push_back( ptr_child2 );

    test_name  = "Check that recursion to child FILE finds match";
    input = "CHILDFIL";
    expect_out = "ROOTFOLDER/CHILDFILE.TXT";
    actual_out = FindFile( ptr_root, input );

    if ( actual_out == expect_out )
    {
      std::cout << GRN << "[PASS] ";
      std::cout << "  FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
    }
    else
    {
      std::cout << RED << "[FAIL] " << test_name << " test " << test_count << ": " << std::endl;
      std::cout << "  EXPECTED FindFile( ptr_root, \"" << input << "\" ) = \"" << expect_out << "\"" << std::endl;
      std::cout << "  ACTUAL   FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
      std::cout << std::endl;
      // HINT:
      std::cout << "  FILESYSTEM for this test:" << std::endl;
      DisplayContents( ptr_root, 3 );
    }
  }
  test_count++;

  // Test 3
  {
    auto ptr_root      = std::shared_ptr<FilesystemNode>( new FilesystemNode( "ROOTFOLDER/", NodeType::FOLDER ) );
    auto ptr_folder1   = shared_ptr<FilesystemNode>( new FilesystemNode( "FOLDER1/", NodeType::FOLDER ) );
    auto ptr_folder2   = shared_ptr<FilesystemNode>( new FilesystemNode( "FOLDER2/", NodeType::FOLDER ) );

    auto ptr_file1     = shared_ptr<FilesystemNode>( new FilesystemNode( "FILEA.TXT", NodeType::FILE ) );
    auto ptr_file2     = shared_ptr<FilesystemNode>( new FilesystemNode( "FILEB.TXT", NodeType::FILE ) );

    ptr_root->contents.push_back( ptr_folder1 );
    ptr_root->contents.push_back( ptr_folder2 );
    ptr_folder1->contents.push_back( ptr_file1 );
    ptr_folder2->contents.push_back( ptr_file2 );

    test_name  = "Check for multiple levels of recursion";
    input = "FILEB";
    expect_out = "ROOTFOLDER/FOLDER2/FILEB.TXT";
    actual_out = FindFile( ptr_root, input );

    if ( actual_out == expect_out )
    {
      std::cout << GRN << "[PASS] ";
      std::cout << "  FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
    }
    else
    {
      std::cout << RED << "[FAIL] " << test_name << " test " << test_count << ": " << std::endl;
      std::cout << "  EXPECTED FindFile( ptr_root, \"" << input << "\" ) = \"" << expect_out << "\"" << std::endl;
      std::cout << "  ACTUAL   FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
      std::cout << std::endl;
      // HINT:
      std::cout << "  FILESYSTEM for this test:" << std::endl;
      DisplayContents( ptr_root, 3 );
    }
  }
  test_count++;

  // Test 4
  {
    auto ptr_root      = std::shared_ptr<FilesystemNode>( new FilesystemNode( "ROOTFOLDER/", NodeType::FOLDER ) );
    auto ptr_folder1   = shared_ptr<FilesystemNode>( new FilesystemNode( "FOLDER1/", NodeType::FOLDER ) );
    auto ptr_folder2   = shared_ptr<FilesystemNode>( new FilesystemNode( "FOLDER2/", NodeType::FOLDER ) );

    auto ptr_file1     = shared_ptr<FilesystemNode>( new FilesystemNode( "FILEA.TXT", NodeType::FILE ) );
    auto ptr_file2     = shared_ptr<FilesystemNode>( new FilesystemNode( "FILEB.TXT", NodeType::FILE ) );

    ptr_root->contents.push_back( ptr_folder1 );
    ptr_root->contents.push_back( ptr_folder2 );
    ptr_folder1->contents.push_back( ptr_file1 );
    ptr_folder2->contents.push_back( ptr_file2 );

    test_name  = "Check that \"\" is returned if item not found.";
    input = "ZZZZ";
    expect_out = "";
    actual_out = FindFile( ptr_root, input );

    if ( actual_out == expect_out )
    {
      std::cout << GRN << "[PASS] ";
      std::cout << "  FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
    }
    else
    {
      std::cout << RED << "[FAIL] " << test_name << " test " << test_count << ": " << std::endl;
      std::cout << "  EXPECTED FindFile( ptr_root, \"" << input << "\" ) = \"" << expect_out << "\"" << std::endl;
      std::cout << "  ACTUAL   FindFile( ptr_root, \"" << input << "\" ) = \"" << actual_out << "\"" << std::endl;
      std::cout << std::endl;
      // HINT:
      std::cout << "  FILESYSTEM for this test:" << std::endl;
      DisplayContents( ptr_root, 3 );
    }
  }
  test_count++;
}




