#include "../Functions.h"

// https://en.wikipedia.org/wiki/Heapsort

int LeftChildIndex( int index )
{
  return 2 * index + 1;
}

int RightChildIndex( int index )
{
  return 2 * index + 2;
}

int ParentIndex( int index )
{
  return int( ( index - 1 ) / 2 );
}

void SiftDown( vector<int>& arr, int root, int end )
{
  // TODO: Implement me
}

void Heapify( vector<int>& arr, size_t size )
{
  // TODO: Implement me
}

void HeapSort( vector<int>& arr )
{
  // TODO: Implement me
}
