#include "Functions.h"

#include <iostream>
#include <chrono>
#include <string>
#include <vector>
#include <cstdlib>
using namespace std;


//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// - HELPER FUNCTION(S) -----------------------------------------------------//
string VectorToString( const vector<int>& arr )
{
  string built_string = "{";
  for ( size_t i = 0; i < arr.size(); i++ )
  {
    if ( i != 0 ) { built_string += ", "; }
    built_string += to_string( arr[i] );
  }
  built_string += "}";
  return built_string;
}

void GenerateData( vector<int>& data, size_t amount_to_generate )
{
  for ( size_t i = 0; i < amount_to_generate; i++ )
  {
    data.push_back( rand() % 1000000 );
  }
}

void TimerStart( chrono::system_clock::time_point& start_time )
{
  start_time = chrono::system_clock::now();
}

size_t TimerElapsedMS( const chrono::system_clock::time_point& start_time )
{
  auto current_time = std::chrono::system_clock::now();
  return chrono::duration_cast<std::chrono::milliseconds>( current_time - start_time ).count();
}

// - PROGRAM STARTER --------------------------------------------------------//
void Program()
{
  chrono::system_clock::time_point start_time;
  int amount_of_data = 100000;

  vector<int> data;
  cout << "Generating " << amount_of_data << " pieces of data... ";
  TimerStart( start_time );
  GenerateData( data, amount_of_data );
  cout << TimerElapsedMS( start_time ) << " milliseconds" << endl;

  vector<int> copy1 = data;
  vector<int> copy2 = data;
  vector<int> copy3 = data;
  vector<int> copy4 = data;
  vector<int> copy5 = data;

  cout << endl << "Running Merge sort, please wait... " << endl;
  TimerStart( start_time );
  MergeSort( copy1 );
  cout << "Completed in " << TimerElapsedMS( start_time ) << " milliseconds" << endl;

  cout << endl <<  "Running Quick sort, please wait..." << endl;
  TimerStart( start_time );
  QuickSort( copy2 );
  cout << "Completed in " << TimerElapsedMS( start_time ) << " milliseconds" << endl;

  cout << endl <<  "Running Radix sort, please wait..." << endl;
  TimerStart( start_time );
  RadixSort( copy3 );
  cout << "Completed in " << TimerElapsedMS( start_time ) << " milliseconds" << endl;

  cout << endl <<  "Running Heap sort, please wait..." << endl;
  TimerStart( start_time );
  HeapSort( copy4 );
  cout << "Completed in " << TimerElapsedMS( start_time ) << " milliseconds" << endl;

  cout << endl <<  "Running Selection sort, please wait..." << endl;
  TimerStart( start_time );
  SelectionSort( copy5 );
  cout << "Completed in " << TimerElapsedMS( start_time ) << " milliseconds" << endl;

  cout << endl <<  "Running Linear search, please wait..." << endl;
  TimerStart( start_time );
  LinearSearch( copy1, -10000 );
  cout << "Completed in " << TimerElapsedMS( start_time ) << " milliseconds" << endl;

  cout << endl <<  "Running Binary search, please wait..." << endl;
  TimerStart( start_time );
  BinarySearch( copy1, -10000 );
  cout << "Completed in " << TimerElapsedMS( start_time ) << " milliseconds" << endl;

  cout << endl << endl;
}

// - AUTOMATED TESTER -------------------------------------------------------//
#include "INFO.h"
void Tester()
{
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U07-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  SortTests();
  SearchTests();
}


void SortTests()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << "SORT tests" << std::endl;

  const int TOTAL_TESTS = 2;
  std::vector<int>  in1[TOTAL_TESTS] = { { 7, 3, 3, 1 }, { 8, 6, 7, 5, 3, 0, 9 } };
  std::vector<int>  exo[TOTAL_TESTS] = { { 1, 3, 3, 7 }, { 0, 3, 5, 6, 7, 8, 9 } };
  std::vector<int>  aco[TOTAL_TESTS];

  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    std::vector<int> copy_selection = in1[i];
    std::vector<int> copy_merge     = in1[i];
    std::vector<int> copy_quick     = in1[i];
    std::vector<int> copy_radix     = in1[i];
    std::vector<int> copy_heap      = in1[i];

    SelectionSort( copy_selection );

    if      ( copy_selection != exo[i] )
    {
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << "a" << std::endl;
      std::cout << "EXPECTED  SelectionSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( exo[i] ) << std::endl;
      std::cout << "ACTUAL    SelectionSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_selection ) << std::endl;
    }
    else
    {
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << "a" << ", SelectionSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_selection ) << "" << std::endl;
    }

    MergeSort( copy_merge );
    if ( copy_merge != exo[i] )
    {
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << "b" << std::endl;
      std::cout << "EXPECTED  MergeSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( exo[i] ) << std::endl;
      std::cout << "ACTUAL    MergeSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_merge ) << std::endl;
    }
    else
    {
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << "b" << ", MergeSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_merge ) << "" << std::endl;
    }

    QuickSort( copy_quick );
    if ( copy_quick != exo[i] )
    {
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << "c" << std::endl;
      std::cout << "EXPECTED  QuickSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( exo[i] ) << std::endl;
      std::cout << "ACTUAL    QuickSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_quick ) << std::endl;
    }
    else
    {
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << "c" << ", QuickSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_quick ) << "" << std::endl;
    }

    HeapSort( copy_heap );
    if ( copy_heap != exo[i] )
    {
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << "f" << std::endl;
      std::cout << "EXPECTED  HeapSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( exo[i] ) << std::endl;
      std::cout << "ACTUAL    HeapSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_heap ) << std::endl;
    }
    else
    {
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << "f" << ", HeapSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_heap ) << "" << std::endl;
    }

    RadixSort( copy_radix );
    if ( copy_radix != exo[i] )
    {
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << "d" << std::endl;
      std::cout << "EXPECTED  RadixSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( exo[i] ) << std::endl;
      std::cout << "ACTUAL    RadixSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_radix ) << std::endl;
    }
    else
    {
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << "d" << ", RadixSort(" << VectorToString( in1[i] ) << ") = " << VectorToString( copy_radix ) << "" << std::endl;
    }

    std::cout << endl;
  }

  std::cout << CLR;
}

void SearchTests()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << "SEARCH tests" << std::endl;

  const int TOTAL_TESTS = 3;
  std::vector<int>  in1[TOTAL_TESTS] = { { 1, 3, 3, 7 }, { 0, 3, 5, 6, 7, 8, 9 }, { 1, 2, 3 } };
  int               in2[TOTAL_TESTS] = { 3, 6, 4 };
  int               exo[TOTAL_TESTS] = { 1, 3, -1 };
  int               aco[TOTAL_TESTS];

  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = LinearSearch( in1[i], in2[i] );
    if ( aco[i] != exo[i] )
    {
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << "a" << std::endl;
      std::cout << "EXPECTED  LinearSearch(" << VectorToString( in1[i] ) << ", " << in2[i] << ") = " << exo[i] << std::endl;
      std::cout << "ACTUAL    LinearSearch(" << VectorToString( in1[i] ) << ", " << in2[i] << ") = " << aco[i] << std::endl;
    }
    else
    {
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << "a" << ", LinearSearch(" << VectorToString( in1[i] ) << ", " << in2[i] << ") = " << aco[i] << std::endl;
    }

    aco[i] = BinarySearch( in1[i], in2[i] );
    if ( aco[i] != exo[i] )
    {
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << "b" << std::endl;
      std::cout << "EXPECTED  BinarySearch(" << VectorToString( in1[i] ) << ", " << in2[i] << ") = " << exo[i] << std::endl;
      std::cout << "ACTUAL    BinarySearch(" << VectorToString( in1[i] ) << ", " << in2[i] << ") = " << aco[i] << std::endl;
    }
    else
    {
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << "b" << ", BinarySearch(" << VectorToString( in1[i] ) << ", " << in2[i] << ") = " << aco[i] << std::endl;
    }
  }

  std::cout << CLR;
}
