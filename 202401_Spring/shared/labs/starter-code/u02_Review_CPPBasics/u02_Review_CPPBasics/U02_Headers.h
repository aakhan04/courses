#ifndef _U02_HEADERS
#define _U02_HEADERS

#include <string>
#include <vector>

int U02_Program1_Function( int guest_count, int pizza_slices );
void U02_Program1_Program();
void U02_Program1_Tester();

char U02_Program2_Function( float points_possible, float my_score );
void U02_Program2_Program();
void U02_Program2_Tester();

float U02_Program3_Function( float starting_salary, float raise_per_year, int years );
void U02_Program3_Program();
void U02_Program3_Tester();

float U02_Program4_Function( std::vector<char> course_grades );
void U02_Program4_Program();
void U02_Program4_Tester();

float U02_Program5_Function( float starting_salary, float raise_per_year, int years );
void U02_Program5_Program();
void U02_Program5_Tester();

std::string U02_Program6_Function( std::string filename, int line_number );
void U02_Program6_Program();
void U02_Program6_Tester();

void U02_Tester();

void U02_Program();

#endif
