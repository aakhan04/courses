// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`
#include <fstream>    // Library that contains `ifstream` and `ofstream`


std::string U02_Program6_Function( std::string filename, int line_number )
{
  // TODO: Create ifstream object named `input`, try to open the `filename`.
  

  // TODO: Check for an input fail. Display error message and return "" on failure.
  

  // TODO: Load in lines from the `input` file until you hit the
  // "n-th" line (given by `line_number`). Return the line loaded in here.
  

  // TODO: Return "" if nothing was returned during the file read.
  return "";
}



//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// - PROGRAM STARTER --------------------------------------------------------//
void U02_Program6_Program()
{
  std::ofstream out;
  out.open( "test2.txt" );
  out << "we can dance if we want to" << std::endl << "we can leave your friends behind" << std::endl << "cuz your friends don't dance" << std::endl << "and if they don't dance" << std::endl << "well then they're no friends of mine." << std::endl;
  out.close();

  std::cout << "Get which line # from test2.txt? ";
  int line;
  std::cin >> line;

  std::string text = U02_Program6_Function( "test2.txt", line );
  std::cout << "Text at that line: " << text << std::endl;
}

// - AUTOMATED TESTER -------------------------------------------------------//
#include "../../INFO.h"
void U02_Program6_Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U02-P6-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  std::ofstream out;

  out.open( "test1.txt" );
  out << "the quick brown fox" << std::endl << "jumps over the lazy dog" << std::endl;
  out.close();

  out.open( "test2.txt" );
  out << "we can dance if we want to" << std::endl << "we can leave your friends behind" << std::endl << "cuz your friends don't dance" << std::endl << "and if they don't dance" << std::endl << "well then they're no friends of mine." << std::endl;
  out.close();

  // (Automated test):

  const int TOTAL_TESTS = 3;
  std::string in1[TOTAL_TESTS]; // inputs 1
  int    in2[TOTAL_TESTS]; // inputs 2
  std::string exo[TOTAL_TESTS]; // expected output
  std::string aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = "test1.txt";
  in2[0] = 1;
  exo[0] = "jumps over the lazy dog";

  // Setup test 2
  in1[1] = "test2.txt";
  in2[1] = 2;
  exo[1] = "cuz your friends don't dance";

  // Setup test 3
  in1[2] = "test2.txt";
  in2[2] = 10;
  exo[2] = "";

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    std::cout << CLR;
    aco[i] = U02_Program6_Function( in1[i], in2[i] );

    if ( aco[i] == exo[i] )
    {
      // PASS
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ") = " << aco[i] << std::endl;
    }
    else
    {
      // FAIL
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ")" << std::endl;
      std::cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << std::endl;
      std::cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << std::endl;
    }
  }
  std::cout << CLR;
}
