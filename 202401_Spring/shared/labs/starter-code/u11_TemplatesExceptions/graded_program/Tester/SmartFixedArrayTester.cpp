#include "SmartFixedArrayTester.h"
#include "../DataStructure/SmartFixedArray.h"

// C++ Library includes
#include <iostream>
#include <iomanip>
#include <string>

SmartFixedArrayTester::SmartFixedArrayTester()
  : RED( "\033[0;31m" ), GRN( "\033[0;32m" ), BOLD( "\033[0;35m" ), CLR( "\033[0m" )
{
    std::cout << std::left;
}

void SmartFixedArrayTester::Run()
{
  Test_Constructor();
  Test_Clear();
  Test_IsFull();
  Test_IsEmpty();
  Test_Size();
  Test_Search();
  Test_ShiftLeft();
  Test_ShiftRight();

  Test_PushBack();
  Test_PopBack();
  Test_GetBack();
}

void SmartFixedArrayTester::Test_Constructor()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_Constructor ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if constructor is implemented";
        bool exp_out = true;
        bool act_out = true;

        try                                             { SmartFixedArray<int> arr; }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << "Function not yet implemented! (Remove NotImplementedException)" << std::endl << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "When a new SmartFixedArray is created, \n\t item_count and should be 0 and ARRAY_SIZE should be 100.";
        size_t exp_out_arraysize = 100;
        size_t exp_out_itemcount = 0;
        size_t act_out_arraysize;
        size_t act_out_itemcount;

        SmartFixedArray<int> arr;
        act_out_arraysize = arr.ARRAY_SIZE;
        act_out_itemcount = arr.item_count;

        if ( exp_out_arraysize != act_out_arraysize )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << " Expected ARRAY_SIZE: " << exp_out_arraysize << std::endl;
            std::cout << " Actual   ARRAY_SIZE: " << act_out_arraysize << std::endl;
        }
        else if ( exp_out_itemcount != act_out_itemcount )
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << " Expected item_count: " << exp_out_itemcount << std::endl;
            std::cout << " Actual   item_count: " << act_out_itemcount << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_Clear()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_Clear ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if Clear is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            arr.Clear();
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << "Function not yet implemented! (Remove NotImplementedException)" << std::endl << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "When Clear is called, item_count should be 0.";
        size_t exp_out_itemcount = 0;
        size_t act_out_itemcount;

        SmartFixedArray<int> arr;
        arr.item_count = 100;
        arr.Clear();
        act_out_itemcount = arr.item_count;

        if ( exp_out_itemcount != act_out_itemcount )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << " Expected item_count: " << exp_out_itemcount << std::endl;
            std::cout << " Actual   item_count: " << act_out_itemcount << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_IsFull()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_IsFull ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if IsFull is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            arr.IsFull();
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << "Function not yet implemented! (Remove NotImplementedException)" << std::endl << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if IsFull returns true when full.";
        bool exp_out = true;
        bool act_out;

        SmartFixedArray<int> arr;
        arr.item_count = 100;
        act_out = arr.IsFull();

        if ( act_out != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << " Expected arr.IsFull(): " << BoolToStr( exp_out ) << std::endl;
            std::cout << " Actual   arr.IsFull(): " << BoolToStr( act_out ) << std::endl;
            std::cout << " item_count:            " << arr.item_count << std::endl;
            std::cout << " ARRAY_SIZE:            " << arr.ARRAY_SIZE << std::endl;
            std::cout << " (If item count matches ARRAY_SIZE, this signifies the SmartFixedArray is full.) " << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if IsFull returns false when not full.";
        bool exp_out = false;
        bool act_out;

        SmartFixedArray<int> arr;
        arr.item_count = 2;
        act_out = arr.IsFull();

        if ( act_out != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << " Expected arr.IsFull(): " << BoolToStr( exp_out ) << std::endl;
            std::cout << " Actual   arr.IsFull(): " << BoolToStr( act_out ) << std::endl;
            std::cout << " item_count:            " << arr.item_count << std::endl;
            std::cout << " ARRAY_SIZE:            " << arr.ARRAY_SIZE << std::endl;
            std::cout << " (If item count matches ARRAY_SIZE, this signifies the SmartFixedArray is full.) " << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_IsEmpty()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_IsEmpty ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if IsEmpty is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            arr.IsEmpty();
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "* Expected SmartFixedArray::IsEmpty to NOT throw NotImplementedException" << std::endl;
            std::cout << "  Make sure to remove this throw once you've implemented the function." << std::endl;
            std::cout << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if IsEmpty returns true when SmartFixedArray is empty.";
        bool exp_out = true;
        bool act_out;

        SmartFixedArray<int> arr;
        arr.item_count = 0;
        act_out = arr.IsEmpty();

        if ( act_out != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << " Expected arr.IsFull(): " << BoolToStr( exp_out ) << std::endl;
            std::cout << " Actual   arr.IsFull(): " << BoolToStr( act_out ) << std::endl;
            std::cout << " item_count:            " << arr.item_count << std::endl;
            std::cout << " (If item count is 0, this signifies the SmartFixedArray is empty.) " << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if IsEmpty returns false when SmartFixedArray is not empty.";
        bool exp_out = false;
        bool act_out;

        SmartFixedArray<int> arr;
        arr.item_count = 1;
        act_out = arr.IsEmpty();

        if ( act_out != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << " Expected arr.IsFull(): " << BoolToStr( exp_out ) << std::endl;
            std::cout << " Actual   arr.IsFull(): " << BoolToStr( act_out ) << std::endl;
            std::cout << " item_count:            " << arr.item_count << std::endl;
            std::cout << " (If item count is NOT 0, this signifies the SmartFixedArray is NOT empty.) " << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_Size()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_Size ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if Size is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            arr.Size();
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "* Expected SmartFixedArray::Size to NOT throw NotImplementedException" << std::endl;
            std::cout << "  Make sure to remove this throw once you've implemented the function." << std::endl;
            std::cout << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if Size returns correct value";
        size_t exp_out = 5;

        SmartFixedArray<int> arr;
        arr.item_count = 5;

        if ( arr.item_count != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << " Expected item_count: " << exp_out << std::endl;
            std::cout << " Actual   item_count: " << arr.item_count << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if Size returns correct value";
        size_t exp_out = 10;

        SmartFixedArray<int> arr;
        arr.item_count = 10;

        if ( arr.item_count != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << " Expected item_count: " << exp_out << std::endl;
            std::cout << " Actual   item_count: " << arr.item_count << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_ShiftLeft()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_ShiftLeft ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if ShiftLeft is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            arr.ShiftLeft( 3 );
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << "Function not yet implemented! (Remove NotImplementedException)" << std::endl << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if InvalidIndexException is thrown if index is invalid.";
        bool exp_exception_thrown = true;
        bool act_exception_thrown = false;

        SmartFixedArray<int> arr;
        arr.item_count = 10;
        try {
            arr.ShiftLeft( 20 );
        }
        catch( Exception::InvalidIndexException& ex ) { act_exception_thrown = true; }
        catch( ... ) { }

        if ( act_exception_thrown != exp_exception_thrown )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "* Valid indices are 0 to item_count, if index provided is" << std::endl;
            std::cout << "  negative or above item_count, then InvalidIndexException should be thrown." << std::endl;
            std::cout << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Populate array, call ShiftLeft, check positions.";
        std::string exp_out_values[] = { "B", "C", "D", "E" };

        SmartFixedArray<std::string> arr;
        arr.item_count = 5;
        arr.data[0] = "A";
        arr.data[1] = "B";
        arr.data[2] = "C";
        arr.data[3] = "D";
        arr.data[4] = "E";
        arr.ShiftLeft( 0 );

        // Check results
        bool all_pass = true;
        std::string errors = "";
        for ( int i = 0; i < 4; i++ )
        {
            if ( arr.data[i] != exp_out_values[i] )
            {
                all_pass = false;
                break;
            }
        }

        if ( !all_pass )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name    << std::endl;
            std::cout << errors << std::endl;
            std::cout << std::setw( 20 ) << "VARIABLE"      << std::setw( 20 ) << "EXPECTED"            << std::setw( 20 ) << "ACTUAL"              << std::endl << std::string( 60, '-' ) << std::endl;
            for ( int i = 0; i < 4; i++ )
            {
                std::cout << std::setw( 20 ) << std::string( "data[" + std::to_string( i ) + "]" )
                          << std::setw( 20 ) << exp_out_values[i]
                          << std::setw( 20 ) << arr.data[i] << std::endl;
            }
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Populate array, call ShiftLeft, check positions.";
        std::string exp_out_values[] = { "A", "B", "D", "E" };

        SmartFixedArray<std::string> arr;
        arr.item_count = 5;
        arr.data[0] = "A";
        arr.data[1] = "B";
        arr.data[2] = "C";
        arr.data[3] = "D";
        arr.data[4] = "E";
        arr.ShiftLeft( 2 );

        // Check results
        bool all_pass = true;
        std::string errors = "";
        for ( int i = 0; i < 4; i++ )
        {
            if ( arr.data[i] != exp_out_values[i] )
            {
                all_pass = false;
                break;
            }
        }

        if ( !all_pass )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name    << std::endl;
            std::cout << errors << std::endl;
            std::cout << std::setw( 20 ) << "VARIABLE"      << std::setw( 20 ) << "EXPECTED"            << std::setw( 20 ) << "ACTUAL"              << std::endl << std::string( 60, '-' ) << std::endl;
            for ( int i = 0; i < 4; i++ )
            {
                std::cout << std::setw( 20 ) << std::string( "data[" + std::to_string( i ) + "]" )
                          << std::setw( 20 ) << exp_out_values[i]
                          << std::setw( 20 ) << arr.data[i] << std::endl;
            }
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_ShiftRight()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_ShiftRight ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if ShiftRight is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            arr.ShiftRight( 3 );
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << "Function not yet implemented! (Remove NotImplementedException)" << std::endl << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if InvalidIndexException is thrown if index is invalid.";
        bool exp_exception_thrown = true;
        bool act_exception_thrown = false;

        SmartFixedArray<int> arr;
        arr.item_count = 10;
        try {
            arr.ShiftLeft( 20 );
        }
        catch( Exception::InvalidIndexException& ex ) { act_exception_thrown = true; }
        catch( ... ) { }

        if ( act_exception_thrown != exp_exception_thrown )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "* Valid indices are 0 to item_count, if index provided is" << std::endl;
            std::cout << "  negative or above item_count, then InvalidIndexException should be thrown." << std::endl;
            std::cout << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if StructureFullException is thrown if array is full.";
        bool exp_exception_thrown = true;
        bool act_exception_thrown = false;

        SmartFixedArray<int> arr;
        arr.item_count = 100;
        try {
            arr.ShiftRight( 20 );
        }
        catch( Exception::StructureFullException& ex ) { act_exception_thrown = true; }
        catch( ... ) { }

        if ( act_exception_thrown != exp_exception_thrown )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "* Expected StructureFullException to be thrown when Pushing into FULL array." << std::endl;
            std::cout << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Populate array, call ShiftRight, check positions.";
        std::string exp_out_values[] = { "A", "B", "B", "C", "D", "E" };

        SmartFixedArray<std::string> arr;
        arr.item_count = 5;
        arr.data[0] = "A";
        arr.data[1] = "B";
        arr.data[2] = "C";
        arr.data[3] = "D";
        arr.data[4] = "E";
        arr.ShiftRight( 1 );

        // Check results
        bool all_pass = true;
        std::string errors = "";
        for ( int i = 0; i < 6; i++ )
        {
            if ( arr.data[i] != exp_out_values[i] )
            {
                all_pass = false;
                break;
            }
        }

        if ( !all_pass )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name    << std::endl;
            std::cout << errors << std::endl;
            std::cout << std::setw( 20 ) << "VARIABLE"      << std::setw( 20 ) << "EXPECTED"            << std::setw( 20 ) << "ACTUAL"              << std::endl << std::string( 60, '-' ) << std::endl;
            for ( int i = 0; i < 4; i++ )
            {
                std::cout << std::setw( 20 ) << std::string( "data[" + std::to_string( i ) + "]" )
                          << std::setw( 20 ) << exp_out_values[i]
                          << std::setw( 20 ) << arr.data[i] << std::endl;
            }
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Populate array, call ShiftRight, check positions.";
        std::string exp_out_values[] = { "A", "B", "C", "C", "D", "E" };

        SmartFixedArray<std::string> arr;
        arr.item_count = 5;
        arr.data[0] = "A";
        arr.data[1] = "B";
        arr.data[2] = "C";
        arr.data[3] = "D";
        arr.data[4] = "E";
        arr.ShiftRight( 2 );

        // Check results
        bool all_pass = true;
        std::string errors = "";
        for ( int i = 0; i < 6; i++ )
        {
            if ( arr.data[i] != exp_out_values[i] )
            {
                all_pass = false;
                break;
            }
        }

        if ( !all_pass )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name    << std::endl;
            std::cout << errors << std::endl;
            std::cout << std::setw( 20 ) << "VARIABLE"      << std::setw( 20 ) << "EXPECTED"            << std::setw( 20 ) << "ACTUAL"              << std::endl << std::string( 60, '-' ) << std::endl;
            for ( int i = 0; i < 4; i++ )
            {
                std::cout << std::setw( 20 ) << std::string( "data[" + std::to_string( i ) + "]" )
                          << std::setw( 20 ) << exp_out_values[i]
                          << std::setw( 20 ) << arr.data[i] << std::endl;
            }
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_Search()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_Search ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if Search is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            arr.Search( 10 );
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << "Function not yet implemented! (Remove NotImplementedException)" << std::endl << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Set up array, search for an item.";
        SmartFixedArray<std::string> arr;
        arr.data[0] = "A";
        arr.data[1] = "B";
        arr.data[2] = "C";
        arr.item_count = 3;

        int exp_out = 1;
        int act_out = arr.Search( "B" );

        if ( act_out != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "Array data: ";
            for ( int i = 0; i < 3; i++ ) { std::cout << i << " = " << arr.data[i] << "   "; }
            std::cout << std::endl;
            std::cout << " Expected return of Search( \"B\" ):            " << exp_out << std::endl;
            std::cout << " Actual   return of Search( \"B\" ):            " << act_out << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Search for an item that isn't in the array. Get ItemNotFoundException?";
        SmartFixedArray<std::string> arr;
        arr.data[0] = "A";
        arr.data[1] = "B";
        arr.data[2] = "C";
        arr.item_count = 3;

        bool exp_out = true;
        bool act_out = false;

        try {
            arr.Search( "Z" );
        }
        catch( Exception::ItemNotFoundException& ex ) { act_out = true; }
        catch( ... ) { }

        if ( act_out != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "* Expected SmartFixedArray::Search to throw ItemNotFoundException" << std::endl;
            std::cout << "  if item passed in is not found." << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_PushBack()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_PushBack ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if PushBack is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            arr.PushBack( 20 );
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << "Function not yet implemented! (Remove NotImplementedException)" << std::endl << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Check if StructureFullException is thrown if array is full.";
        bool exp_exception_thrown = true;
        bool act_exception_thrown = false;

        SmartFixedArray<int> arr;
        arr.item_count = 100;
        try {
            arr.PushBack( 20 );
        }
        catch( Exception::StructureFullException& ex ) { act_exception_thrown = true; }
        catch( ... ) { }

        if ( act_exception_thrown != exp_exception_thrown )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "* Expected StructureFullException to be thrown when Pushing into FULL array." << std::endl;
            std::cout << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Add three items, check they're in the correct position.";
        size_t exp_out_itemcount = 3;
        size_t act_out_itemcount;
        std::string exp_out_values[] = { "a", "b", "c" };

        SmartFixedArray<std::string> arr;
        for ( int i = 0; i < 3; i++ ) { arr.PushBack( exp_out_values[i] ); }
        act_out_itemcount = arr.item_count;

        // Check results
        bool all_pass = true;
        std::string errors = "";
        if ( exp_out_itemcount != act_out_itemcount )
        {
            all_pass = false;
        }

        for ( int i = 0; i < 3; i++ )
        {
            if ( arr.data[i] != exp_out_values[i] )
            {
                all_pass = false;
                break;
            }
        }

        if ( !all_pass )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name    << std::endl;
            std::cout << errors << std::endl;
            std::cout << std::setw( 20 ) << "VARIABLE"      << std::setw( 20 ) << "EXPECTED"            << std::setw( 20 ) << "ACTUAL"              << std::endl << std::string( 60, '-' ) << std::endl;
            std::cout << std::setw( 20 ) << "item_count"    << std::setw( 20 ) << exp_out_itemcount     << std::setw( 20 ) << act_out_itemcount     << std::endl;
            for ( int i = 0; i < 3; i++ )
            {
                std::cout << std::setw( 20 ) << std::string( "data[" + std::to_string( i ) + "]" )
                          << std::setw( 20 ) << exp_out_values[i]
                          << std::setw( 20 ) << arr.data[i] << std::endl;
            }
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_PopBack()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_PopBack ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if PopBack is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            arr.PopBack();
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << "Function not yet implemented! (Remove NotImplementedException)" << std::endl << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Try to Pop from empty array. Get StructureEmptyException?";
        SmartFixedArray<std::string> arr;
        arr.item_count = 0;

        bool exp_out = true;
        bool act_out = false;

        try {
            arr.PopBack();
        }
        catch( Exception::StructureEmptyException& ex ) { act_out = true; }
        catch( ... ) { }

        if ( act_out != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "* Expected SmartFixedArray::PopBack to throw StructureEmptyException" << std::endl;
            std::cout << "  if data structure is empty" << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Set up array, PopBack, check positions.";
        SmartFixedArray<std::string> arr;
        arr.item_count = 3;
        arr.data[0] = "A";
        arr.data[1] = "B";
        arr.data[2] = "C";

        arr.PopBack();

        size_t exp_out_itemcount = 2;
        size_t act_out_itemcount = arr.item_count;
        std::string exp_out_values[] = { "A", "B" };

        // Check results
        bool all_pass = true;
        std::string errors = "";
        if ( exp_out_itemcount != act_out_itemcount )
        {
            all_pass = false;
        }

        for ( size_t i = 0; i < exp_out_itemcount; i++ )
        {
            if ( arr.data[i] != exp_out_values[i] )
            {
                all_pass = false;
                break;
            }
        }

        if ( !all_pass )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name    << std::endl;
            std::cout << errors << std::endl;
            std::cout << std::setw( 20 ) << "VARIABLE"      << std::setw( 20 ) << "EXPECTED"            << std::setw( 20 ) << "ACTUAL"              << std::endl << std::string( 60, '-' ) << std::endl;
            std::cout << std::setw( 20 ) << "item_count"    << std::setw( 20 ) << exp_out_itemcount     << std::setw( 20 ) << act_out_itemcount     << std::endl;
            for ( size_t i = 0; i < exp_out_itemcount; i++ )
            {
                std::cout << std::setw( 20 ) << std::string( "data[" + std::to_string( i ) + "]" )
                          << std::setw( 20 ) << exp_out_values[i]
                          << std::setw( 20 ) << arr.data[i] << std::endl;
            }
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}

void SmartFixedArrayTester::Test_GetBack()
{
    std::string test_set_name = "** SmartFixedArrayTester Test_GetBack ";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check if GetBack is implemented";
        bool exp_out = true;
        bool act_out = true;

        try {
            SmartFixedArray<int> arr;
            int x = arr.GetBack();
            x++; // Avoid annoying compiler warnings
        }
        catch( Exception::NotImplementedException& ex ) { act_out = false; }
        catch( ... ) { }

        if ( exp_out != act_out )
        {
            std::cout << RED << "Function not yet implemented! (Remove NotImplementedException)" << std::endl << CLR;
            return;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Try to Get from empty array. Get StructureEmptyException?";
        SmartFixedArray<std::string> arr;
        arr.item_count = 0;

        bool exp_out = true;
        bool act_out = false;

        try {
            arr.GetBack();
        }
        catch( Exception::StructureEmptyException& ex ) { act_out = true; }
        catch( ... ) { }

        if ( act_out != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "* Expected SmartFixedArray::PopBack to throw StructureEmptyException" << std::endl;
            std::cout << "  if data structure is empty" << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Set up array, call Get, check correct item returned.";
        SmartFixedArray<std::string> arr;
        arr.data[0] = "A";
        arr.data[1] = "B";
        arr.data[2] = "C";
        arr.item_count = 3;

        std::string exp_out = "C";
        std::string act_out = arr.GetBack();

        if ( act_out != exp_out )
        {
            std::cout << RED << std::endl << "[FAIL] " << test_name << std::endl;
            std::cout << "Array data: ";
            for ( int i = 0; i < 3; i++ ) { std::cout << i << " = " << arr.data[i] << "   "; }
            std::cout << std::endl << std::endl;
            std::cout << std::setw( 20 ) << "VARIABLE"      << std::setw( 20 ) << "EXPECTED"            << std::setw( 20 ) << "ACTUAL"              << std::endl << std::string( 60, '-' ) << std::endl;
            std::cout << std::setw( 20 ) << "arr.GetBack()"  << std::setw( 20 ) << exp_out     << std::setw( 20 ) << act_out     << std::endl;
        }
        else
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
    } std::cout << CLR;
}
