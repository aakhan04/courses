#ifndef _SMART_FIXED_ARRAY_TESTER
#define _SMART_FIXED_ARRAY_TESTER

#include "SmartFixedArrayTester.h"

#include <string>

class SmartFixedArrayTester
{
  public:
  SmartFixedArrayTester();
  void Run();
  void Test_Constructor();
  void Test_Clear();
  void Test_IsFull();
  void Test_IsEmpty();
  void Test_Size();
  void Test_PushBack();
  void Test_PopBack();
  void Test_GetBack();
  void Test_ShiftLeft();
  void Test_ShiftRight();
  void Test_Search();

  private:
  const std::string RED;
  const std::string GRN;
  const std::string BOLD;
  const std::string CLR;

  std::string BoolToStr( bool val )
  {
    return ( val ) ? "true" : "false";
  }
};

#endif
