#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "DataStructure/SmartFixedArray.h"
#include "Tester/SmartFixedArrayTester.h"

int main()
{
  SmartFixedArrayTester tester;
  tester.Run();

  return 0;
}
