# -*- mode: org -*-

C++ comes with a bunch of data structures we can use to store sets of data. Each of these have
different uses and work better for different designs. They all have documentation available
online so you can learn more about how these structures work there as well.


* *Without the STL: Traditional fixed-length array*

One of the first ways you may have learned to store a sequence of data is with a *traditional array* in C++.
These aren't "smart" at all, cannot be resized, and you either have to manually keep track of the size of the
array or do a calculation whenever you want to find the size.

- Declaration ::

They come in this form:

#+BEGIN_SRC cpp :class cpp
  const int ARRAY_SIZE = 100;
  int itemCount = 0;
  int myArray[ ARRAY_SIZE ];
#+END_SRC

The =ARRAY_SIZE= named constant and the =itemCount= variable are optional, however,
we have to manually keep track of the size of the array and how many items are stored
within it - it won't take care of that for us.

Reading and writing values to this array is simple:

#+BEGIN_SRC cpp :class cpp
  cout << "Enter a number: ";
  cin >> myArray[ 0 ];
  cout << "Item #0 is " << myArray[0] << endl;
#+END_SRC

And iterating through it requires, again, keeping track of the amount of items
we've stored in the array with some kind of variable:

- Iterating over elements ::

#+BEGIN_SRC cpp :class cpp
  for ( int i = 0; i < itemCount; i++ )
  {
    cout << i << " = " << myArray[i] << endl;
  }
#+END_SRC


-----
* *Without the STL: Dynamic array*

We can create *dynamic arrays* by using *pointers*. This allows us to define the size
of the array at /runtime/, instead of defining the size at /compile-time/ like with our
traditional fixed-length array. The downside is manually managing the memory - making sure
to free whatever we have allocated.

- Declaration ::

#+BEGIN_SRC cpp :class cpp
  int itemCount = 0;
  int arraySize;

  cout << "Enter an array size: ";
  cin >> arraySize;

  int* myArray = new int[ arraySize ];
#+END_SRC

Reading and writing values to the array is the same as with the traditional array. We just have to
make sure to free the memory before the program ends:

- Iterating over elements ::

#+BEGIN_SRC cpp :class cpp
  for ( int i = 0; i < itemCount; i++ )
  {
    cout << i << " = " << myArray[i] << endl;
  }
#+END_SRC

- Freeing memory ::

You need to free the memory you allocate before its pointer loses scope -
if this happens, you lose the address and that memory is now taken up and cannot be freed.

#+BEGIN_SRC cpp :class cpp
  delete [] myArray;
#+END_SRC


-----
* *STL std::array*
*Documentation:* https://cplusplus.com/reference/array/array/

he =array= from the STL is basically a class wrapping our traditional array. It allows us to use the array
as an object with basic *functions*, so that we don't have to keep track of as much.

- Declaration ::

#+BEGIN_SRC cpp :class cpp
  array<int, 100> myArray;
#+END_SRC

- Accessing the size ::

#+BEGIN_SRC cpp :class cpp
  cout << "Size: " << myArray.size();
#+END_SRC

- Accessing elements ::

#+BEGIN_SRC cpp :class cpp
  cout << "Enter item 0: ";
  cin >> myArray[0];
#+END_SRC

- Iterating over elements ::

#+BEGIN_SRC cpp :class cpp
  for ( int i = 0; i < myArray.size(); i++ )
  {
    cout << i << " = " << myArray[i] << endl;
  }
#+END_SRC




-----
* *STL std::vector*
*Documentation:* https://cplusplus.com/reference/vector/vector/

Vectors are *dynamic arrays* so you can add items to it and it will resize -
no worries on your part regarding resizing and managing memory.

- Declaration ::

#+BEGIN_SRC cpp :class cpp
  vector<int> myVector;
#+END_SRC

- Accessing the size ::

#+BEGIN_SRC cpp :class cpp
  cout << "Size: " << myVector.size();
#+END_SRC

- Accessing elements ::

Because you're generally not pre-allocating space for a vector,
you need to use the =push_back= function to add additional items
to the end of the vector's internal array.

#+BEGIN_SRC cpp :class cpp
  cout << "Enter item 0: ";
  int item;
  cin >> item;
  myVector.push_back( item );
#+END_SRC

- Iterating over elements ::

#+BEGIN_SRC cpp :class cpp
  for ( int i = 0; i < myVector.size(); i++ )
  {
    cout << i << " = " << myVector[i] << endl;
  }
#+END_SRC



-----
* *STL std::list*
*Documentation:* https://cplusplus.com/reference/list/list/

The list is /similar/ to a vector in that you can store any amount of items in it,
however you can only ever access the /front/ and the /back/ of the list -
not random items in the middle. You can still iterate over all the items, though.

- Declaration ::

#+BEGIN_SRC cpp :class cpp
  list<int> myList;
#+END_SRC

- Accessing the size ::

#+BEGIN_SRC cpp :class cpp
  cout << "Size: " << myList.size();
#+END_SRC

- Adding data ::

#+BEGIN_SRC cpp :class cpp
  myList.push_front( 1 ); // Stored at the start of the list
  myList.push_back( 10 ); // Stored at the end of the list
#+END_SRC

- Accessing data ::

#+BEGIN_SRC cpp :class cpp
  cout << "Front item: " << myList.get_front() << endl;
  cout << "Back item:  " << myList.get_back() << endl;
#+END_SRC

- Removing data ::

#+BEGIN_SRC cpp :class cpp
  myList.pop_front();
  myList.pop_back();
#+END_SRC

- Iterating over elements ::

#+BEGIN_SRC cpp :class cpp
  for ( auto& item : myList )
  {
    cout << item << endl;
  }
#+END_SRC


-----
* *STL std::stack*

[[file:../../images/topics/stack.png]]

*Documentation:* https://cplusplus.com/reference/stack/stack/

A stack is a type of restricted-access data type. It can store a series of items,
but you can only add and remove items from the /top/.

- Declaration ::

#+BEGIN_SRC cpp :class cpp
  stack<char> myStack;
#+END_SRC

- Accessing the size ::

#+BEGIN_SRC cpp :class cpp
  cout << "Size: " << myStack.size();
#+END_SRC

- Adding data ::

#+BEGIN_SRC cpp :class cpp
  myStack.push( 'A' );
  myStack.push( 'B' );
  myStack.push( 'C' );
#+END_SRC

- Accessing data ::

#+BEGIN_SRC cpp :class cpp
  cout << "Top item is: " << myStack.top() << endl;
#+END_SRC

- Removing data ::

#+BEGIN_SRC cpp :class cpp
  myStack.pop(); // Remove top item
#+END_SRC


-----
* *STL std::queue*

[[file:../../images/topics/queue.png]]

*Documentation:* https://cplusplus.com/reference/queue/queue/

A queue is another type of restricted-access data type. It stores a series of items,
with items being added at the /back/ of the queue, and being removed from the /front/ of the queue,
similar to waiting in line at the store.

- Declaration ::

#+BEGIN_SRC cpp :class cpp
  queue<char> myQueue;
#+END_SRC

- Accessing the size ::

#+BEGIN_SRC cpp :class cpp
  cout << "Size: " << myQueue.size() << endl;
#+END_SRC

- Adding data ::

#+BEGIN_SRC cpp :class cpp
  myQueue.push( 'A' );
  myQueue.push( 'B' );
  myQueue.push( 'C' );
#+END_SRC

- Accessing data ::

#+BEGIN_SRC cpp :class cpp
  cout << "The front item is: " << myQueue.front() << endl;
#+END_SRC

- Removing data ::

#+BEGIN_SRC cpp :class cpp
  myQueue.pop(); // Remove front item
#+END_SRC


-----
* *STL std::map*

[[file:../../images/topics/hashtable.png]]

*Documentation:* https://cplusplus.com/reference/map/map/

Our traditional arrays have *index* numbers (0, 1, 2, ...) and *elements* stored at that position in the array.

With a map, we can have any data type as a *unique identifier* for an *element*. This could be an integer,
but it doesn't have to be 0, 1, 2, and so on - it could be an employee ID, a phone number, etc., but the
data type of the identifier (called a *key*) can be any data type.

- Declaration ::

#+BEGIN_SRC cpp :class cpp
  map<int, string> area_codes;
#+END_SRC

- Adding data ::

#+BEGIN_SRC cpp :class cpp
  area_codes[913] = "Northeastern Kansas";
  area_codes[816] = "Northwestern Missouri";
#+END_SRC

- Accessing the size ::

#+BEGIN_SRC cpp :class cpp
  cout << "Size: " << area_codes.size();
#+END_SRC

- Accessing data ::

#+BEGIN_SRC cpp :class cpp
  int key;
  cout << "Enter an area code: ";
  cin >> key;

  cout << "The region for that is: " << area_codes[ key ];
#+END_SRC


- Iterating over elements ::

#+BEGIN_SRC cpp :class cpp
  for ( auto& item : area_codes )
  {
    cout << "Key: " << item.first << ", Value: " << item.second << endl;
  }
#+END_SRC


----------------------------

* *Review questions:*

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
1. Identify what "type" each of the following is... (traditional array / traditional dynamic array / array object / vector object)
   1. =vector<int> arr;=
   2. =int * arr = new int[100];=
   3. =array<int, 100> arr;=
   4. =int arr[100];=
2. Which of these std data types can be resized (store any amount of data after declaration)? (array / vector / list)
3. You can only add items to the ... of a stack, and you can only remove items from the ... of a stack.
4. You can only add items to the ... of a queue, and you can only remove items from the ... of a queue.
5. True or false: A map can use any data type for its unique key item.
#+END_HTML
