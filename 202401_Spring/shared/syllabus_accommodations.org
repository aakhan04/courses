# -*- mode: org -*-

- How do I get accommodations? - Access Services ::
   https://www.jccc.edu/student-resources/access-services/
   Access Services provides students with disabilities equal opportunity and access. Some of the accommodations and services include testing accommodations, note-taking assistance, sign language interpreting services, audiobooks/alternative text and assistive technology. 
- What if I'm having trouble making ends meet in my personal life? - Student Basic Needs Center ::
  https://www.jccc.edu/student-resources/basic-needs-center/
  Check website for schedule and location. The JCCC Student Assistance Fund is to help students facing a sudden and unforeseen emergency that has affected their ability to attend class or otherwise meet the academic obligations of a JCCC student. If you are experiencing food or housing insecurity, or other hardships, stop by COM 319 and visit with our helpful staff. 
- Is there someone I can talk to for my degree plan? - Academic Advising ::
  https://www.jccc.edu/student-resources/academic-counseling/
  JCCC has advisors to help you with:
  - Choose or change your major and stay on track for graduation.
  - Ensure a smooth transfer process to a 4-year institution.
  - Discover resources and tools available to help build your schedule, complete enrollment and receive help with coursework each semester.
  - Learn how to get involved in Student Senate, clubs and orgs, athletics, study abroad, service learning, honors and other leadership programs.
  - If there’s a hold on your account due to test scores, academic probation or suspension, you are required to meet with a counselor.
- Is there someone I can talk to for emotional support? - Personal Counseling ::
  https://www.jccc.edu/student-resources/personal-counseling/
  JCCC counselors provide a safe and confidential environment to talk about personal concerns. We advocate for students and assist with personal issues and make referrals to appropriate agencies when needed. 
- How do I get a tutor? - The Academic Achievement Center ::
  https://www.jccc.edu/student-resources/academic-resource-center/academic-achievement-center/
  The AAC is open for Zoom meetings and appointments. See the website for their schedule. Meet with a Learning Specialist for help with classes and study skills, a Reading Specialist to improve understanding of your academic reading, or a tutor to help you with specific courses and college study skills. You can sign up for workshops to get off to a Smart Start in your semester or analyze your exam scores! 
- How can I report ethical concerns? - Ethics Report Line ::
  https://www.jccc.edu/about/leadership-governance/administration/audit-advisory/ethics-line/
  You can report instances of discrimination and other ethical issues to JCCC via the EthicsPoint line. 
- What other student resources are there? - Student Resources Directory ::
  https://www.jccc.edu/student-resources/
