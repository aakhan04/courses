# -*- mode: org -*-

* *Visual Studio*

- *Create a project:* ::
  - 📽️ Video: https://youtu.be/4f1UJSGAcT8
  - Open Visual Studio
  - Make sure the language dropdown is set to *"C++"*.
  - Select *"Empty Project"*, then click *"Next"*.
  - *Configure your new project* page:
    - Set the *"Project name"* to whatever you'd like; best to name it after the assignment you're working on.
    - Set the *"Location"* to within your repository folder.
    - I would recomment checking the *"Place solution and project in the same directory"* option, but it's not required.
    - Click on *"Create"*.


- *Add a NEW file to your project:* ::
  - 📽️ Video: https://youtu.be/9-VgiCXgdxk
  - Make sure you can see the *Solution Explorer* pane in your IDE. If it doesn't show up, go to the *View* dropdown menu and select *"Solution Explorer"*.
  - Right-click your project in the Solution Explorer. Go to *"Add"*, then *"New item..."*.
  - Type in a filename, such as "main.cpp", "Functions.h", etc. *"Add"* when done.


- *Add an EXISTING file to your project:* ::
  - 📽️ Video: https://youtu.be/0x-srqvdmXg
  - Make sure you can see the *Solution Explorer* pane in your IDE. If it doesn't show up, go to the *View* dropdown menu and select *"Solution Explorer"*.
  - Right-click your project in the Solution Explorer. Go to *"Add"*, then *"Existing item..."*.
  - Locate the file you wish to add to the project, and then click *"Add"*.


- Building the program:* ::
  - 📽️ Video: https://youtu.be/DmrgpzXUk2w
  - From the dropdown menu, select *"Build"*, then click *"Build"* to start compiling.
  - An *Output* window should show up at the bottom of the program with information. If any build errors occur, you can see them in the *Error List*.


- *Running the program:* ::
  - 📽️ Video: https://youtu.be/pP0SWeDHNaQ
  - To run the program, you can click on the *"▶️ Local Windows Debugger"* button.


- *Reopening your project later:* ::
  - 📽️ Video: https://youtu.be/cLKi1QgbeEo
  - To reopen your project, you can either locate the *.sln* file and double-click it to open it in Visual Studio, OR
  - You can open Visual Studio, and select *"Open a project or solution"*, then locate your *.sln* file and open it.


- *Opening code windows side-by-side:* ::
  - 📽️ Video: https://youtu.be/wCc9LU4RZCM


- *Go to declaration, definition, and find references:* ::
  - 📽️ Video: https://youtu.be/CzoeUfOu6Ek

--------------------------------------------

* *Code::Blocks*

- *Create a project:* ::
  - 📽️ Video: https://youtu.be/mGoCoV0L8hU
  - Click on the *"Create a new project"* link.
  - Select *"Empty project"* and click *"Go"*.
  - Set a *Project title* to whatever you'd like; best to name it after the assignment you're working on.
  - Set the *Folder to create project in:* to a folder within your repository directory.
  - Click *"Next >"*.
  - Use the default Compiler and *Create Debug* and *Create Release* config settings. Click *"Finish"*.



- *Add a NEW file to your project:* ::
  - 📽️ Video: https://youtu.be/q9D8LIn0QCg
  - Go to the *File* dropdown menu and select *New* and *Empty file*.
  - It will ask if you want to save the file, select *Yes*.
  - Give the file a name then click *"Save"*.



- *Add an EXISTING file to your project:* ::
  - 📽️ Video: https://youtu.be/nCkNvLgf4e0
  - Right-click on your project in the left pane and select *"Add files..."*.
  - Locate the files you want to add, then click *"Open"*.



- *Building and running the program:* ::
  - 📽️ Video: https://youtu.be/OY1Pg7Ao990
  - From the dropdown menu select *Build* and then *Build*.
  - The build status will show up at the bottom under "Build log".
  - If the build succeeds, you can run the program using the *Build* dropdown menu then *Run*.



- *Reopening your project later:* ::
  - 📽️ Video: https://youtu.be/DghoQhkgqc8
  - To reopen your project, you can either locate the *.cbp* file and double-click it to open it in Code::Blocks, OR
  - You can open Code::Blocks, and select *"Open an existing project"*, then locate your *.cbp* file and open it.



- *Opening code windows side-by-side:* ::
  - 📽️ Video: https://youtu.be/XQqJ5ncS60s



- *Go to declaration, definition, and find references:* ::
  - 📽️ Video: https://youtu.be/4Ev9iDqba8I


--------------------------------------------

* *Xcode*

 (@ProgrammingKnowledge channel): https://www.youtube.com/watch?v=F6QZ2atZrDw

- *Create a project:* ::
  - Open Xcode
  - Click on *"Create a new Xcode project"*
  - On the *Choose a template for your new project:* screen, click on the *"macOS"* option, then select *"Command Line Tool"*. Click *"Next"*.
  - Set a *Product Name*, *Organization Identifier*, and make sure the *Language* is set to *"C++"*. Click *"Next"*.
  - Select a location where to save your project (such as in your repository folder). Click *"Create"*.



- *Add a NEW file to your project:* ::
  - On the left pane, right-click on your project folder and click "*New File...*".
  - On the *Choose a template for your new file:* screen, click on *"macOS"* (if it's not already there) and select either a header .h file or a source .cpp file.



- *Add an EXISTING file to your project:* ::
  - On the left pane, right-click on your project folder and click "*Add Files to (PROJECT)..."*.



- *Building and running the program:* ::
  - Click on the Play button on the top of the left pane. It will automatically build the program and run it.
  - The program output shows up in a small window at the bottom of Xcode.



- *Reopening your project later:* ::
  - Open Xcode
  - Click on *"Open a project or file"*.
  - Locate your project.



- *Go to declaration, definition, and find references:* ::
  - 📃 Docs: https://www.avanderlee.com/workflow/essential-shortcuts-xcode-workflow/#jump-to-definition-directly


--------------------------------------------

* *VSCode*

- *Create a project:* ::
  - 📽️ Video: https://youtu.be/rNF7ZZW5Biw
  - Create a folder on your computer for your project. It should be located in your repository directory somewhere.
  - Open VSCode, and select *"Open Folder..."* and open the folder you just created.



- *Add a NEW file to your project:* ::
  - 📽️ Video: https://youtu.be/obcAcHvAltk
  - Click on the *"Add new file..."* button to create a new file in your project folder.



- *Add an EXISTING file to your project:* ::
  - 📽️ Video: https://youtu.be/LgvHQ7bvtFU
  - Any files added to the project folder will show up in your project.



- *Building and running the program:* ::
  - 📽️ Video: https://youtu.be/lps2-W7B4H0
  - Click on *"View"* and select *"Terminal"*.
  - In the terminal, type in your build command, such as =g++ *.h *.cpp=
  - To run the program afterwards, use =./a.out= (Linux/Mac) or =.\a.exe= (Win)



- *Reopening your project later:* ::
  - 📽️ Video: https://youtu.be/iccpI3vpUck
  - When you open VSCode, select *"Open folder"* and navigate to your folder.



- *Opening code windows side-by-side:* ::
  - 📽️ Video: https://youtu.be/LAt75CEOWJ4



- *Go to declaration, definition, and find references:* ::
  - 📽️ Video: https://youtu.be/d9FW0CdddNQ
