# -*- mode: org -*-

*What is a class?*

*What is an object?*

*What is an access modifier?*

*What is public?*

*What is private?*

*What is a member variable?*

*What is a member function?*

*How do you tell a member variable from a parameter variable when inside of a class' function?*

*How do you declare a class? (Write down example code)*

*How do you declare an object variable of some class type? (Write down example code)*

*What kind of file do class declarations go in?*

*What kind of file do class function definitions go in?*

*What does a class function definition function header look like? (it's a little different from a non-class function)*

