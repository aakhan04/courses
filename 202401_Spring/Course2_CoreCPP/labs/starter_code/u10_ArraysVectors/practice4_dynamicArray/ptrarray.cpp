// PROGRAM: Practice creating a dynamic array using a pointer
#include <iostream>
#include <array>
#include <string>
using namespace std;

int main()
{
  // TODO: Declare a size_t variable named total_classes.


  // TODO: Ask the user how many classes they have. Store their answer in total_classes.
  cout << "How many classes do you have? ";
  cin >> total_classes;

  // TODO: Use a pointer to allocate space for a new dynamic array of integers.
  // The size should be total_classes.


  cout << "Getting input:" << endl;


  // TODO: Use a for loop from i = 0 to my_classes.size().
  // Within the loop, ask the user to enter class # i, get their input and store it in the array at my_classes[i].


  cout << endl << "Resulting array:" << endl;


  // TODO: Use a for loop from i = 0 to my_classes.size().
  // Within the loop, display the index (i) and element (my_classes[i]).


  // TODO: Free the memory at the my_classes address.


  return 0;
}
