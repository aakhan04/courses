# -*- mode: org -*-

*Example program output:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
1. Run AUTOMATED TESTS
2. Run PROGRAMS
>> 2
Enter starting salary: $100000
Enter raise per year (as decimal): 0.05
Enter the amount of years to compute: 4
RESULT: 121550.00
Check results.txt for additional information
#+END_SRC

*Example file output:*

#+ATTR_HTML: :class fileout
#+BEGIN_SRC fileout :class fileout
Starting salary: $100000
Year: 1, Salary: $105000
Year: 2, Salary: $110250
Year: 3, Salary: $115762
Year: 4, Salary: $121551
#+END_SRC

This program calculates an increase in salary over time based on some percent.
Within the =raise.cpp= file you'll see the StudentCode function:

#+BEGIN_SRC cpp :class cpp
int StudentCode( float starting_salary, float raise_per_year, int years )
{
  // TODO: Create an ofstream object named `output`, open "results.txt".


  // TODO: Create float called `updated_salary`. Initialize it to the `starting_salary` amount.
  float updated_salary = 0;


  // TODO: Write "Starting salary:" and the `starting_salary` to the `output` file.


  // TODO: Use a loop, you need to create a counter variable that goes from 1 to `years`.
  // Within the loop, calculate the updated salary with
  // `updated_salary = updated_salary + ( updated_salary * raise_per_year );`
  // Use `output` to display the year (counter) and the `updated_salary` each iteration.


  // TODO: Return the `updated_salary` as the result.
  return updated_salary;
}
#+END_SRC

Notice that this function takes in three variables that we'll be using:
- =starting_salary=
- =raise_per_year=
- =years=

We'll be calculating the update and writing the result to our output file.
Do the following:

1. Create an =ofstream= object named =output=.
2. Open the "results.txt" file.
3. =updated_salary= has already been declared and initialized to 0.
4. Write "Starting salary:" and the =starting_salary= to the =output= file.
5. Use a for loop that goes from =counter = 1= to =years=. With the loop:
   1. Update the salary: =updated_salary = updated_salary + ( updated_salary * raise_per_year );=
   2. Output the current information (=counter= and =updated_salary=) to the =output= file.
6. After the loop is over, return the =updated_salary=.


When the program is run the details will be calculated and written to an output file.
Check the "results.txt" file on your computer after running the program to make sure
its output looks like the example above.
