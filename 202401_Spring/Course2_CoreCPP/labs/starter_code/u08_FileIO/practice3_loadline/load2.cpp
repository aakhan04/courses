// PROGRAM: Practice loading text files, one line at a time

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <fstream>    // Library that contains `ifstream` and `ofstream`
using namespace std;  // Using the C++ STanDard libraries

/*
 *Example program output:*
 ------------------------------------------
 LINE #0, READ IN: We're no strangers to love
 LINE #1, READ IN: You know the rules and so do I (do I)
 LINE #2, READ IN: A full commitment's what I'm thinking of
 ------------------------------------------


 *Example input file:*
 ------------------------------------------
 We're no strangers to love
 You know the rules and so do I (do I)
 A full commitment's what I'm thinking of
 You wouldn't get this from any other guy

 I just wanna tell you how I'm feeling
 Gotta make you understand
 ------------------------------------------

 *Program requirements:*

 For this program, copy what you have for practice 2 (`practice2_loadsingle/load1.cpp`).
 Instead of using the `>>` operator to read in from the `input` file, we'll use the
 `getline` function instead.

 Each time you have `input >> word`, replace that line instead with `getline( input, line );`.

 Once you run the program, it will show three lines of text read in.
 */

int main()
{
  string line;
  int counter = 0;



  return 0;
}
