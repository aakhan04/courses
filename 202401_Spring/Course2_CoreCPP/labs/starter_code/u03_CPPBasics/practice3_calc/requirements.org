# -*- mode: org -*-

*=practice3_calc=:*

*Starter code:*

#+BEGIN_SRC cpp :class cpp
// PROGRAM: Variable arithmetic practice

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{

  // Declaring variables
  float num1, num2;

  // Ask the user to enter num1
  cout << "Please enter num1: "; // Display prompt
  cin >> num1;                   // Get input from keyboard

  // Ask the user to enter num2
  cout << "Please enter num2: "; // Display prompt
  cin >> num2;                   // Get input from keyboard

  // Declaring more variables
  float sum, difference, product, quotient;

  // TODO: Assign the variable `sum` the value of `num1` plus `num2`.


  // TODO: Assign the variable `difference` the value of `num1` minus `num2`.


  // TODO: Assign the variable `product` the value of `num1` times `num2`.


  // TODO: Assign the variable `quotient` the value of `num1` divided by `num2`.


  cout << endl; // Adding more space
  cout << "ARITHMETIC:" << endl;
  cout << "SUM:        " << num1 << " + " << num2 << " = " << sum << endl;
  cout << "DIFFERENCE: " << num1 << " - " << num2 << " = " << difference << endl;
  cout << "PRODUCT:    " << num1 << " * " << num2 << " = " << product << endl;
  cout << "QUOTIENT:   " << num1 << " / " << num2 << " = " << quotient << endl;

  // Return 0 means quit program with no errors, in this context.
  return 0;
}
#+END_SRC

For this program, there are two float variables declared: =num1= and =num2=.
The program already contains some =cin= statements to get values into unm1 and num2.

There are also four "results" float variables declared: =sum=, =difference=, =product=, and =quotient=.
Under teach comment that says =TODO=, write down a math operation:

1. Add =num1= and =num2= and store the result in the =sum= variable.
1. Subtract =num1= and =num2= and store the result in the =difference= variable.
1. Multiply =num1= and =num2= and store the result in the =product= variable.
1. Divide =num1= and =num2= and store the result in the =quotient= variable.

Remember that assignment statements are written in this form:

=VARIABLE = VALUE;=

After implementing it, build and test the program. If the math comes out wrong, then try to investigate
your code and see if you can find a *logic error*.

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
$ g++ calc.cpp
$ ./a.exe
Please enter num1: 5
Please enter num2: 10

ARITHMETIC:
SUM:        5 + 10 = 15
DIFFERENCE: 5 - 10 = -5
PRODUCT:    5 * 10 = 50
QUOTIENT:   5 / 10 = 0.5
#+END_SRC
