# -*- mode: org -*-

*=practice1_greeting=:*

This program starts off partially written:

#+BEGIN_SRC cpp :class cpp
// PROGRAM: Console output practice

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  // TODO: Update the placeholder text below, NAME, COLOR, and SOMETHING, with your own text.

  cout << "Hello, I'm: NAME" << endl;
  cout << "My favorite color is: COLOR" << endl;
  cout << "This semester I hope to learn about... SOMETHING" << endl;

  // Return 0 means quit program with no errors, in this context.
  return 0;
}
#+END_SRC

There are certain comments marked =TODO= that have instructions.
For this assignment, you'll do the following:

1. Replace =NAME= with your name
2. Replace =COLOR= with your favorite color
3. Replace =SOMETHING= with something you'd like to learn about

After you've substituted the text, we're going to build and run the program.

Open up the *terminal*, or *power shell*, within the =practice1_greeting= folder.

((TODO: ADD IMAGE))

Build the program with =g++ greetings_v1.cpp=

Then run the program with =./a.exe=

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
$ g++ greetings_v1.cpp
$ ./a.exe
Hello, I'm: Rachel
My favorite color is: Purple
This semester I hope to learn about... Cmake :)
#+END_SRC
