# -*- mode: org -*-

*Graded Program 1: Grade*

#+BEGIN_SRC cpp :class cpp
char StudentCode( float points_possible, float my_score )
{
  char letter_grade = 'F';

  // TODO: Create a float for `grade_percent`. Calculate the percent with `my_score` divided by `points_possible` times 100.


  // TODO: Use if/else if/else statements to determine value for `letter_grade` based on the `grade_percent`;
  // 89.5 and above = A, 79.5 and above = B, 69.5 and above = C, 59.5 and above = D, below that is F.


  // TODO: Display grade information, including `my_score`, `points_possible`, `grade_percent`, and `letter_grade`.


  // TODO: Return the `letter_grade` as the result.
  return letter_grade;
}
#+END_SRC


*Program inputs:*

| Variable          | Description                                            |
|-------------------+--------------------------------------------------------|
| =points_possible= | The amount of points an assignment is worth            |
| =my_score=        | The amount of points the user earned on the assignment |

*Program outputs:*

| Scenario | Output                     | Criteria                   |
|----------+----------------------------+----------------------------|
|       1. | Corresponding letter grade | 'A', 'B', 'C', 'D', or 'F' |

*Program logic:*

- Create a float to store the =grade_percent=.
  The calculation is $$g = \frac{s}{p} \cdot 100$$,
  where $g$ is the grade percent, $s$ is the user's score, and $p$ is points possible.
- Use if/else if statements to determine the letter grade. Use the table below for the data.
- Display the grade information, such as =my_score=, =points_possible=, =grade_percent=, and the letter grade.
- At the end, make sure you have =return letter_grade;= to return the result.

*Automated tests:*

When you run the program it will ask if you want to run the AUTOMATED TESTS or the PROGRAM.
You can run the automated tests to check your work, and run the program to manually check your work.

*Example output:*

Program output:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
1. Run AUTOMATED TESTS
2. Run PROGRAM
>> 2
Enter points possible: 80
Enter your score: 75

GRADE INFORMATION
My score ......... 75.00
Points possible ...80.00
Grade % ...........93.75
Letter grade ......A

RESULT: A
#+END_SRC
