# -*- mode: org -*-

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*What we're gonna do:*
- Look at variables' memory addresses
- Create pointer variables that store memory addresses
- Dereference pointers to access the address location

*IDE help:* Look at the "C++ projects - Integrated Development Environments" section under the Reference part of this textbook.

| Topic                            | Visual Studio                                      | Code::Blocks                              | Xcode                                   | VSCode                              |
|----------------------------------+----------------------------------------------------+-------------------------------------------+-----------------------------------------+-------------------------------------|
| Creating a project               | [[https://youtu.be/4f1UJSGAcT8][🎥 Create a new project in Visual Studio]]           | [[https://youtu.be/mGoCoV0L8hU][🎥 Creating a new project in Code::Blocks]] | [[https://youtu.be/F6QZ2atZrDw?si=c89K6tY5rAGzxVZg&t=231][🎥 Creating a project in Xcode]]          | [[https://youtu.be/rNF7ZZW5Biw][🎥 Creating a new project in VSCode]] |
| Creating a new file              | [[https://youtu.be/9-VgiCXgdxk][🎥 Add new files in Visual Studio]]                  | [[https://youtu.be/q9D8LIn0QCg][🎥 Add new files in Code::Blocks]]          | [[https://developer.apple.com/documentation/xcode/managing-files-and-folders-in-your-xcode-project][📃 Xcode documentation - Managing files]] | [[https://youtu.be/obcAcHvAltk][🎥 Add new files in VSCode]]          |
| Adding an existing file(s)       | [[https://youtu.be/0x-srqvdmXg][🎥 Add existing files in Visual Studio]]             | [[https://youtu.be/nCkNvLgf4e0][🎥 Add existing files in Code::Blocks]]     | [[https://developer.apple.com/documentation/xcode/managing-files-and-folders-in-your-xcode-project][📃 Xcode documentation - Managing files]] | [[https://youtu.be/LgvHQ7bvtFU][🎥 Add existing files in VSCode]]     |
| Building and running the program | [[https://youtu.be/DmrgpzXUk2w][🎥 Build in Visual Studio]]  [[https://youtu.be/pP0SWeDHNaQ][🎥 Run in Visual Studio]] | [[https://youtu.be/OY1Pg7Ao990][🎥 Build and run in Code::Blocks]]          | [[https://youtu.be/F6QZ2atZrDw?si=-PQ-bzeOFbZnM-FY&t=359][🎥 Build and run in Xcode]]               | [[https://youtu.be/lps2-W7B4H0][🎥 Build and run in VSCode]]          |

#+END_HTML


----------------------------------------------------------------
* *Practice: =practice1_sizes=*
#+INCLUDE: "starter_code/u09_Pointers/practice1_sizes/requirements.org"

----------------------------------------------------------------
* *Practice: =practice2_addresses=*
#+INCLUDE: "starter_code/u09_Pointers/practice2_addresses/requirements.org"

----------------------------------------------------------------
* *Practice: =practice3_pointers=*
#+INCLUDE: "starter_code/u09_Pointers/practice3_pointers/requirements.org"

----------------------------------------------------------------
* *Practice: =practice4_dereferencing=*
#+INCLUDE: "starter_code/u09_Pointers/practice4_dereferencing/requirements.org"

----------------------------------------------------------------
* *Graded program: VIP Student*
#+INCLUDE: "starter_code/u09_Pointers/graded_program/requirements.org"


-----------------------------------------------------------------------------------------
* *Turning in your work*

Go to the *GitLab* webpage, locate the folder for the unit you're working in. Upload all *.h and .cpp files* (as applicable).
If you would like, you can also back up your project file: *.vcxproj* for Visual Studio or *.cbp* for Code::Blocks, but these are not required.
