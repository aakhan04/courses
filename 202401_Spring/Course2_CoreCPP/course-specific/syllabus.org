# -*- mode: org -*-

# COURSE INFORMATION, COURSE 2 CORE C++

** Course information

** Schedule
(Tentative)

| Week | Monday date  | Unit(s)                                                    |
|------+--------------+------------------------------------------------------------|
|    1 | Jan 15, 2024 | Unit 0: Welcome/Setup, Unit 1: Exploring software          |
|    2 | Jan 22, 2024 | Unit 2: Command line, Unit 3: Main, Variables, Console I/O |
|    3 | Jan 29, 2024 | Unit 4: Branching                                          |
|    4 | Feb 5, 2024  | BREAK WEEK                                                 |
|    5 | Feb 12, 2024 | Unit 5: Structs                                            |
|    6 | Feb 19, 2024 | Unit 6: Functions                                          |
|    7 | Feb 26, 2024 | Unit 7: Strings, Unit 8: File I/O                          |
|    8 | Mar 4, 2024  | Unit 9: Pointers, Unit 10: Arrays, Vectors, For loops      |
|    9 | Mar 11, 2024 | BREAK WEEK                                                 |
|   10 | Mar 18, 2024 | Unit 11: Classes, Unit 12: Inheritance                     |
|   11 | Mar 25, 2024 | Unit 13: Searching & sorting, Unit 14: Recursion           |
|   12 | Apr 1, 2024  | SEMESTER PROJECT PART 1                                    |
|   13 | Apr 8, 2024  | SEMESTER PROJECT PART 1                                    |
|   14 | Apr 15, 2024 | BREAK WEEK                                                 |
|   15 | Apr 22, 2024 | SEMESTER PROJECT PART 2                                    |
|   16 | Apr 29, 2024 | SEMESTER PROJECT PART 2                                    |
|   17 | May 6, 2024  | FINALS WEEK                                                |

Campus schedule notes:
- Jan 15: Campus closed
- Jan 16: first day of semester
- Mar 11 - 17: Spring break
- May 5: Last day of spring classes
- May 7 - 13: Final exams

** Policies
