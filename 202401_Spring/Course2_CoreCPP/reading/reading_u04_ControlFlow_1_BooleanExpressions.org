# -*- mode: org -*-

* *Boolean expressions*

In order to start writing questions that a computer can understand,
we need to understand *boolean expressions*.

A math expression looks like this:

$$ 2 + 3 $$


A boolean expression is a statement that result in either *true* or *false*:

/"it is daytime."/

/"the user saved the document"/

/"the user's age greater than 12 and less than 20"/

[[file:images/reading_u03_CPPBasics_cat.png]]

=if ( i_have_not_been_fed && it_is_6_am ) { MakeLotsOfNoise(); }=


Logic in computer programs are based around these types of questions:
something that can only be *true* or *false*. Statements
include:

- Is [boolean variable] true?
  #+BEGIN_SRC cpp :class cpp
    x
  #+END_SRC
- Is [boolean variable] false?
  #+BEGIN_SRC cpp :class cpp
  !x
  #+END_SRC
- Are two variables equal?
  #+BEGIN_SRC cpp :class cpp
  x == y
  #+END_SRC
- Are two variables /not/ equal?
  #+BEGIN_SRC cpp :class cpp
  x != y
  #+END_SRC
- Is x less than y?
  #+BEGIN_SRC cpp :class cpp
  x < y
  #+END_SRC
- Is x less than or equal to y?
  #+BEGIN_SRC cpp :class cpp
  x <= y
  #+END_SRC
- Is x greater than y?
  #+BEGIN_SRC cpp :class cpp
  x > y
  #+END_SRC
- Is x greater than or equal to y?
  #+BEGIN_SRC cpp :class cpp
  x >= y
  #+END_SRC

For the first two, we can check the value of a *boolean variable*.
The rest of them, we can use any data type to compare if two
values are equal, greater than, less than, etc. =a= and =b= can be replaced
with variables and/or values...

#+BEGIN_SRC cpp :class cpp
  if ( age < 18 ) { ... }

  if ( my_state == your_state ) { ... }

  if ( word1 < word2 ) { ... }

  if ( document_saved == true ) { ... }

  if ( document_saved == false ) { ... }
#+END_SRC

Note: When using $<$ and $>$ with strings or chars, it
will compare them based on alphabetical order - if they're both the
same case (both uppercase or both lowercase).



---------------------------------------------------------
** *And, Or, and Not operators*

We can also combine boolean expressions together to ask more sophisticated questions.


*** *Not operator: =!=*
If we're checking a boolean variable, or a boolean expression, and we happen
to want to execute some code for when that result is *false*, we can
use the not operator...

 Using with *boolean variable *documentSaved*:*
#+BEGIN_SRC cpp :class cpp
  // Just checking documentSaved = true
  if ( documentSaved )
    {
    Quit();
  }

  // Checking if documentSaved = false with the not operator
  if ( !documentSaved )
  {
    Save();
    Quit();
  }
#+END_SRC

 Using with *boolean expressions *( age >= 21 )*:*
#+BEGIN_SRC cpp :class cpp
  // If the age is not 21 or over...
  if ( !( age >= 21 ) )
  {
    NoBeer();
  }
#+END_SRC






#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *And operator: =&&=*

When using the and operator, we can check to see if *all boolean variables/expressions are true*.
The full question of "is this and this and this true?" only results to *true*
if *all boolean variables/expressions are true*. If even one of them
are false, then the entire statement is false.

#+BEGIN_SRC cpp :class cpp
if ( wantsBeer && isAtLeast21 ) {
GiveBeer();
}
#+END_SRC

To break it down, the customer would only get beer if they
/want beer/ AND if /they are over 21/. If they don't
want beer but are 21 or over, they don't get beer. If they want beer
but are under 21, then they don't get beer.





#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Or operator: =||=*

For an or operator, we can check to see if *at least one boolean variable/expression is true*.
If at least one item is true, then the whole statement is true.
The only way for it to be false is when each boolean variable/expression is false.

#+BEGIN_SRC cpp :class cpp
  if ( isBaby || isSenior || isVet )
  {
    GiveDiscount();
  }
#+END_SRC

In this case, discounts are given to babies, seniors, and vets. A customer
wouldn't have to be all three to qualify (and clearly, couldn't be all three
at the same time!). If the customer is /not/ a baby and /not/ a senior
and /not/ a vet, then they don't get the discount - all three criteria
have to be false for the entire expression to be false.





---------------------------------------------------------
** *Truth tables*

We can use *truth tables* to help us visualize the logic that we're working with,
and to validate if our assumptions are true. Truth tables are for when we have
an expression with more than one variable (usually) and want to see the result
of using ANDs, ORs, and NOTs to combine them.




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Truth table for NOT:*

On the top-left of the truth table we will have the boolean variables or expressions
written out, and we will write down all its possible states. With just one
variable $p$, we will only have two states: when it's true, or when it's false.


On the right-hand side (I put after the double lines) will be the result of the
expression - in this case, "not-p" $!p$. The not operation simply takes the
value and flips it to the opposite: true $\to$ false, and false $\to$ true.

| =p= | =!p= |
|-----+------|
| T   | F    |
| F   | T    |






#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Truth table for AND:*
For a truth table that deals with two variables, $p$ and $q$, the total
amount of states will be 4:

1. $p$ is true and $q$ is true.
2. $p$ is true and $q$ is false.
3. $p$ is false and $q$ is true.
4. $p$ is false and $q$ is false.


| =p= | =q= | =p && q= |
|-----+-----+----------|
| T   | T   | T        |
| T   | F   | F        |
| F   | T   | F        |
| F   | F   | F        |


This is just a generic truth table. We can replace
the values with boolean expressions in C++ to check our logic.


For example, we will only quit the program if the game is saved and the
user wants to quit:

| =game_saved= | =want_to_quit= | =game_saved && want_to_quit= |
|--------------+----------------+------------------------------|
| T            | T              | T                            |
| T            | F              | F                            |
| F            | T              | F                            |
| F            | F              | F                            |


The states are:


1. =gameSaved= is true and =wantToQuit= is true: Quit the game. ✅
2. =gameSaved= is true and =wantToQuit= is false: The user doesn't want to quit; don't quit. ❌
3. =gameSaved= is false and =wantToQuit= is true: The user wants to quit but we haven't saved yet; don't quit. ❌
4. =gameSaved= is false and =wantToQuit= is false: The user doesn't want to quit and the game hasn't been saved; don't quit. ❌






#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Truth table for OR:*
Generic form:

| =p= | =q= | =p ‖ q= |
|-----+-----+---------|
| T   | T   | T       |
| T   | F   | T       |
| F   | T   | T       |
| F   | F   | F      |


Again, if at least one of the expressions is *true*,
then the entire expression is true.

Example: If the store has cakes OR ice cream, then suggest it to the user that wants dessert.

| =hasCake= | =hasIcecream= | =hasCake= ‖ =hasIceCream= |
|-----------+---------------+---------------------------|
| T         | T             | T                         |
| T         | F             | T                         |
| F         | T             | T                         |
| F         | F             | F                         |

The states are:


1. =hasCake= is true and =hasIcecream= is true: The store has desserts; suggest it to the user. ✅
2. =hasCake= is true and =hasIcecream= is false: The store has cake but no ice cream; suggest it to the user. ✅
3. =hasCake= is false and =hasIcecream= is true: The store has no cake but it has ice cream; suggest it to the user. ✅
4. =hasCake= is false and =hasIcecream= is false: The store doesn't have cake and it doesn't have ice cream; don't suggest it to the user. ❌



---------------------------------------------------------
** *DeMorgan's Laws*

Finally, we need to cover DeMorgan's Laws, which tell us what the /opposite/
of an expression means.


For example, if we ask the program *"is a $>$ 20?"* and the result is *false*,
then what does this imply? If $a > 20$ is false,
then $a \leq 20$ is true... notice that the opposite of
"greater than" is "less than OR equal to"!




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Opposite of =a && b=*

| =a= | =b= | =a && b= | =!( a && b )= | =!a ‖ !b= |
|-----+-----+----------+---------------+-----------|
| T   | T   | T        | F             | F         |
| T   | F   | F        | T             | T         |
| F   | T   | F        | T             | T         |
| F   | F   | F        | T             | T         |

If we're asking "is $a$ true and is $b$ true?" together,
and the result is *false*, that means either:


1. $a$ is false and $b$ is true, or
2. $a$ is true and $b$ is false, or
3. $a$ is false and $b$ is false.

These are the states where the result of =a && b= is false in the truth table.

In other words,

=!( a && b )= $\equiv$ =!a ‖ !b=

If $a$ is false, or $b$ is false, or both are false,
then the result of =a && b= is false.




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Opposite of =a ‖ b=:*

| =a= | =b= | =a ‖ b= | =!( a ‖ b )= | =!a && !b= |
|-----+-----+---------+--------------+------------|
| T   | T   | T       | F            | F          |
| T   | F   | T       | F            | F          |
| F   | T   | T       | F            | F          |
| F   | F   | F       | T            | T          |

If we're asking "is $a$ true or $b$ true?", if the result of that is *false*
that means only one thing:

1. Both $a$ and $b$ were false.


This is the only state where the result of =a || b= is false.

In other words,


=!( a ‖ b )= $\equiv$ =!a && !b=


=a ‖ b= is false only if $a$ is false AND $b$ is false.



---------------------------------------------------------
** *Summary*

In order to be able to write *if statements* or *while loops*,
you need to understand how these boolean expressions work. If you're not familiar
with these concepts, they can lead to you writing *logic errors*
in your programs, leading to behavior that you didn't want.

[[file:images/reading_u04_ControlFlow_evilbug.png]]
