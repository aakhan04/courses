// PROGRAM: Console output practice

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  // TODO: Update the placeholder text below, NAME, COLOR, and SOMETHING, with your own text.

  cout << "Hello, I'm: NAME" << endl;
  cout << "My favorite color is: COLOR" << endl;
  cout << "This semester I hope to learn about... SOMETHING" << endl;

  // Return 0 means quit program with no errors, in this context.
  return 0;
}

