# -*- mode:org -*-

* *About*

For this assignment we're going to take the code you have from v3 and wrap it all in a Program class. We will also implement a basic search feature.

Utilize the starter code as a base: https://gitlab.com/moosadee/courses/-/tree/main/202401_Spring/Course2_CoreCPP/projects/starter_code/Project4?ref_type=heads

----------------------------------------
* *How to be successful at projects*

- Your program should *always build*. Don't continue writing features if you have build errors. WORKING CODE THAT IS INCOMPLETE IS BETTER THAN "COMPLETE" BUT BROKEN CODE THAT DOESN'T EVEN RUN.
- Implement one feature at a time. Test after implementing one feature. Work on small chunks at a time.
- Utilize other code as reference material; code from in class, previous projects, example code from the example code repository (https://gitlab.com/rachels-courses/example-code/-/tree/main/C++?ref_type=heads).
- Research error messages via search engine, look for an explanation of what an error message means. You can even try to ask an AI what an error message means and see if it gives a coherent explanation.
- You can refer to outside sources, just don't plagiarize. You need to learn how to do the programming yourself, but researching is a normal part of "the job".
- If you get stuck, you should seek solutions, such as:
  - Asking a tutor - The campus has tutors via the Academic Achievement Center.
  - Asking the instructor - You can email me, you can ask me in class, you can schedule a one-on-one time to meet via Zoom or in person.
  - Looking at example code from class videos and the example code repository.

----------------------------------------
* *Example output*

*Main menu*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
################################################################################
# MAIN MENU #
#############
 0. Quit program
 1. View recipe
 2. Save recipes to text file
 3. Add new recipe
 4. Search for a recipe
 --> Where do you want to go?
#+END_SRC

*View recipes menu*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
################################################################################
# VIEW RECIPE #
###############
 0. GO BACK
 1. Tiger Butter Candy
 2. White Chocolate Peppermint Pretzels
 3. Pie for breakfast shake

 --> Which recipe? 1
 --> How many batches would you like to make? 2
#+END_SRC

*View single recipe menu*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
################################################################################
# Tiger Butter Candy #
######################
From https://butterwithasideofbread.com/tiger-butter-fudge/

INGREDIENTS
- 11 oz of Vanilla chips
- 0.5 cups of Creamy peanut butter
- 0.5 cups of Semi-sweet chocolate chips
--------------------------------------------------------------------------------
#+END_SRC

*Save recipes to text file menu*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
################################################################################
# PRINT RECIPES #
#################
 Enter a filename for your output: output.txt
 File saved.


 --> Press ENTER to continue
#+END_SRC

*Add new recipe menu*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
################################################################################
# NEW RECIPE #
##############
 --> Enter new recipe name: Choco Cookies
 --> Enter URL: asdf.com
 --> How many ingredients? 2

 INGREDIENT #1:
 --> Name: Chocolate, melted
 --> Unit of measurement: cups
 --> Measurement amount: 2

 INGREDIENT #2:
 --> Name: Chocolate chips, whole
 --> Unit of measurement: cups
 --> Measurement amount: 2

 New recipe added.


 --> Press ENTER to continue
#+END_SRC

*Search for a recipe menu*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
################################################################################
# SEARCH FOR RECIPE #
#####################
Enter part of recipe name to search for: Ch

RESULTS
1. White Chocolate Peppermint Pretzels
 - Ingredients: Vanilla chips, Creamy peanut butter, Semi-sweet chocolate chips, Pretzel, White chocolate melting wafers, Crushed candy canes

3. Choco Cookies
 - Ingredients: Chocolate, melted, Chocolate chips, whole



 --> Press ENTER to continue
#+END_SRC
